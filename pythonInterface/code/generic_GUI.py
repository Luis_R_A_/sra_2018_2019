
#
# pack organizes the widget first https://www.tutorialspoint.com/python/tk_pack.htm
#
#
#

from tkinter import *
import tkinter as tk
from tkinter import ttk
import callbacksLogging
from queue import Queue
from serial.tools import list_ports



from parserLogging import c_parser
import parserLogging


from fileAccess import c_logFile

from livePlot import c_livePlot
import struct


class c_generic_GUI():


    def serialRefresh(self):
        currentSerial = self.serialBox.get()
        serialPorts = []
        serialStillExists = False
        for p in list_ports.comports():
            if p.device == currentSerial:
                serialStillExists = True
            serialPorts.append(p.device)
        self.serialBox['values'] = serialPorts
        if serialStillExists:
            self.serialBox.set(currentSerial)
        else:
            self.serialBox.set("")

        self.root.after(1000, self.serialRefresh)


    def __init__(self,root, controller):

        self.root = root
        self.controller = controller
        self.root.title("SRA_labwork1_interface_v001")

        #register closing method when window close button is pressed
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.lineCounter_tfieldSysConfig = 0
        #create queues
        #self.queueInput = Queue()
        #self.queueOutput = Queue()


        #self.infinityController = None
        self.serialPort = None
        self.transport = None

        #-----------------------------------------------------
        self.buttonConnect = tk.Button(self.root,text="connect", command=self.connect)
        #self.buttonConnect.pack(side=RIGHT)
        self.buttonConnect.grid(row = 1, column = 0, sticky = E+W)
        self.connected = False
        #-----------------------------------------------------


        #self.buttonGoNow = tk.Button(self.root,text="Frente", command=self.goNow)
        #self.buttonGoNow.grid(row = 3, column = 7, sticky = E+W)
        #self.buttonLeft = tk.Button(self.root,text="Left", command=self.goLeft)
        #self.buttonLeft.grid(row = 4, column = 6, sticky = E+W)
        #self.buttonRight = tk.Button(self.root,text="Right", command=self.goRight)
        #self.buttonRight.grid(row = 4, column = 8, sticky = E+W)
        #self.buttonBack = tk.Button(self.root,text="Back", command=self.goBack)
        #self.buttonBack.grid(row = 5, column = 7, sticky = E+W)
        #self.buttonStop = tk.Button(self.root,text="Stop", command=self.goStop)
        #self.buttonStop.grid(row = 4, column = 7, sticky = E+W)
        #self.goNowFlag = False


        self.labelEntryLaw = tk.Label(self.root, text = "Law {0,1,2,3}")
        self.labelEntryLaw.grid(row = 4, column = 0, sticky = S)
        self.entryLawValue = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryLawValue.grid(row = 5, column = 0, sticky = E+W)
        self.entryLawValue.delete(0,END)
        self.entryLawValue.insert(0,0)
        self.entryLawValue.bind('<Return>', self.SendLaw)

        self.labelEntryPose = tk.Label(self.root, text = "Desired Pose (x,y,phi)")
        self.labelEntryPose.grid(row = 6, column = 0, sticky = S)
        self.entryPoseValue = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryPoseValue.grid(row = 7, column = 0, sticky = E+W)
        self.entryPoseValue.delete(0,END)
        self.entryPoseValue.insert(0,"1200,1200,0")
        self.entryPoseValue.bind('<Return>', self.SendPose)

        self.labelEntryABCV= tk.Label(self.root, text = "ABCV (a,b,c,v)")
        self.labelEntryABCV.grid(row = 8, column = 0, sticky = S)
        self.entryABCVValue = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryABCVValue.grid(row = 9, column = 0, sticky = E+W)
        self.entryABCVValue.delete(0,END)
        self.entryABCVValue.insert(0,"0,-1,200,600")
        self.entryABCVValue.bind('<Return>', self.SendABCV)

        self.kv = 1.0
        self.ki = 1.0
        self.ks = 3.0
        self.labelEntryGainLaw1= tk.Label(self.root, text = "Gains Law 1(kv,ki,ks)")
        self.labelEntryGainLaw1.grid(row = 11, column = 0, sticky = S)
        self.entryGainLaw1Value = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryGainLaw1Value.grid(row = 12, column = 0, sticky = E+W)
        self.entryGainLaw1Value.delete(0,END)
        self.entryGainLaw1Value.insert(0,"40.0,0.08,20.0")
        self.entryGainLaw1Value.bind('<Return>', self.SendGainsLaw1)

        self.kd = 0.01
        self.kh = 5.0
        self.labelEntryGainLaw2= tk.Label(self.root, text = "Gains Law 2(kd,kh)")
        self.labelEntryGainLaw2.grid(row = 13, column = 0, sticky = S)
        self.entryGainLaw2Value = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryGainLaw2Value.grid(row = 14, column = 0, sticky = E+W)
        self.entryGainLaw2Value.delete(0,END)
        self.entryGainLaw2Value.insert(0,"0.01,5.0")
        self.entryGainLaw2Value.bind('<Return>', self.SendGainsLaw2)

        self.kp = 3.0
        self.ka = 8.0
        self.kb = -2.5
        self.labelEntryGainLaw3= tk.Label(self.root, text = "Gains Law 3(kp,ka,kb)")
        self.labelEntryGainLaw3.grid(row = 15, column = 0, sticky = S)
        self.entryGainLaw3Value = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryGainLaw3Value.grid(row = 16, column = 0, sticky = E+W)
        self.entryGainLaw3Value.delete(0,END)
        self.entryGainLaw3Value.insert(0,"3.0,8.0,-2.5")
        self.entryGainLaw3Value.bind('<Return>', self.SendGainsLaw3)

        self.labelEntryPoseNow = tk.Label(self.root, text = "Pose now(x,y,phi)")
        self.labelEntryPoseNow.grid(row = 17, column = 0, sticky = S)
        self.entryPoseNowValue = tk.Entry(self.root, font = "Helvetica 12 bold")
        self.entryPoseNowValue.grid(row = 18, column = 0, sticky = E+W)
        self.entryPoseNowValue.delete(0,END)
        self.entryPoseNowValue.insert(0,"500,500,0")
        self.entryPoseNowValue.bind('<Return>', self.SendPoseNow)

        self.root.bind("<KeyPress>", self.keydown)
        self.root.bind("<KeyRelease>", self.keyup)

        #-----------------------------------------------------
        baudrates = ['921600', '115200', '1843200', '38400', '9600','1000000']
        self.baudBox = ttk.Combobox(self.root, values=baudrates, state="readonly", height=4)
        if baudrates != []:
            self.baudBox.set(baudrates[0])
            self.baudrate = baudrates[0]
        self.baudBox.bind("<<ComboboxSelected>>", self._changeBaudrate)
        #self.baudBox.pack(fill='x', side=RIGHT)
        self.baudBox.grid(row = 2, column = 0, sticky = E+W)
        #-----------------------------------------------------




        #-----------------------------------------------------
        serialPorts = []
        for p in list_ports.comports():
             print(p.device)
             serialPorts.append(p.device)
        self.serialBox = ttk.Combobox(self.root, values=serialPorts, state="readonly", height=4)
        if serialPorts != []:
            self.serialBox.set(serialPorts[0])
            self.serialPort = serialPorts[0]
            print("serial defaul port:",self.serialPort)
        self.serialBox.bind("<<ComboboxSelected>>", self._changeSerial)
        #self.serialBox.pack(fill='x', side=RIGHT)
        self.serialBox.grid(row = 3, column = 0, sticky = E+W)
        self.root.after(1000, self.serialRefresh)
        #self.serialBox.grid_remove()
        #-----------------------------------------------------


        #-------------------------------------------------------
        self.buttonBlinkerPoseMessageLabel = tk.Label(self.root, text = "Pose Message activity",  justify = tk.LEFT, anchor="w")
        self.buttonBlinkerPoseMessageLabel.config(font=("Courier", 11))
        self.buttonBlinkerPoseMessageLabel.grid(row=1,column=3, sticky = E+W, rowspan=1, columnspan = 2)
        self.buttonBlinkerPoseMessage = tk.Button(self.root, text = "", command = self.responseBlinker, state = "disabled", height = 5, width =10)
        self.buttonBlinkerPoseMessage.grid(row = 2, column = 3, sticky = N, rowspan=2, columnspan = 2)
        self.buttonBlinkerPoseMessage.configure(bg = "blue")
        self.buttonBlinkerStatePoseMessage = False

        #-------------------------------------------------------


        self.logFolder = c_logFile("Folder")


        self.MotorSpeedMessage = parserLogging.c_message(type = 0, callback = callbacksLogging.c_callback_MotorSpeed(self.logFolder))
        self.OrientationMessage = parserLogging.c_message(type = 1, callback = callbacksLogging.c_callback_Orientation(self.logFolder))
        self.GyroMessage = parserLogging.c_message(type = 2, callback = callbacksLogging.c_callback_Gyro(self.logFolder))
        self.AccMessage = parserLogging.c_message(type = 3, callback = callbacksLogging.c_callback_Acc(self.logFolder))

        self.RawAccGyroMessage = parserLogging.c_message(type = 15, callback = callbacksLogging.c_callback_rawAccGyro(self.logFolder))

        self.pythonMessageMessage = parserLogging.c_message(type = 11, callback = callbacksLogging.c_callback_pythonMessage(self.logFolder))

        self.poseMessage = parserLogging.c_message(type = 16, callback = callbacksLogging.c_callback_poseMessage(self.logFolder))

        self.remoteControlMessage = parserLogging.c_message(type = 17)

        self.sendDesiredPoseMessage = parserLogging.c_message(type = 18)
        self.sendDesiredABCVMessage = parserLogging.c_message(type = 19)
        self.startDesiredLawMessage = parserLogging.c_message(type = 20)
        self.startDesiredGainMessage = parserLogging.c_message(type = 21)

        self.RPLidarMessage = parserLogging.c_message(type = 23, callback = callbacksLogging.c_callback_RPLidarMessage(self.logFolder))

        self.mapCellOccupancyMessage = parserLogging.c_message(type = 24, callback = callbacksLogging.c_callback_mapCellOccupancyMessage(self.logFolder))

        self.histogramMessage = parserLogging.c_message(type = 25, callback = callbacksLogging.c_callback_histogramMessage(self.logFolder))

        self.encoderMessage = parserLogging.c_message(type = 26, callback = callbacksLogging.c_callback_EncoderMessage(self.logFolder))

        self.knownMessages = [self.MotorSpeedMessage, self.OrientationMessage,
                              self.GyroMessage, self.AccMessage, self.pythonMessageMessage,
                              self.RawAccGyroMessage, self.poseMessage, self.remoteControlMessage,
                              self.sendDesiredPoseMessage, self.sendDesiredABCVMessage, self.startDesiredLawMessage,
                              self.startDesiredGainMessage, self.RPLidarMessage, self.mapCellOccupancyMessage,
                              self.histogramMessage, self.encoderMessage]

        self.controller.updateKnownMessages(self.knownMessages)

        self.transportHandler = None


        self.liveplot = c_livePlot(self.root, self.logFolder , "poseMessage001.csv", row = 4, column = 5, yaxes = [0,4000], xaxes = [0, 4000], hasX = True)

        self.root.after(100, self.pollSerial)

    def SendPoseNow(self,k):
        txt = self.entryPoseNowValue.get()
        a = txt.split(',')
        if(len(a) != 3):
            return
        x = float(a[0])
        y = float(a[1])
        phi = float(a[2])/180.0*3.14

        x_array = bytearray(struct.pack('f', x))
        y_array = bytearray(struct.pack('f', y))
        phi_array = bytearray(struct.pack('f', phi))
        toSend = []
        toSend.extend(x_array)
        toSend.extend(y_array)
        toSend.extend(phi_array)
        self.controller.sendMessage(22,toSend)
    def sendAllGains(self):
        kv_array = bytearray(struct.pack('f', self.kv))
        ki_array = bytearray(struct.pack('f', self.ki))
        ks_array = bytearray(struct.pack('f', self.ks))
        kd_array = bytearray(struct.pack('f', self.kd))
        kh_array = bytearray(struct.pack('f', self.kh))
        kp_array = bytearray(struct.pack('f', self.kp))
        ka_array = bytearray(struct.pack('f', self.ka))
        kb_array = bytearray(struct.pack('f', self.kb))

        toSend = []
        toSend.extend(kv_array)
        toSend.extend(ki_array)
        toSend.extend(ks_array)
        toSend.extend(kd_array)
        toSend.extend(kh_array)
        toSend.extend(kp_array)
        toSend.extend(ka_array)
        toSend.extend(kb_array)
        self.controller.sendMessage(21,toSend)

    def SendGainsLaw1(self,k):
        txt = self.entryGainLaw1Value.get()
        astr = txt.split(',')
        if(len(astr) != 3):
            return
        self.kv = float(astr[0])
        self.ki = float(astr[1])
        self.ks = float(astr[2])
        self.sendAllGains()

    def SendGainsLaw2(self,k):
        txt = self.entryGainLaw2Value.get()
        astr = txt.split(',')
        if(len(astr) != 2):
            return
        self.kd = float(astr[0])
        self.kh = float(astr[1])

        self.sendAllGains()

    def SendGainsLaw3(self,k):
        txt = self.entryGainLaw3Value.get()
        astr = txt.split(',')
        if(len(astr) != 3):
            return
        self.kp = float(astr[0])
        self.ka = float(astr[1])
        self.kb = float(astr[2])

        self.sendAllGains()

    def SendABCV(self,k):
        txt = self.entryABCVValue.get()
        astr = txt.split(',')
        if(len(astr) != 4):
            return
        a = float(astr[0])
        b = float(astr[1])
        c = float(astr[2])
        v = float(astr[3])

        a_array = bytearray(struct.pack('f', a))
        b_array = bytearray(struct.pack('f', b))
        c_array = bytearray(struct.pack('f', c))
        v_array = bytearray(struct.pack('f', v))
        toSend = []
        toSend.extend(a_array)
        toSend.extend(b_array)
        toSend.extend(c_array)
        toSend.extend(v_array)
        self.controller.sendMessage(19,toSend)
        print(toSend)

    def SendPose(self,k):
        txt = self.entryPoseValue.get()
        a = txt.split(',')
        if(len(a) != 3):
            return
        x = float(a[0])
        y = float(a[1])
        phi = float(a[2])/180.0*3.14

        x_array = bytearray(struct.pack('f', x))
        y_array = bytearray(struct.pack('f', y))
        phi_array = bytearray(struct.pack('f', phi))
        toSend = []
        toSend.extend(x_array)
        toSend.extend(y_array)
        toSend.extend(phi_array)
        self.controller.sendMessage(18,toSend)
        print(a)
    def SendLaw(self, k):

        try:
            toSend = [ int(self.entryLawValue.get())];
            self.controller.sendMessage(20,toSend)
        except:
            pass
    def keyup(self,e):
        #print("key released", e.char)
        #self.goStop()
        pass

    def keydown(self,e):
        print("key down", e.char)
        if(e.char == 'w'):
            self.goNow()
        elif(e.char == 'a'):
            self.goLeft()
        elif(e.char == 'd'):
            self.goRight()
        elif(e.char == 's'):
            self.goBack()
        elif(e.char == 'z'):
            self.goStop()
    def goNow(self):



        #self.goNowFlag = True
        toSend = [50,0, 50,0];
        self.controller.sendMessage(17,toSend)
    def goLeft(self):
        toSend = [0,0, 50,0];
        self.controller.sendMessage(17,toSend)

    def goRight(self):
        toSend = [50,0, 0,0];
        self.controller.sendMessage(17,toSend)
    def goBack(self):
        toSend = [156,255, 156,255];
        self.controller.sendMessage(17,toSend)
    def goStop(self):
        toSend = [1,0, 1,0];
        self.controller.sendMessage(17,toSend)
    def responseBlinker(self):
        pass

    #  int_to_bytes
    #=======================================
    def int_to_bytes(self, number):
        b = []
        k = 0
        while number:
            b.append(number % 256)
            number = int(number / 256)
            k += 1
            if k >= 4:
                return b
        i = k
        for n in range(i,4):
            b.append(0)
            k += 1
        return b
    #=======================================
    # END int_to_bytes
    #=======================================

    #=======================================
    #  _changeSerial
    #=======================================
    def _changeSerial(self,event):
        self.serialPort = self.serialBox.get()
        print("Serial port is now = ", self.serialPort)
        self.serialBox.config({"background": "Green"})
    #=======================================
    # END  _changeSerial
    #=======================================
    #=======================================
    #  _changeBaudrate
    #=======================================
    def _changeBaudrate(self, event):
        self.baudrate = self.baudBox.get()
        print("baudrate is now = ", self.baudrate)
        self.baudBox.config({"background": "Green"})
    #=======================================
    # END _changeBaudrate
    #=======================================



    #=======================================
    # clearConsole
    #=======================================
    def clearConsole(self):
        self.printClear()
        self.print("***Other messages window***\n")
    #=======================================
    # END clearConsole
    #=======================================
    #=======================================
    # connect
    #=======================================
    def connect(self):
        if(self.connected == False):

            if(self.serialPort and self.baudrate):



                #self.transportHandler = transportSerial(self.serialPort, self.baudrate)

                #self.printClear()
                self.transport = [self.serialPort, self.baudrate] # transportSerial( self.serialPort, self.baudrate)
                self.controller.startProcess(self.transport)
                try:
                    print("generic_GUI: initTransport seems okay")
                    self.baudBox['state'] = 'disabled'
                    self.serialBox['state'] = 'disabled'
                    self.connected = True

                   #GUI change aspect
                    self.orig_color = self.buttonConnect.cget("background")
                    self.buttonConnect.configure(bg="Green")
                    self.buttonConnect.config(text='Disconnect')
                except:
               #GUI change aspect
                    self.orig_color = self.buttonConnect.cget("background")
                    self.buttonConnect.configure(bg="Red")
                    print("error in generic GUI when initializing")
            else:
                if not self.serialPort:
                    self.print("Port not set\n")
                if not self.baudrate:
                    self.print("baudrate not set\n")
                self.print("============================\n")
        else:
            self.controller.stopProcess()
            self.baudBox['state'] = 'readonly'
            self.serialBox['state'] = 'readonly'
            self.connected = False
            self.transportHandler = None
            self.buttonConnect.configure(bg=self.orig_color)
            self.buttonConnect.config(text='Connect')

    #=======================================
    # END connect
    #=======================================




    #=======================================
    # on_closing
    #=======================================
    def on_closing(self):
        self.liveplot.stop()
        print("gui bye")
        self.root.destroy()
        self.controller.stopProcess()
        if self.connected:
            self.transport = None
    #=======================================
    # END on_closing
    #=======================================



    def pollSerial(self):
        counter = 0
        while counter < 100:
            message = self.controller.getMessages()

            if message:
                #print("found message", message.type)
                for x in self.knownMessages:
                    if message.type == x.type:
                        toSend = x.callback.runCallback(message.content)

                        if toSend.find("Pose Message") != -1:
                            if self.buttonBlinkerStatePoseMessage:
                                self.buttonBlinkerStatePoseMessage = False
                                self.buttonBlinkerPoseMessage.configure(bg = "blue")
                            else:
                                self.buttonBlinkerStatePoseMessage = True
                                self.buttonBlinkerPoseMessage.configure(bg = "yellow")

                        #self.tfield.config(state=NORMAL)
                        #self.tfield.insert("end", toSend)
                        #self.tfield.see("end")
                        #self.tfield.config(state=DISABLED)
                        break
            else:
                break
            counter += 1

        self.root.after(1, self.pollSerial)






