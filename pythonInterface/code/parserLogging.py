from transport import transportSerial


#this is just content size.
                        #0 , 1,   2,  3,  4, 5, 6, 7, 8, 9,10, 11,12,13,14, 15, 16,17,18,19,20,   21, 22
logging_messagesSizes = [12, 8,  16, 16, 16, 0, 0, 1, 0, 0, 0, 16, 1, 4, 1, 12, 16, 8, 16, 20, 5, 32, 16, 28, 16, 152, 8]


class c_message():

    def __init__(self, type = None, callback = None):

        self.sync1 = 0x01
        self.sync2 = 0x20
        self.type = type
        self.content = []
        self.checksum = 0
        self.counter = 0
        self.callback = callback

    def addContent(self, byte):
        #print("size", logging_messagesSizes[self.type])
        #print(self.counter)
        if  self.counter >= logging_messagesSizes[self.type]:
            return -1
        self.counter +=1
        self.content.append(byte)
        if  self.counter >= logging_messagesSizes[self.type]:
            return -1

    def updateCheckSum(self):
        self.checksum = self.sync1
        self.checksum += self.sync2
        self.checksum += self.type
        for x in self.content:
            self.checksum += x

        self.checksum = self.checksum % 256

    def printSelf(self):
        print("type={}\nContent:\n".format(self.type))
        for x in self.content:
            print(x)
        print("checksum={}".format(self.checksum))


from multiprocessing import Process
import multiprocessing
import copy

class c_parser():

    def __init__(self):


        self.knownMessages = None

        self.state = 0;
        self.message = c_message()

        self.queue2Process = multiprocessing.Queue()
        self.queueFromProcess = multiprocessing.Queue()
        self.queueCommands = multiprocessing.Queue()
        self.queueReportFromProcess = multiprocessing.Queue()

    def updateKnownMessages(self, knownMessagesList):
        #print("updating known Messages")
        #self.queue2Process.put(knownMessagesList)
        #print("sent known Messages")
        self.knownMessages = knownMessagesList

    def getMessages(self):
        try:
            message = self.queueFromProcess.get_nowait()
            return message
        except:
            return None

    def createProcess(self):
        self.p = Process(target=self.processTask, args=(self.queueCommands, self.queue2Process, self.queueFromProcess, self.queueReportFromProcess))
        self.p.start()

    def startProcess(self, transport):
        self.queueCommands.put(transport)

    def stopProcess(self):
        self.queueCommands.put("STOP")
        self.p.terminate()


    def sendMessage(self, messageID, content):


        message2Send = c_message()


        message2Send.type = messageID

        for x in content:
            message2Send.addContent(x)


        message2Send.updateCheckSum()


        buff2Send = []
        buff2Send.append(message2Send.sync1)
        buff2Send.append(message2Send.sync2)
        buff2Send.append(message2Send.type)
        for x in message2Send.content:
            buff2Send.append(x)
        buff2Send.append(message2Send.checksum)
        #self.transportHandler.sendData(buff2Send)
        #print("content", content)
        #print(buff2Send)
        self.queue2Process.put(buff2Send)





    def processTask(self, queueCommands, queue2Process, queueFromProcess, queueReportFromProcess):
        transport = queueCommands.get(block = True, timeout = 999999)
        print("processTask received", transport)


        serialPort = transport[0]
        baudrate = transport[1]
        transportOk = False
        ransportHandler = None
        try:
            transportHandler = transportSerial(serialPort, baudrate)
            transportOk = True
        except:
            pass

        if transportOk:
            queueReportFromProcess.put("OK")


            while(1):

                try:
                    command = queueCommands.get_nowait()
                    if(command == "STOP"):
                        transportHandler = None
                        break
                except:
                    pass

                try:
                    buffer = queue2Process.get_nowait()
                    #print("test")
                    transportHandler.sendData(buffer)
                except:
                    pass



                byte = transportHandler.pollByte()
                if byte != None:
                    #byte = int.from_bytes(byte, byteorder='big')
                    message = self.parser(byte)
                    if message:
                        queueFromProcess.put(message)
                        #for x in self.knownMessages:
                        #    if message.type == x.type:
                                #toSend = x.callback.runCallback(message.content)
                                #self.tfield.config(state=NORMAL)
                                #self.tfield.insert("end", toSend)
                                #self.tfield.see("end")
                                #self.tfield.config(state=DISABLED)
                        #        break

    def parser(self, byte):
        #print("byte", byte)
        if self.state == 0 and byte == self.message.sync1:
            #print("found sync1")
            self.state = 1

        elif self.state == 1:
            if byte == self.message.sync2:
                #print("found sync2")
                self.state = 2
                self.message = c_message()
            else:
                self.state = 0
        elif self.state == 2:
            #print("type =", byte)
            if byte >= 0 and byte <= 26:
                self.message.type = byte
                self.state = 3
            else:
                self.state = 0
        elif self.state == 3:

            if self.message.addContent(byte) == -1:
                #print("found last byte of content")
                self.state = 4

        elif self.state == 4:
            self.state = 0
            #print("checksum check")
            self.message.updateCheckSum()
            if self.message.checksum == byte:
                return self.message
            else:
                pass
                print("checksum fail, got", byte, "should be", self.message.checksum)
                #self.message.printSelf()


        return None
