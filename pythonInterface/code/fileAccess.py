


import datetime
import os
from pathlib import Path
from file_read_backwards import FileReadBackwards


class c_fileAccess():
    def __init__(self, path, override = False):
        if not isinstance(path, str):
            raise ValueError

        if(override):
            try:
                os.remove(path)
            except:
                pass
        self.file = open(path, "a+")


    def read(self):
        return self.file.read()
    def readLine(self):
        return self.file.readlines()
    def appendToFile(self, data):
        self.file.seek(0,2) #set to send of file. second parameter = 2 makes it relative to the end
        self.file.write(data)
    def close(self):
        self.file.close()
    def eraseLast(self):
        self.file.seek(-1, os.SEEK_END)
        self.file.truncate()


class c_logFile():
    def __init__(self, override = False):
        #fileNewName = nameFile
        #if(override == False):
        #    counter = 1
        #    while(os.path.isfile(fileNewName + ".txt")):
        #        counter += 1
        #        fileNewName = nameFile + str(counter)
        now = datetime.datetime.now()
        self.folderName = "{:0004}{:02}{:02}_{:02}{:02}{:02}_Logs".format(now.year, now.month, now.day, now.hour, now.minute, now.second)
        if not os.path.exists(self.folderName):
            os.makedirs(self.folderName)
        #self.logFile = c_fileAccess(folderName + "/" + fileNewName + ".txt", override)

    def createFile(self, fileName):
        return c_fileAccess(self.folderName + "/" + fileName + ".txt")


    def openPythonCSV(self, fileName, arg = "r"):
        return open(self.folderName + "/" + fileName + ".csv", arg)

    def createCSV(self, fileName):
        return c_fileAccess(self.folderName + "/" + fileName + ".csv")


    def readLineFromEnd(self, filename, nLines, nOfCollumns):
        lines = []
        cnt = 1

        nOfCollumns=nOfCollumns-1

        with FileReadBackwards(self.folderName + "/" + filename, encoding="utf-8") as frb:
            l = "123"
            while l:
                l = frb.readline()
                if l.count(",") >= nOfCollumns:
                    if cnt <= nLines:
                        lines.append(l)
                    else:
                        break
                    cnt += 1
        return lines

    def getCSVValuesFromEnd(self, filename, nLines, nOfCollumns):
        lines = self.readLineFromEnd(filename, nLines+1, nOfCollumns)
        nOfCollumns = nOfCollumns-1
        values = [ [] for _ in range(nLines)]
        for line in lines[1:]:
            if line.count(',') >= nOfCollumns:
                l = line.split(',')
                i = 0
                for n in l:
                    value = 0
                    try:
                        value = float(n)
                    except:
                        pass
                    values[i].append(value)
                    i += 1







        return values




    def fileInFolderExists(self, fileName):
        my_file = Path(self.folderName + "/" + fileName)
        if my_file.is_file():
            return True
        return False
        pass


    def logToFile(self, file, data, timestamp = True):

        now = datetime.datetime.now()
        if timestamp:
            logDate = "[{}/{}/{} {}h{}m{}.{:3.0f}s] ".format(now.day, now.month, now.year, now.hour, now.minute, now.second, now.microsecond/1000)
            file.appendToFile(logDate)
        file.appendToFile(data)
        if timestamp:
            file.appendToFile('\n')

    def logToCSV(self,file, content):
        line = ""
        if type(content) != list:
            raise ValueError
        length = len(content)
        counter = 1
        for x in content:
            line += str(x)
            file.appendToFile(str(x))
            if counter != length:
                file.appendToFile(',')
                line += ','
            counter += 1
        file.appendToFile("\n")
        return line

