from tkinter import *
import tkinter as tk
from tkinter import ttk

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math

class c_livePlot():

    def __init__(self, root, folderObject, fileName, numberofSamples = 1000, yaxes = [-1000, 1000], xaxes = [-1000, 1000], row = 0, column = 0, hasX = False):
        self.root = root
        self.folderObject = folderObject
        self.fileName = fileName
        self.hasX = hasX

      #------------------ Liveplot related --------------------------------------------

        #self.file = None
        self.fig = plt.figure()

        self.startRow = row
        self.startColumn = column
        self.root.grid_rowconfigure(8+self.startRow, weight=1)
        self.root.grid_columnconfigure(0+self.startColumn, weight=1)

        self.canvas = FigureCanvasTkAgg(self.fig, self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row = 8+self.startRow, column=0 +self.startColumn, sticky = E+S+N+W, rowspan=20, columnspan = 10)





        toolbarFrame = Frame(master=self.root)
        toolbarFrame.grid(row=7+self.startRow,column=0+self.startColumn)
        toolbar = NavigationToolbar2Tk(self.canvas,toolbarFrame)
        toolbar.update()


        self.ax1 = self.fig.add_subplot(1,1,1, picker=True)


        self.graphEnable = False

        self.numberOfSamples = numberofSamples
        self.ax1.clear()
        self.ax1.set_ylim(yaxes)
        self.ax1.set_xlim(xaxes)

        self.ys =  [0]*self.numberOfSamples
        self.xs = [0]*self.numberOfSamples

        self.line = self.ax1.plot(self.xs, self.ys, 'ro', label = "X,Y", linewidth=1, markersize=1)[0]
        self.arrow = self.ax1.arrow(0, 0, 1, 1, head_width=50, head_length=20, fc='k', ec='k')

        #print(type(self.line))

        self.ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=5)

        self.entryNumberSamples = tk.Entry(self.root)
        self.entryNumberSamples["textvariable"] = self.numberOfSamples
        self.entryNumberSamples.insert(0,self.numberOfSamples)
        self.entryNumberSamples.bind('<Key-Return>', self.updateNumberSamples)
        self.entryNumberSamples.grid(row=0+self.startRow,column=0+self.startColumn, sticky = E+W)
        self.labelNumberSamples = tk.Label(self.root, text = "Samples on screen")
        self.labelNumberSamples.grid(row=1+self.startRow,column=0+self.startColumn, sticky = E+W)


        self.refreshRate = 10


        self.entryRefreshRate = tk.Entry(self.root)
        #self.entryRefreshRate["textvariable"] = self.refreshRate
        self.entryRefreshRate.insert(0,self.refreshRate)
        self.entryRefreshRate.bind('<Key-Return>', self.updateRefreshRate)
        self.entryRefreshRate.grid(row=2+self.startRow,column=0+self.startColumn, sticky = E+W)
        self.labelRefreshRate = tk.Label(self.root, text = "Refresh rate (60hz max)")
        self.labelRefreshRate.grid(row=3+self.startRow,column=0+self.startColumn, sticky = E+W)

        self.buttonScroll = ttk.Button(self.root,text="Start/Stop scroll", command=self.toggleScroll)
        self.buttonScroll.grid(row = 4+self.startRow, column=0 +self.startColumn, sticky = N)
        self.automaticScroll = True


        #self.animCounter = 0
        self.ani = animation.FuncAnimation(self.fig, self.animate, fargs=(self.ys,self.xs), interval=100, frames=100)
        self.simulator = 0
        self.increment = 5
        self.redraw = False
        #-----------------------END Liveplot related---------------------------------------
        pass


    def animate(self, i, ys, xs):
        self.simulator += self.increment
        if self.simulator >= 1000 or self.simulator <= -1000:
            self.increment = self.increment*-2

        if self.automaticScroll:
            #values = self.folderObject.getCSVValuesFromEnd(self.fileName, nLines = 1, nOfCollumns = 2)
            nColumns = 4

            values = self.folderObject.getCSVValuesFromEnd(self.fileName, nLines=self.numberOfSamples, nOfCollumns=nColumns)
            #print(values)



            ys[2] = (values[2])
            #ys[2].extend([2]*(self.numberOfSamples-len(values[2])))
            #ys[2].reverse()

            xs = (values[1])
            #xs.extend([1]*(self.numberOfSamples-len(values[1])))
            #xs.reverse()
            self.line.set_ydata(ys[2])
            self.line.set_xdata(xs)


            phi = 0
            if values[3]:
                phi = values[3][0]
                #print(phi)



            dx = math.cos(phi)*1;
            dy = math.sin(phi)*1;

            if values[3]:
                self.arrow.remove()
                self.arrow = self.ax1.arrow(xs[0], ys[2][0], dx, dy, head_width=50, head_length=20, fc='k', ec='k')

            if(self.redraw == True):
                plt.ion()
                self.redraw = False
        return self.line
        pass
    #=======================================
    # update refresh rate
    #=======================================
    def RefreshRate_goGreen(self):
        self.entryRefreshRate.config({"background": "Green"})
    def RefreshRate_goRed(self):
        self.entryRefreshRate.config({"background": "Red"})
    def updateRefreshRate(self, event):
        self.entryRefreshRate.config({"background": "White"})
        number = self.entryRefreshRate.get()
        try:
            refreshRate = float(number)
            if refreshRate > 0 and refreshRate <= 60:
                self.refreshRate = refreshRate
                self.ani.event_source.interval = 1000/self.refreshRate
                self.root.after(100, self.RefreshRate_goGreen)
            else:
                self.root.after(100, self.RefreshRate_goRed)
        except:
            self.root.after(100, self.RefreshRate_goRed)
            pass
    #=======================================
    # END refresh rate
    #=======================================

    #=======================================
    # update Number of Samples
    #=======================================
    def NumberSamples_goGreen(self):
        self.entryNumberSamples.config({"background": "Green"})
    def NumberSamples_goRed(self):
        self.entryNumberSamples.config({"background": "Red"})
    def updateNumberSamples(self, event):
        self.entryNumberSamples.config({"background": "White"})
        number = self.entryNumberSamples.get()
        try:
            numberOfSamples = int(number)
            if numberOfSamples > 0:
                self.numberOfSamples = numberOfSamples
                #self.xs = [0]*numberOfSamples
                #values = self.folderObject.getCSVValuesFromEnd(self.fileName, nLines = self.numberOfSamples, nOfCollumns = 2)

                #self.line.set_xdata(self.xs)
                #self.ys = ([0]*self.numberOfSamples)

                self.redraw = True
                #plt.show(block = False)
                self.root.after(100, self.NumberSamples_goGreen)
            else:
                self.root.after(100, self.NumberSamples_goRed)
        except:
            self.root.after(100, self.NumberSamples_goRed)
            pass
    #=======================================
    # END update Number of Samples
    #=======================================


    def toggleScroll(self):
        if self.automaticScroll:
            self.automaticScroll = False
        else:
            self.automaticScroll = True



    def stop(self):
        plt.close('all')
