
class c_callback():

    def __init__(self):

        print("created generic callback")
        self.sendCounterMax = 10
        #size in bytes
        self.formatsSize = {"int32_t" : 4, "uint32_t" : 4, "float" : 4, "double" : 8, "int64_t" : 8, "int16_t": 2,
                            "uint8_t": 1, "uint32_t_Bits":4, "uint16_t": 2}

        #function to convert each data type from bytes
        #uint8 doesn't do anything really.
        self.formatsConversion = {"int32_t" : self.int32_from_bytes, "uint32_t" : self.int_from_bytes,
                             "float" : self.float_from_bytes, "double" : self.double_from_bytes,
                              "int64_t": self.int64_from_bytes,
                              "int16_t": self.int16_from_bytes,
                              "uint8_t": self.uint8_from_bytes,
                              "uint32_t_Bits":self.uint32_Bits_from_bytes,
                              "uint16_t": self.uint16_from_bytes,}


    def uint32_Bits_from_bytes(self, inBytes):
        res = ""

        value1 = inBytes[3] & 0xFF
        value2 = (inBytes[3] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        value1 = inBytes[2] & 0xFF
        value2 = (inBytes[2] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        value1 = inBytes[1] & 0xFF
        value2 = (inBytes[1] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        value1 = inBytes[0] & 0xFF
        value2 = (inBytes[0] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        return res

    def int_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8
        return res

    def float_from_bytes(self, inbytes):
        bits = self.int_from_bytes(inbytes)
        mantissa = ((bits&8388607)/8388608.0)
        exponent = (bits>>23)&255
        sign = 1.0 if bits>>31 ==0 else -1.0
        if exponent != 0:
            mantissa+=1.0
        elif mantissa==0.0:
            return sign*0.0
        return sign*pow(2.0,exponent-127)*mantissa

    def double_from_bytes(self,inbytes):
        b = bytes(inbytes)

        value =  str(  struct.unpack('d', b)[0] )#struct.unpack('f', b)
        return value

    def int32_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8

        if res > (2**32)/2:
            res = res - (2**32)
        return res

    def int64_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8

        if res > (2**64)/2:
            res = res - (2**64)
        return res

    def int16_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8

        if res > (2**16)/2:
            res = res - (2**16)
        return res

    def uint8_from_bytes(self, inbytes):
        return inbytes[0]
    def uint16_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8
        return res
    def callback(self, content, queueInput, queueOutput):
        print("default callback")
        queueOutput.put("==================\n")
        queueOutput.put("default callback\n")
        queueOutput.put("string content")
        queueOutput.put(str(content))
        queueOutput.put("==================")


    def getValue(self, content, format):
        size = self.formatsSize[format]
        value = self.formatsConversion[format](content[:size])

        return [value, size]


    def setSendCounterMax(self, value):
        if type(value) == 'integer':
            self.sendCounterMax = value
        else:
            raise ValueError

    def setFolderName(self, folderObject, file):
        self.sample = 0
        self.folderObject = folderObject
        self.file = file
#====================================




class c_callback_MotorSpeed(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "MotorsSpeed001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float", "float"]

        dataNames = ["time", "speedMotor1", "speedMotor2"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Motors Speed ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend


class c_callback_Gyro(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "Gyro001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "int32_t", "int32_t", "int32_t"]

        dataNames = ["time", "Xgyro", "Ygyro", "Zgyro"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Gyro ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_Acc(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "Acc001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "int32_t", "int32_t", "int32_t"]

        dataNames = ["time", "Xacc", "Yacc", "Zacc"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Acc ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_rawAccGyro(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "rawAccGyro"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["int16_t", "int16_t", "int16_t", "int16_t", "int16_t", "int16_t"]

        dataNames = ["Xacc", "Yacc", "Zacc", "Xgyro", "Ygyro", "Zgyro"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== rawAccGyro ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_Orientation(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "Orientation001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float"]

        dataNames = ["time", "Pitch"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Orientation==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend
class c_callback_pythonMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "pythonMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float", "float", "float"]

        dataNames = ["time", "Pitch", "M1", "M2"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Python Message  ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_poseMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "poseMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float", "float", "float"]

        dataNames = ["time", "x", "y", "phi"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Pose Message  ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_RPLidarMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "RPLidarMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float", "float", "float", "float", "float", "float"]

        dataNames = ["time", "distance", "angle", "quality", "x", "y", "phi"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        if(1 == 0):
            toSend = ""
            toSend += "==============================" + "\n"
            toSend += "== RPLidar Message  ==" + "\n"
            for x, data in zip(dataNames, listData):
                toSend += x + " = " + data + "\n"
            toSend += "==============================" + "\n"
        #print(toSend);
        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_mapCellOccupancyMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "mapCellOccupancyMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "uint32_t", "uint32_t", "float"]

        dataNames = ["time", "x", "y", "logsOccupancy"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        if(1 == 0):
            toSend = ""
            toSend += "==============================" + "\n"
            toSend += "== mapCellOccupancyMessage Message  ==" + "\n"
            for x, data in zip(dataNames, listData):
                toSend += x + " = " + data + "\n"
            toSend += "==============================" + "\n"
        #print(toSend);
        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend


class c_callback_histogramMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "histogramMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float", "float", "float", "float",
                      "float"]

        dataNames = ["time",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "sector1", "sector1", "sector1", "sector1",
                     "phi"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        if(1 == 0):
            toSend = ""
            toSend += "==============================" + "\n"
            toSend += "== mapCellOccupancyMessage Message  ==" + "\n"
            for x, data in zip(dataNames, listData):
                toSend += x + " = " + data + "\n"
            toSend += "==============================" + "\n"
        #print(toSend);
        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_EncoderMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "EncoderMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "int16_t", "int16_t"]

        dataNames = ["time", "left_count", "right_count"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        if(1 == 0):
            toSend = ""
            toSend += "==============================" + "\n"
            toSend += "== RPLidar Message  ==" + "\n"
            for x, data in zip(dataNames, listData):
                toSend += x + " = " + data + "\n"
            toSend += "==============================" + "\n"
        #print(toSend);
        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend
