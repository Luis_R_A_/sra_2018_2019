/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "arm_math.h"
#include "math.h"
#include "Robot.h"
#include "Robot_Parameters.h"
#include "logging.h"
#include "rplidar_a1.h"
#include "VFF_VFH.h"
#include "Mapping.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim7;
TIM_HandleTypeDef htim13;

UART_HandleTypeDef huart4;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */
UART_HandleTypeDef *huart_interface = &huart4;
volatile uint32_t error_periodicTime_hasPassed = 0;
volatile uint32_t periodicTime_hasPassed = 0;
Robot_Platform_Typedef Platform;
float Pose_error_integral = 0;
float T = 0.02;

extern uint8_t mapArray[80][80];

RPLidarMeasurement testMeasurement;

//#define num_samples 9000
//RPLidarMeasurement Calib_Measurement[num_samples];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM13_Init(void);
static void MX_UART4_Init(void);
static void MX_TIM7_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  MX_TIM13_Init();
  MX_UART4_Init();
  MX_TIM7_Init();
  MX_USART1_UART_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 1024);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 360;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 89;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 1500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 44999;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 22499;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 89;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 49999;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief TIM13 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM13_Init(void)
{

  /* USER CODE BEGIN TIM13_Init 0 */

  /* USER CODE END TIM13_Init 0 */

  /* USER CODE BEGIN TIM13_Init 1 */

  /* USER CODE END TIM13_Init 1 */
  htim13.Instance = TIM13;
  htim13.Init.Prescaler = 0;
  htim13.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim13.Init.Period = 8999;
  htim13.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim13.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim13) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OnePulse_Init(&htim13, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM13_Init 2 */

  /* USER CODE END TIM13_Init 2 */

}

/**
  * @brief UART4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART4_Init(void)
{

  /* USER CODE BEGIN UART4_Init 0 */

  /* USER CODE END UART4_Init 0 */

  /* USER CODE BEGIN UART4_Init 1 */

  /* USER CODE END UART4_Init 1 */
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 921600;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART4_Init 2 */

  /* USER CODE END UART4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 921600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|PROBE1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PROBE2_GPIO_Port, PROBE2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : START_BUTTON_Pin */
  GPIO_InitStruct.Pin = START_BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(START_BUTTON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PROBE2_Pin */
  GPIO_InitStruct.Pin = PROBE2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(PROBE2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PROBE1_Pin */
  GPIO_InitStruct.Pin = PROBE1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(PROBE1_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART3 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}
int _write( int file, char *ptr, int len ){


    int i;




    for(i = 0 ; i < len ; i++){


        HAL_UART_Transmit(&huart2,(uint8_t*)&ptr[i],1,10);


    }




    char ch='\r';


    HAL_UART_Transmit(&huart2,(uint8_t*)&ch,1,10);

    return 0;
}

//void testMaxSpeed()
//{
//	if(newSpeed != speed)
//	{
//	speed = newSpeed;
//
//	}
//	setSpeeds(speed, speed);
//	/*newSpeed+=increment;
//	if(newSpeed > 200 || newSpeed < -200)
//	  increment *= -1;*/
//
//	getCountsAndReset(&countsLeft, &countsRight);
//	float speedLeft = calculateSpeed_cm_s(countsLeft, 0.020);
//	float speedRight = calculateSpeed_cm_s(countsRight, 0.020);
//	float angularSpeedLeft = calculateWheelAngularSpeed_rad_s(countsLeft, 0.020);
//	float angularSpeedRight = calculateWheelAngularSpeed_rad_s(countsRight, 0.020);
//	lastCountLeft = countsLeft;
//	lastCountRight = countsRight;
//
//	printf("speedLeft =%6.2f   speedRight=%6.2f; angularSpeedLeft =%6.2f   angularSpeedRight=%6.2f;\r\n",
//			speedLeft, speedRight, angularSpeedLeft, angularSpeedRight);
//	osDelay(20);
//}
//
//float newSpeed_rad = 0, speed_rad = 0;
//void testMaxSpeed_rad_s()
//{
//	if(newSpeed_rad != speed_rad)
//	{
//		speed_rad = newSpeed_rad;
//
//	}
//	updateMotorSpeed_rad_s(speed_rad, speed_rad);
//	/*newSpeed+=increment;
//	if(newSpeed > 200 || newSpeed < -200)
//	  increment *= -1;*/
//
//	getCountsAndReset(&countsLeft, &countsRight);
//	float speedLeft = calculateSpeed_cm_s(countsLeft, 0.020);
//	float speedRight = calculateSpeed_cm_s(countsRight, 0.020);
//	float angularSpeedLeft = calculateWheelAngularSpeed_rad_s(countsLeft, 0.020);
//	float angularSpeedRight = calculateWheelAngularSpeed_rad_s(countsRight, 0.020);
//	lastCountLeft = countsLeft;
//	lastCountRight = countsRight;
//
//	printf("speedLeft =%6.2f; speedRight=%6.2f; angularSpeedLeft =%6.2f; angularSpeedRight=%6.2f;\r\n",
//			speedLeft, speedRight, angularSpeedLeft, angularSpeedRight);
//	//osDelay(20);
//}

int16_t speedLeft = 0, speedRight = 0;
void callbackRemote(uint8_t content[])
{

	volatile s_logging_remoteControlMessage message;
	memcpy((void*)&message, (const void *)content, sizeof(s_logging_remoteControlMessage));
	speedLeft = message.speedLeft;
	speedRight = message.speedRight;



}
int firstRun = 1;
s_logging_desiredPoseMessage desiredPose = {0,0,0};
extern gridPosition desiredTarget;
void callback_desiredPose(uint8_t content[])
{
	//volatile s_logging_desiredPoseMessage message;
	memcpy((void*)&desiredPose, (const void *)content, sizeof(s_logging_desiredPoseMessage));
	desiredTarget = VFF_getRobotGridPosition(desiredPose.x, desiredPose.y);
	firstRun = 1;
}
s_logging_desiredABCVMessage desiredABCV = {0,0,0,0};
void callback_desiredABCV(uint8_t content[])
{
	//volatile s_logging_desiredABCVMessage message;
	memcpy((void*)&desiredABCV, (const void *)content, sizeof(s_logging_desiredABCVMessage));
}

int8_t desiredLaw = 0;
void callback_desiredLaw(uint8_t content[])
{
	volatile s_logging_startDesiredLaw message;
	memcpy((void*)&message, (const void *)content, sizeof(s_logging_startDesiredLaw));
	desiredLaw = message.lawNumber;
	firstRun = 1;
}

void callback_desiredGains(uint8_t content[])
{
	//volatile s_logging_setControlGains message;
	memcpy((void*)&(Platform.gains), (const void *)content, sizeof(s_logging_setControlGains));


}


void callback_setPose(uint8_t content[])
{
	volatile s_logging_setPoseMessage message;
	memcpy((void*)&(message), (const void *)content, sizeof(s_logging_setPoseMessage));

	Platform.x = message.x;
	Platform.y = message.y;
	Platform.phi = message.phi;
}


#define UART_RX_SIZE 255
volatile uint8_t rxBuffer[UART_RX_SIZE];

typedef struct
{
	uint8_t *buffer;
	uint16_t head;
	uint16_t tail;
	uint16_t spaceAvailable;
	uint16_t bytesAvailable;
}s_circularBuffer;

s_circularBuffer rxCircularBuffer = {rxBuffer, 0, 0, UART_RX_SIZE, 0};
void UART_Receive()
{


	  if((__HAL_UART_GET_FLAG(huart_interface, UART_FLAG_RXNE) ? SET : RESET) == SET)
	  {
		  //uint16_t uhMask;
		  //UART_MASK_COMPUTATION(&huart4);
		  //uhMask = huart4.Mask;
		  while((__HAL_UART_GET_FLAG(huart_interface, UART_FLAG_RXNE) ? SET : RESET) == SET)
		  {
			  if(rxCircularBuffer.spaceAvailable)
			  {
				  rxCircularBuffer.buffer[rxCircularBuffer.head] = (uint8_t)(huart_interface->Instance->DR);
				  rxCircularBuffer.head++;
				  rxCircularBuffer.spaceAvailable--;
				  rxCircularBuffer.bytesAvailable++;
				  if(rxCircularBuffer.head > UART_RX_SIZE)
					  rxCircularBuffer.head = 0;
			  }
			  else{
				  volatile uint8_t temp = huart_interface->Instance->DR;
			  }
		  }

	  }
}

int32_t bufferReadByte(uint8_t *byte)
{
	if( rxCircularBuffer.bytesAvailable == 0)
		return 0;

	*byte = rxCircularBuffer.buffer[rxCircularBuffer.tail];
	rxCircularBuffer.tail++;
	if(rxCircularBuffer.tail > UART_RX_SIZE)
		rxCircularBuffer.tail = 0;
	rxCircularBuffer.bytesAvailable--;
	rxCircularBuffer.spaceAvailable++;

	return 1;

}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN 5 */
	initRobot(&hi2c1, &htim13, &Platform);

	// Initial Platform configurations
	Platform.T = 0.02; 						//set control period
	setMaxAngularSpeeds_rad_s(56.7,56.9);	//set motors max speed
	setMinAngularSpeeds_rad_s(0.0, 0.0);	//setMinAngularSpeeds_rad_s(5.0, 5.0);	//set motors min speed
	setMaxLimitAngularSpeeds_rad_s(10,10);
	updateMotorSpeed_rad_s(0,0);			//stop motors

	// Set initial conditions for test purposes
	Platform.x = 2000;
	Platform.y = 2000;
	Platform.phi = 0;


	logging_registerCallback(callbackRemote, logging_remoteControlMessage);
	logging_registerCallback(callback_desiredPose, logging_desiredPoseMessage);
	logging_registerCallback(callback_desiredABCV, logging_desiredABCVMessage);
	logging_registerCallback(callback_desiredLaw, logging_startDesiredLaw);
	logging_registerCallback(callback_desiredGains, logging_setControlGains);
	logging_registerCallback(callback_setPose, logging_setPoseMessage);
	SET_BIT(huart_interface->Instance->CR1, USART_CR1_RXNEIE);


	RPLidar_begin(&huart1);
	SET_BIT(huart1.Instance->CR1, USART_CR1_RXNEIE);

	rplidar_response_device_info_t info;
	getDeviceInfo(&info, 500);
	rplidar_response_device_health_t health;
	getHealth(&health ,500);
	startScan(1, 500);

	int16_t temp1,temp2;
	getCountsAndReset(&temp1, &temp2); //do a dummy read to clear old encoder values




	while(HAL_GPIO_ReadPin(START_BUTTON_GPIO_Port,START_BUTTON_Pin) == 1)
	{
		uint8_t byte = 0;
		if(bufferReadByte(&byte))
		{
			if(loggin_parseMessage(byte))
			{

			}
		}
	}


	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);

	osDelay(2000);

	HAL_TIM_Base_Start_IT(&htim7);//start timer for constant sample rate

	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);



	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

	/*
	 * 	RPLidar Calibration
	 */
//	volatile uint32_t counterRPLidar = 0;
//
//	while(counterRPLidar < num_samples)
//	{
//
//		if(getCurrentPoint(&testMeasurement) == 0)
//		{
//			if((testMeasurement.distance <= 250) && (testMeasurement.distance != 0))
//			{
//				Calib_Measurement[counterRPLidar].distance = testMeasurement.distance;
//				Calib_Measurement[counterRPLidar].angle = testMeasurement.angle;
//				Calib_Measurement[counterRPLidar].quality = testMeasurement.quality;
//				counterRPLidar++;
//			}
//		}
//	}
//
//	int i;
//	s_logging_RPLidarMessage message;
//	for(i = 0; i < num_samples; i++)
//	{
//		message = logging_getRPLidarMessage(Calib_Measurement[i].distance, Calib_Measurement[i].angle, Calib_Measurement[i].quality, 0, 0, 0);
//		logging_sendMessage(huart_interface, logging_RPLidarMessage, (uint8_t *)&message);
//		HAL_Delay(3);
//	}
//
//	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 1);
//	while(1);
	/*
	 *  End - RPLidar Calibration
	 */
#ifdef UPDATE_MAP
	int i = 0;
	while (i < 4000)
	{
		if(getCurrentPoint(&testMeasurement) == 0)
		{
			i++;
			//convert pol to cart
			float angle = M_PI*(360.0f-testMeasurement.angle)/180.0f;
			float point_x = testMeasurement.distance*arm_cos_f32(angle);
			float point_y = testMeasurement.distance*arm_sin_f32(angle);

			//Transformation
			float x = (0.0099714380f*point_x) + (0.9999502840f*point_y) - 33.6835f;
			float y = (0.0099714380f*point_y) - (0.9999502840f*point_x) - 6.8362f;

			//convert cart to pol
			float r = 0;
			arm_sqrt_f32( (powf(x,2.0f) + powf(y,2.0f)), &r);
			testMeasurement.distance = r;
			testMeasurement.angle = atan2f(y,x);




			s_LidarMeasurement lidarMeasurement;
			lidarMeasurement.distance_mm_lidar = testMeasurement.distance;
			lidarMeasurement.theta_lidar = testMeasurement.angle;
			lidarMeasurement.quality_lidar = testMeasurement.quality;
			lidarMeasurement.x_robot = Platform.x;
			lidarMeasurement.y_robot = Platform.y;
			lidarMeasurement.phi_robot = Platform.phi;
			//HAL_GPIO_WritePin(PROBE2_GPIO_Port, PROBE2_Pin, 1);

			updateMapGivenZ(lidarMeasurement);



			//s_logging_RPLidarMessage message = logging_getRPLidarMessage(testMeasurement.distance, testMeasurement.angle, testMeasurement.quality, Platform.x, Platform.y, Platform.phi);
			//logging_sendMessage(huart_interface, logging_RPLidarMessage, (uint8_t *)&message);
			//HAL_Delay(1);
		}
	}
#endif
	/* Infinite loop */
	for(;;)
	{


		uint8_t byte = 0;
		if(bufferReadByte(&byte))
		{
			if(loggin_parseMessage(byte))
			{

			}
		}





		if(getCurrentPoint(&testMeasurement) == 0)
		{
			//convert pol to cart
			float angle = M_PI*(360.0f-testMeasurement.angle)/180.0f;
			float point_x = testMeasurement.distance*arm_cos_f32(angle);
			float point_y = testMeasurement.distance*arm_sin_f32(angle);

			//Transformation
			float x = (0.0099714380f*point_x) + (0.9999502840f*point_y) - 33.6835f;
			float y = (0.0099714380f*point_y) - (0.9999502840f*point_x) - 6.8362f;

			//convert cart to pol
			float r = 0;
			arm_sqrt_f32( (powf(x,2.0f) + powf(y,2.0f)), &r);
			testMeasurement.distance = r;
			testMeasurement.angle = atan2f(y,x);

			//HAL_GPIO_WritePin(PROBE1_GPIO_Port, PROBE1_Pin, 1);
			odometry_noT();
			//HAL_GPIO_WritePin(PROBE1_GPIO_Port, PROBE1_Pin, 0);
			s_LidarMeasurement lidarMeasurement;
			lidarMeasurement.distance_mm_lidar = testMeasurement.distance;
			lidarMeasurement.theta_lidar = testMeasurement.angle;
			lidarMeasurement.quality_lidar = testMeasurement.quality;
			lidarMeasurement.x_robot = Platform.x;
			lidarMeasurement.y_robot = Platform.y;
			lidarMeasurement.phi_robot = Platform.phi;
			//HAL_GPIO_WritePin(PROBE2_GPIO_Port, PROBE2_Pin, 1);
#ifdef UPDATE_MAP
			updateMapGivenZ(lidarMeasurement);
#endif
			//HAL_GPIO_WritePin(PROBE2_GPIO_Port, PROBE2_Pin, 0);

			uint32_t freq = 44999;
			freq = 0xFFFF - (testMeasurement.distance / 4000.0f * (0xFFFF-44999));
			__HAL_TIM_SET_AUTORELOAD(&htim3, freq);
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, freq/2);

			//s_logging_RPLidarMessage message = logging_getRPLidarMessage(testMeasurement.distance, testMeasurement.angle, testMeasurement.quality, Platform.x, Platform.y, Platform.phi);
			//logging_sendMessage(huart_interface, logging_RPLidarMessage, (uint8_t *)&message);
			//HAL_Delay(1);
		}


		if(HAL_GPIO_ReadPin(START_BUTTON_GPIO_Port,START_BUTTON_Pin) == 0 || desiredLaw == 7)
		{
			setSpeeds(0, 0);
			HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);
			HAL_Delay(1000);
			UnloadMap(huart_interface);
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
			while(desiredLaw == 7)
			{
				uint8_t byte = 0;
				if(bufferReadByte(&byte))
				{
					if(loggin_parseMessage(byte))
					{

					}
				}
			}
		}

		if(periodicTime_hasPassed)
		{
			periodicTime_hasPassed = 0;

			// Update robot position
			if(desiredLaw == 0)
			{
				// Stop mode
				setSpeeds(0, 0);
			}
			else if(desiredLaw == 1)
			{
				// part 1 - Go to (X,Y) with no Phi restrictions
				// use: goTo_XY(X,Y)
				goTo_XY(desiredPose.x, desiredPose.y);
			}
			else if(desiredLaw == 2)
			{
				// part 2 - Follow line defined by ax + by + c = 0, with speed v
				// use: followLine(a, b, c, v)
				followLine(desiredABCV.a, desiredABCV.b, desiredABCV.c, desiredABCV.v);
			}
			else if(desiredLaw == 3)
			{
				// part 3 - Go to (X,Y,Phi)
				// use: goTo_XYPhi(X,Y,Phi)
				goTo_XYPhi(desiredPose.x, desiredPose.y, desiredPose.phi, firstRun);
				firstRun = 0;
			}
			else if(desiredLaw == 4)
			{
				// Remote control
				setSpeeds(speedLeft, speedRight);
			}
			else if(desiredLaw == 5)
			{
				doVFF();
			}
			else if(desiredLaw == 6)
			{
				doVFF_VFH();
				/*
				* Send current pose
				*/
				s_logging_poseMessage message = logging_getPoseMessage(Platform.x, Platform.y, Platform.phi);
				logging_sendMessage(huart_interface, logging_poseMessage, (uint8_t *)&message);
			}
			HAL_GPIO_WritePin(PROBE2_GPIO_Port, PROBE2_Pin, 0);









			//HAL_GPIO_WritePin(PROBE1_GPIO_Port, PROBE1_Pin, 0);

		}
	}
  /* USER CODE END 5 */ 
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM14 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM14) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
