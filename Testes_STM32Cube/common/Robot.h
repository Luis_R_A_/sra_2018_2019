/*
 * Robot.h
 *
 *  Created on: Feb 21, 2019
 *      Author: Luis Afonso
 */

#ifndef ROBOT_H_
#define ROBOT_H_

#include "stm32f4xx_hal.h"
#include "loggingTypes.h"
#define MAX_PWM_LIMIT (300)

typedef struct
{
	// Size
	float radius_mm;

	// Config
	float max_we;
	float max_wd;
	float min_we;
	float min_wd;
	float maxLimit_we;
	float maxLimit_wd;
	float T;

	// Odometry
	int16_t last_left_count;
	float left_angular_speed;
	int16_t last_right_count;
	float right_angular_speed;
	float delta_x;
	float delta_y;
	float delta_phi;
	float delta_s;
	float delta_s_e;
	float delta_s_d;

	// Pose
	float x;
	float y;
	float phi;

	// Gains
	s_logging_setControlGains gains;

}Robot_Platform_Typedef;

extern Robot_Platform_Typedef Platform;

void initRobot(I2C_HandleTypeDef *_i2c_handler, TIM_HandleTypeDef *_timer_handler, Robot_Platform_Typedef *_platform);

/** \brief Sets the speed for both motors.
 *
 * \param leftSpeed A number from -300 to 300 representing the speed and
 * direction of the left motor. Values of -300 or less result in full speed
 * reverse, and values of 300 or more result in full speed forward.
 * \param rightSpeed A number from -300 to 300 representing the speed and
 * direction of the right motor. Values of -300 or less result in full speed
 * reverse, and values of 300 or more result in full speed forward. */
void setSpeeds(int16_t leftSpeed, int16_t rightSpeed);

/** \brief Sets the speed for the left motor.
 *
 * \param speed A number from -300 to 300 representing the speed and
 * direction of the left motor.  Values of -300 or less result in full speed
 * reverse, and values of 300 or more result in full speed forward. */
void setLeftSpeed(int16_t speed);

/** \brief Sets the speed for the right motor.
 *
 * \param speed A number from -300 to 300 representing the speed and
 * direction of the right motor. Values of -300 or less result in full speed
 * reverse, and values of 300 or more result in full speed forward. */
void setRightSpeed(int16_t speed);

/*! Returns the number of counts that have been detected from the both
 * encoders.  These counts start at 0.  Positive counts correspond to forward
 * movement of the wheel of the Romi, while negative counts correspond
 * to backwards movement.
 *
 * The count is returned as a signed 16-bit integer.  When the count goes
 * over 32767, it will overflow down to -32768.  When the count goes below
 * -32768, it will overflow up to 32767. */
void getCounts(int16_t *countsLeft, int16_t *countsRight);

/*! This function is just like getCounts() except it also clears the
 *  counts before returning.  If you call this frequently enough, you will
 *  not have to worry about the count overflowing. */
void getCountsAndReset(int16_t *countsLeft, int16_t *countsRight);

float calculateSpeed_cm_s(int16_t counts, float dt);
float calculateWheelAngularSpeed_rad_s(int16_t counts, float dt);
void updateMotorSpeed_rad_s(float we, float wd);

void setMaxAngularSpeeds_rad_s(float we, float wd);
void setMinAngularSpeeds_rad_s(float we, float wd);
void setMaxLimitAngularSpeeds_rad_s(float we, float wd);


float calculateDistance(float x_initial, float y_initial, float x_final, float y_final);
float distanceToPoint(float x_final, float y_final);
float calculatePoseDiference(float phi_desired);
void odometry_noT(void);
void odometry(void);
void goTo_XY(float x, float y);
void followLine(float line_a, float line_b, float line_c, float speed);
void goTo_XYPhi(float x, float y, float phi, int firstRun);
void goTo_XY_integral(float x, float y);

#endif /* ROBOT_H_ */
