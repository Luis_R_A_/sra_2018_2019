/*
 * labwork4.c
 *
 *  Created on: Jun 4, 2019
 *      Author: Luis Afonso
 */
#include "stm32f4xx_hal.h"

#include "arm_math.h"
#include "math.h"
#include "Robot.h"
#include "Robot_Parameters.h"
#include "logging.h"
#include "rplidar_a1.h"
#include "VFF_VFH.h"
#include "Mapping.h"


#define WHEEL_RADIUS_MM_e 36.0f
#define WHEEL_RADIUS_MM_d 34.0f
#define WHEELS_L_MM_BAD 149.0f //149mm is between the outer face of the wheels
float Sensor_xrobot_offset = -33.6835, Sensor_yrobot_offset = -6.8362; //in mm

float Mahalanobis_limit = 3.48;
float kr = 0.5, kl = 0.5;

float C[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 0.0087}};
arm_matrix_instance_f32 C_matrix = {3, 3, C};
float Q[3][3] = { {0.001, 0, 0}, {0, 0.001, 0}, {0, 0, 0.000076154} };
arm_matrix_instance_f32 Q_matrix = {3, 3, Q};

#define MAX_VALID 20
uint32_t maxNumberOfOdometries = 200, numberOfOdometries = 0;
uint32_t maxNumberOfValidObs = MAX_VALID, numberOfValidObs = 0;
Robot_Platform_Typedef badPose, goodPose;

// pose of robot (x,y,theta) and target cell (xmj,ymj). Copied from matlab
float get_jacobian_gi_x(float x, float y, float theta, float xmj, float ymj)
{

	return (2*x - 2*xmj - (67367*arm_cos_f32(theta))/1000 + (34181*arm_sin_f32(theta))/2500)/(2*sqrtf( powf((x - xmj - (67367*arm_cos_f32(theta))/2000 + (34181*arm_sin_f32(theta))/5000),2) + powf((ymj - y + (34181*arm_cos_f32(theta))/5000 + (67367*arm_sin_f32(theta))/2000),2)));

}
float get_jacobian_gi_y(float x, float y, float theta, float xmj, float ymj)
{
	return -(2*ymj - 2*y + (34181*arm_cos_f32(theta))/2500 + (67367*arm_sin_f32(theta))/1000)/(2*sqrtf(powf((x - xmj - (67367*arm_cos_f32(theta))/2000 + (34181*arm_sin_f32(theta))/5000),2) + powf((ymj - y + (34181*arm_cos_f32(theta))/5000 + (67367*arm_sin_f32(theta))/2000),2)));
}
float get_jacobian_gi_theta(float x, float y, float theta, float xmj, float ymj)
{
	return (2*((34181*arm_cos_f32(theta))/5000 + (67367*arm_sin_f32(theta))/2000)*(x - xmj - (67367*arm_cos_f32(theta))/2000 + (34181*arm_sin_f32(theta))/5000) + 2*((67367*arm_cos_f32(theta))/2000 - (34181*arm_sin_f32(theta))/5000)*(ymj - y + (34181*arm_cos_f32(theta))/5000 + (67367*arm_sin_f32(theta))/2000))/(2*sqrtf(powf((x - xmj - (67367*arm_cos_f32(theta))/2000 + (34181*arm_sin_f32(theta))/5000),2) + powf((ymj - y + (34181*arm_cos_f32(theta))/5000 + (67367*arm_sin_f32(theta))/2000),2)));
}



void get_jacobian_pF(float delta_s, float theta, float delta_theta, arm_matrix_instance_f32 *pF)
{

	(pF->pData)[0]= 1; //[0][0]
	pF->pData[1] = 0; //[0][1]
	pF->pData[2] = -delta_s * arm_sin_f32(theta + delta_theta/2); //[0][2]

	pF->pData[3] = 0; //[1][0]
	pF->pData[4] = 1; //[1][1]
	pF->pData[5] = delta_s * arm_cos_f32(theta + delta_theta/2); //[1][2]


	pF->pData[6] = 0; //[2][0]
	pF->pData[7] = 0; //[2][1]
	pF->pData[8] = 1; //[2][2]



	return;
}

void get_jacobian_uF(float delta_s, float theta, float delta_theta, arm_matrix_instance_f32 *uF)
{

	float temp = theta + delta_theta/2.0f;

	uF->pData[0] = 0.5f * arm_cos_f32(temp) - (delta_s/(2.0f*WHEELS_L_MM_BAD)) * arm_sin_f32(temp); //[0][0]
	uF->pData[1] = 0.5f * arm_cos_f32(temp) + (delta_s/(2.0f*WHEELS_L_MM_BAD)) * arm_sin_f32(temp); //[0][1]

	uF->pData[2] = 0.5f * arm_sin_f32(temp) + (delta_s/(2.0f*WHEELS_L_MM_BAD)) * arm_cos_f32(temp); //[1][0]
	uF->pData[3] = 0.5f * arm_sin_f32(temp) - (delta_s/(2.0f*WHEELS_L_MM_BAD)) * arm_cos_f32(temp); //[1][1]

	uF->pData[4] = 1/WHEELS_L_MM_BAD; //[2][0]
	uF->pData[5] = -1/WHEELS_L_MM_BAD; //[2][1]



	return;
}
void odometry_bad(Robot_Platform_Typedef *_Platform, int16_t counts_e, int16_t counts_d)
{

	//odometry_noT();

	_Platform->last_left_count =counts_e;
	_Platform->last_right_count = counts_d;

	//Platform.left_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_left_count, 1.0f);
	//Platform.right_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_right_count, 1.0f);
	float delta_theta_e = WHEEL_RADIUS_MM_e*((2.0f*PI * (float)_Platform->last_left_count)/(float)ENCODER_PULSES_360);
	float delta_theta_d = WHEEL_RADIUS_MM_d*((2.0f*PI * (float)_Platform->last_right_count)/(float)ENCODER_PULSES_360);

	_Platform->delta_s_e = delta_theta_e;
	_Platform->delta_s_d = delta_theta_d;


	_Platform->delta_phi = (delta_theta_d - delta_theta_e)/WHEELS_L_MM_BAD;
	_Platform->phi += (_Platform->delta_phi);
	_Platform->phi = atan2f(arm_sin_f32(_Platform->phi),arm_cos_f32 (_Platform->phi));

	_Platform->delta_s = (delta_theta_d+delta_theta_e)/2.0f;

	float w_t_2 = _Platform->delta_phi/2.0f;
	float tempx = _Platform->delta_s * arm_cos_f32(_Platform->phi+w_t_2);
	float tempy = _Platform->delta_s * arm_sin_f32(_Platform->phi+w_t_2);


	_Platform->x += (tempx);
	_Platform->y += (tempy);
}

void odometry_good2(Robot_Platform_Typedef *_Platform, int16_t counts_e, int16_t counts_d)
{

	//odometry_noT();

	_Platform->last_left_count =counts_e;
	_Platform->last_right_count = counts_d;

	//Platform.left_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_left_count, 1.0f);
	//Platform.right_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_right_count, 1.0f);
	float delta_theta_e = WHEEL_RADIUS_MM*((2.0f*PI * (float)_Platform->last_left_count)/(float)ENCODER_PULSES_360);
	float delta_theta_d = WHEEL_RADIUS_MM*((2.0f*PI * (float)_Platform->last_right_count)/(float)ENCODER_PULSES_360);

	_Platform->delta_s_e = delta_theta_e;
	_Platform->delta_s_d = delta_theta_d;


	_Platform->delta_phi = (delta_theta_d - delta_theta_e)/WHEELS_L_MM_BAD;
	_Platform->phi += (_Platform->delta_phi);
	_Platform->phi = atan2f(arm_sin_f32(_Platform->phi),arm_cos_f32 (_Platform->phi));

	_Platform->delta_s = (delta_theta_d+delta_theta_e)/2.0f;

	float w_t_2 = _Platform->delta_phi/2.0f;
	float tempx = _Platform->delta_s * arm_cos_f32(_Platform->phi+w_t_2);
	float tempy = _Platform->delta_s * arm_sin_f32(_Platform->phi+w_t_2);


	_Platform->x += (tempx);
	_Platform->y += (tempy);
}

void odometry_good(Robot_Platform_Typedef *_Platform, int16_t counts_e, int16_t counts_d)
{

	//odometry_noT();

	_Platform->last_left_count =counts_e;
	_Platform->last_right_count = counts_d;
	//Platform.left_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_left_count, 1.0f);
	//Platform.right_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_right_count, 1.0f);
	float delta_theta_e = ((2.0f*PI * (float)_Platform->last_left_count)/(float)ENCODER_PULSES_360);
	float delta_theta_d = ((2.0f*PI * (float)_Platform->last_right_count)/(float)ENCODER_PULSES_360);

	_Platform->delta_s_e = 35.0f*delta_theta_e;
	_Platform->delta_s_d = 35.0f*delta_theta_d;
	_Platform->delta_s = 35.0f*(delta_theta_d+delta_theta_e)/2.0f;

	_Platform->delta_phi = ANGULAR_SPEED_TO_PHI*(delta_theta_d - delta_theta_e);
	_Platform->phi += (_Platform->delta_phi);
	_Platform->phi = atan2f(arm_sin_f32(_Platform->phi),arm_cos_f32 (_Platform->phi));

	//Platform.delta_x = ((WHEEL_RADIUS_MM * arm_cos_f32(Platform.phi)) / 2) * (Platform.left_angular_speed + Platform.right_angular_speed);
	//Platform.delta_y = ((WHEEL_RADIUS_MM * arm_sin_f32(Platform.phi)) / 2) * (Platform.left_angular_speed + Platform.right_angular_speed);

	float w_t_2 = _Platform->delta_phi/2.0f;
	float tempx, tempy;

	if(w_t_2 != 0.0f)
	{
		tempx = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				(arm_sin_f32(w_t_2)/w_t_2) * arm_cos_f32(_Platform->phi+w_t_2);
		tempy = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				(arm_sin_f32(w_t_2)/w_t_2) * arm_sin_f32(_Platform->phi+w_t_2);
	}
	else
	{
		tempx = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				arm_cos_f32(_Platform->phi+w_t_2);
		tempy = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				arm_sin_f32(_Platform->phi+w_t_2);
	}

	_Platform->x += (tempx);
	_Platform->y += (tempy);
}

float getProb(float _value_logs)
{
	float probability = 1 - 1/(1+exp(_value_logs));
	return probability;
}

float v_array[MAX_VALID];
arm_matrix_instance_f32 v_matrix = {MAX_VALID, 1, v_array};
float jacobian_g[MAX_VALID][3];
arm_matrix_instance_f32 jacobian_g_matrix = {MAX_VALID, 3, jacobian_g};
float R_array[MAX_VALID][MAX_VALID];
arm_matrix_instance_f32 R_matrix = {MAX_VALID, MAX_VALID, R_array};
float S[MAX_VALID][MAX_VALID];
arm_matrix_instance_f32 S_matrix = {MAX_VALID, MAX_VALID, S};
float copyS[MAX_VALID][MAX_VALID];
arm_matrix_instance_f32 copyS_matrix = {MAX_VALID, MAX_VALID, copyS};
float invS[MAX_VALID][MAX_VALID];
arm_matrix_instance_f32 invS_matrix = {MAX_VALID, MAX_VALID, invS};
float G[3][MAX_VALID];
arm_matrix_instance_f32 G_matrix = {3, MAX_VALID, G};

extern UART_HandleTypeDef huart4;
extern float mapArray[80][80];
float occupied_value = 0.75;
void labwork4_raytracing(s_LidarMeasurement _lidarMeasurement, float *x_mj, float *y_mj)
{
	if(_lidarMeasurement.quality_lidar == 0)
	{
		*x_mj = 8000;
		*y_mj = 8000;
		//_lidarMeasurement.distance_mm_lidar = 125;
		return;
	}

	_lidarMeasurement.distance_mm_lidar = 8000;

    float Sensor_world_x = (-33.6835f)*arm_cos_f32(_lidarMeasurement.phi_robot) - (-6.8362f)*arm_sin_f32(_lidarMeasurement.phi_robot) + _lidarMeasurement.x_robot;
    float Sensor_world_y = (-33.6835f)*arm_sin_f32(_lidarMeasurement.phi_robot) + (-6.8362f)*arm_cos_f32(_lidarMeasurement.phi_robot) + _lidarMeasurement.y_robot;

	// Robot cell position
	int X_Robot_Cell = floorf(Sensor_world_x/cellSize_mm);// * cellSize_mm +cellSize_mm_half;
	int Y_Robot_Cell = floorf(Sensor_world_y/cellSize_mm);// * cellSize_mm +cellSize_mm_half;

	// Measurement angle
	float phi_measurement = _lidarMeasurement.phi_robot + _lidarMeasurement.theta_lidar;

	// Detected cell position
	int X_Measurement_Cell = X_Robot_Cell;
	float temp = _lidarMeasurement.distance_mm_lidar*arm_cos_f32(phi_measurement)/cellSize_mm;
	X_Measurement_Cell+= round(temp);
//	if( temp >= 0.0f)
//	{
//		X_Measurement_Cell+= ceil(temp);
//	}
//	else
//	{
//		X_Measurement_Cell+= floor(temp);
//	}
	//X_Measurement_Cell += roundf(_lidarMeasurement.distance_mm_lidar*arm_cos_f32(phi_measurement)/cellSize_mm);
	int Y_Measurement_Cell = Y_Robot_Cell;
    temp = _lidarMeasurement.distance_mm_lidar*arm_sin_f32(phi_measurement)/cellSize_mm;
    Y_Measurement_Cell+= round(temp);
//	if( temp >= 0.0f)
//	{
//		Y_Measurement_Cell+= ceil(temp);
//	}
//	else
//	{
//		Y_Measurement_Cell+= floor(temp);
//	}
	//Y_Measurement_Cell += roundf(_lidarMeasurement.distance_mm_lidar*arm_sin_f32(phi_measurement)/cellSize_mm);

	// Displacement
	int dX_cells = X_Measurement_Cell - X_Robot_Cell;
	int dY_cells = Y_Measurement_Cell - Y_Robot_Cell;
	float next_x = X_Robot_Cell;
	float next_y = Y_Robot_Cell;

	if(dX_cells == 0){
		while(next_y != Y_Measurement_Cell)
		{
			/*if (next_y < 80.0f && next_y >= 0.0f)
			{
				mapArray[X_Robot_Cell][(int32_t)next_y] = mapArray[X_Robot_Cell][(int32_t)next_y] + l_free + initial_mapData[X_Robot_Cell][(int32_t)next_y];
			}*/
			if (next_y < 80.0f && next_y >= 0.0f)
			{
				if(getProb(mapArray[X_Robot_Cell][(int32_t)next_y]) > occupied_value)
				{
					*x_mj = (X_Robot_Cell)*cellSize_mm + cellSize_mm/2;;
					*y_mj = (next_y)*cellSize_mm + cellSize_mm/2;
					return;
				}
			}
			else
			{
				*x_mj = 8000;
				*y_mj = 8000;
				return;
			}
			if(dY_cells > 0)
				next_y += 1;
			else
				next_y -= 1;
		}

		/*if (next_y < 80 && next_y >= 0)
		{
			mapArray[X_Robot_Cell][(int32_t)next_y] = mapArray[X_Robot_Cell][(int32_t)next_y] + l_ooc + initial_mapData[X_Robot_Cell][(int32_t)next_y];
		}*/
	}else if(dY_cells == 0){
		while(next_x != X_Measurement_Cell)
				{
					/*if (next_x < 80 && next_x >= 0)
					{
						mapArray[(int32_t)next_x][Y_Robot_Cell] = mapArray[(int32_t)next_x][Y_Robot_Cell] + l_free + initial_mapData[(int32_t)next_x][Y_Robot_Cell];
					}*/
					if (next_x < 80 && next_x >= 0)
					{
						if(getProb(mapArray[(int32_t)next_x][Y_Robot_Cell]) > occupied_value)
						{
							*x_mj = (next_x)*cellSize_mm + cellSize_mm/2;;
							*y_mj = (Y_Robot_Cell)*cellSize_mm + cellSize_mm/2;
							return;
						}
					}
					else
					{
						*x_mj = 8000;
						*y_mj = 8000;
						return;
					}
					if(dX_cells > 0)
						next_x += 1;
					else
						next_x -= 1;
				}
				/*if (next_x < 80.0f && next_x >= 0.0f)
				{
					mapArray[(int32_t)next_x][Y_Robot_Cell] = mapArray[(int32_t)next_x][Y_Robot_Cell] + l_ooc + initial_mapData[(int32_t)next_x][Y_Robot_Cell];
				}*/
	}else{
		float diferential = (float)dY_cells/(float)dX_cells;
		float X_Inc, Y_Inc;

		if(fabsf(diferential) < 1){
			if(dX_cells > 0){
				X_Inc = 1;
				Y_Inc = diferential;
			}else{
				X_Inc = -1;
				Y_Inc = -diferential;
			}
		}else if(dY_cells > 0){
			X_Inc = 1/diferential;
			Y_Inc = 1;
		}else{
			X_Inc = -1/diferential;
			Y_Inc = -1;
		}

		int32_t x = roundf(next_x);
		int32_t y = roundf(next_y);
		while((x != X_Measurement_Cell) || (y != Y_Measurement_Cell))
		{
			/*if (x < 80 && y < 80 && x >= 0 && y >= 0)
			{
				mapArray[x][y] = mapArray[x][y] + l_free + initial_mapData[x][y];
			}*/
			if (x < 80 && y < 80 && x >= 0 && y >= 0)
			{
				if(getProb(mapArray[x][(int32_t)y]) > occupied_value)
				{
					*x_mj = (x)*cellSize_mm + cellSize_mm/2;;
					*y_mj = (y)*cellSize_mm + cellSize_mm/2;
					return;
				}
			}
			else
			{
				*x_mj = 8000;
				*y_mj = 8000;
				return;
			}

			next_x += X_Inc;
			next_y += Y_Inc;
			x = roundf(next_x);
			y = roundf(next_y);
		}

		/*if (x < 80 && y < 80 && x >= 0 && y >= 0)
		{
			mapArray[x][y] = mapArray[x][y] + l_ooc + initial_mapData[x][y];
		}*/
	}

	*x_mj = 8000;
	*y_mj = 8000;
	return;
}
typedef int32_t (*func)(void);
extern func operacao[];
extern int32_t op;
int labwork4_firstRun = 1;
float offset[3];

float C_log[205];
int32_t C_log_counter = 0;
int bad_vi = 0;
void labwork4()
{

	if(labwork4_firstRun)
	{
		labwork4_firstRun = 0;
		badPose.x = Platform.x;
		badPose.y = Platform.y;
		badPose.phi = Platform.phi;

		goodPose.x = Platform.x;
		goodPose.y = Platform.y;
		goodPose.phi = Platform.phi;
	}

	//estado 1 - odometria
	//estado 2 - observa��o

	//if estado 1 - fazer odometria (simples)
	if(numberOfOdometries < maxNumberOfOdometries)
	{
		arm_status status;
		int16_t counts_e, counts_d;
		getCountsAndReset(&counts_e, &counts_d);
		odometry_bad(&badPose, counts_e, counts_d);
		odometry_good2(&goodPose, counts_e, counts_d);

		//calculate C
		float jacobian_pF[3][3];
		arm_matrix_instance_f32 jacobian_pF_matrix = {3, 3, jacobian_pF};
		float jacobian_uF[3][2];
		arm_matrix_instance_f32 jacobian_uF_matrix = {3, 2, jacobian_uF};
		get_jacobian_pF(badPose.delta_s, badPose.phi, badPose.delta_phi, &jacobian_pF_matrix);
		get_jacobian_uF(badPose.delta_s, badPose.phi, badPose.delta_phi, &jacobian_uF_matrix);



		float Cu[2][2] = { {kr * fabsf(badPose.delta_s_d), 0}, {0, kl * fabsf(badPose.delta_s_e)}};
		arm_matrix_instance_f32 Cu_matrix = {2, 2, Cu};

//		if(badPose.delta_s_d != 0.0f || badPose.delta_s_e != 0.0f)
//		{
//			while(1);
//		}
		float temp_aux[3][3], temp2_aux[3][3];
		arm_matrix_instance_f32 temp = {3,3,temp_aux}, temp2 = {3,3,temp2_aux};
		status = arm_mat_mult_f32(&jacobian_pF_matrix, &C_matrix, &temp);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_trans_f32(&jacobian_pF_matrix, &temp2);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_mult_f32(&temp, &temp2, &C_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);

		float temp3_aux[3][2], temp4_aux[2][3];
		arm_matrix_instance_f32 temp3 = {3,2,temp3_aux}, temp4 = {2,3,temp4_aux};
		status = arm_mat_mult_f32(&jacobian_uF_matrix, &Cu_matrix, &temp3);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_trans_f32(&jacobian_uF_matrix, &temp4);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_mult_f32(&temp3, &temp4, &temp);
		if(status != ARM_MATH_SUCCESS)
			while(1);

		status = arm_mat_add_f32(&C_matrix, &temp, &temp2);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_add_f32(&temp2, &Q_matrix, &C_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);

		C_log[C_log_counter] = C[1][1];
		C_log_counter++;
		//end of calculate C

		s_logging_poseMessage message = logging_getPoseMessage(badPose.x, badPose.y, badPose.phi);
		logging_sendMessage(&huart4, logging_poseMessage, (uint8_t *)&message);
		s_logging_poseMessage message2 = logging_getPoseMessage(goodPose.x, goodPose.y, goodPose.phi);
		message2.time = 0;
		logging_sendMessage(&huart4, logging_poseMessage, (uint8_t *)&message2);

		HAL_Delay(3);
		numberOfOdometries++;
	}
	//if estado 2 - recolher medidas do lidar.
	//a cada medida obter inova��o
	//ap�s x medidas ou x inova��es v�lidas correr kalman
	else
	{
		arm_status status;

		C_log_counter=0;

		numberOfOdometries = 0;
		numberOfValidObs = 0;

		setSpeeds(1, 1);
		do
		{

			RPLidarMeasurement testMeasurement;
			if(getCurrentPoint(&testMeasurement) == 0)
			{
				HAL_Delay(5);



				float angle = M_PI*(360.0f-testMeasurement.angle-89.4287)/180.0f;
				testMeasurement.angle = angle;


				float oi = testMeasurement.distance; //observation


				float x_mj, y_mj, sensor_x, sensor_y;
				s_LidarMeasurement lidarMeasurement;
				lidarMeasurement.distance_mm_lidar = testMeasurement.distance;
				lidarMeasurement.theta_lidar = testMeasurement.angle;
				lidarMeasurement.quality_lidar = testMeasurement.quality;
				lidarMeasurement.x_robot = badPose.x;
				lidarMeasurement.y_robot = badPose.y;
				lidarMeasurement.phi_robot = badPose.phi;
				labwork4_raytracing(lidarMeasurement, &x_mj, &y_mj);


				sensor_x = Sensor_xrobot_offset*cosf(badPose.phi) - Sensor_yrobot_offset*sinf(badPose.phi) + badPose.x;
				sensor_y = Sensor_xrobot_offset*sinf(badPose.phi) + Sensor_yrobot_offset*cosf(badPose.phi) + badPose.y;

				float oi_estimate = sqrtf(powf(sensor_x - x_mj,2)+powf(sensor_y - y_mj,2));

				float jacobian_gi_x = get_jacobian_gi_x(badPose.x,badPose.y,badPose.phi,x_mj,y_mj);
				float jacobian_gi_y = get_jacobian_gi_y(badPose.x,badPose.y,badPose.phi,x_mj,y_mj);
				float jacobian_gi_theta = get_jacobian_gi_theta(badPose.x,badPose.y,badPose.phi,x_mj,y_mj);
				float jacobian_gi[] = {jacobian_gi_x, jacobian_gi_y, jacobian_gi_theta};
				arm_matrix_instance_f32 jacobian_gi_matrix = {1,3, jacobian_gi};

				float vi = oi - oi_estimate;


				float Ri = powf((oi*0.01 + cellSize_mm/2),2);
				arm_matrix_instance_f32 Ri_matrix = {1, 1, &Ri};

				/* float si = jacobian_g * C * transpose(jacobian_g) + Ri; */
				float temp_aux[3], temp2_aux[3];
				arm_matrix_instance_f32 temp = {1,3,temp_aux}, temp2 = {3,1,temp2_aux};
				status = arm_mat_mult_f32(&jacobian_gi_matrix, &C_matrix, &temp);
				if(status != ARM_MATH_SUCCESS)
					while(1);
				status = arm_mat_trans_f32(&jacobian_gi_matrix, &temp2); //get transpose of jacobian gi
				if(status != ARM_MATH_SUCCESS)
					while(1);
				float si;
				arm_matrix_instance_f32 si_matrix = {1, 1, &si};
				status = arm_mat_mult_f32(&temp, &temp2, &si_matrix); //finish: jacobian_g * C * transpose(jacobian_g)
				if(status != ARM_MATH_SUCCESS)
					while(1);
				status = arm_mat_add_f32(&si_matrix, &Ri_matrix, &si_matrix);
				if(status != ARM_MATH_SUCCESS)
					while(1);


				/* Get quality (Mahalanobis distance)*/
				float Mahalanobis = vi / si * vi; //we can actually ignore matrix math because vi and si are 1x1

				if(Mahalanobis < Mahalanobis_limit)
				{
					if(fabsf(vi) > 100.0f)
					{
						bad_vi = 1;//while(1);
					}
					//add vi to v array
					v_array[numberOfValidObs] = vi;
					jacobian_g[numberOfValidObs][0] = jacobian_gi_x;
					jacobian_g[numberOfValidObs][1] = jacobian_gi_y;
					jacobian_g[numberOfValidObs][2] = jacobian_gi_theta;
					R_array[numberOfValidObs][numberOfValidObs] = Ri;

					//add jacobian_gi_matrix/jacobian_gi to jacobian_G_matrix
					//Add RI to R_vector/R_matrix

					numberOfValidObs++;
				}



			}

		}while(numberOfValidObs < maxNumberOfValidObs);


//		if(bad_vi)
//			while(1);
		//do the kalman

		//S_matrix = jacobian_gi_matrix * C * transpose(jacobian_gi_matrix) + R_matrix;
		//G = C * transpose(jacobian_gi_matrix) * inv(S);
		//Pose = Pose + G*v_array;
		//C = C - G*S*transpose(G);

		//get S
		float temp_aux[MAX_VALID][3], temp2_aux[3][MAX_VALID];
		arm_matrix_instance_f32 temp = {MAX_VALID,3,temp_aux}, temp2 = {3,MAX_VALID,temp2_aux};
		status = arm_mat_mult_f32(&jacobian_g_matrix, &C_matrix, &temp);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_trans_f32(&jacobian_g_matrix, &temp2);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_mult_f32(&temp, &temp2, &S_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_add_f32(&S_matrix, &R_matrix, &S_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);


		//Get G
		float temp3_aux[3][MAX_VALID];
		arm_matrix_instance_f32 temp3 = {3,MAX_VALID,temp3_aux};
		//temp2 should still have transpose(jacobian_g_matrix);
		status = arm_mat_mult_f32(&C_matrix, &temp2, &temp3);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		memcpy(&copyS, &S, 4*MAX_VALID*MAX_VALID);
		status = arm_mat_inverse_f32(&copyS_matrix, &invS_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_mult_f32(&temp3, &invS_matrix, &G_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);

		//get offset to pose

		arm_matrix_instance_f32 offset_matrix = {3,1,offset};
		status =  arm_mat_mult_f32(&G_matrix, &v_matrix, &offset_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);

		badPose.x = badPose.x + offset[0];
		badPose.y = badPose.y + offset[1];
		badPose.phi = badPose.phi + offset[2];

		//get new C
		status = arm_mat_mult_f32(&G_matrix, &S_matrix, &temp3);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_trans_f32(&G_matrix, &temp);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		float temp4_aux[3][3];
		arm_matrix_instance_f32 temp4 = {3,3,temp4_aux};
		status = arm_mat_mult_f32(&temp3, &temp, &temp4);
		if(status != ARM_MATH_SUCCESS)
			while(1);
		status = arm_mat_sub_f32(&C_matrix, &temp4, &C_matrix);
		if(status != ARM_MATH_SUCCESS)
			while(1);

		s_logging_poseMessage message = logging_getPoseMessage(badPose.x, badPose.y, badPose.phi);
		logging_sendMessage(&huart4, logging_poseMessage, (uint8_t *)&message);
		s_logging_poseMessage message2 = logging_getPoseMessage(goodPose.x, goodPose.y, goodPose.phi);
		message2.time = 0;
		logging_sendMessage(&huart4, logging_poseMessage, (uint8_t *)&message2);

		(*operacao[op-1])();
	}












}
