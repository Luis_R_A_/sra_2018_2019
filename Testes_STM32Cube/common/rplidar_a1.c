/*
 * rplidar_a1.c
 *
 *  Created on: Apr 5, 2019
 *      Author: Luis Afonso
 */



#include "rplidar_a1.h"


RPLidarMeasurement  currentMeasurement;
const uint32_t RPLIDAR_SERIAL_BAUDRATE = 115200;
const uint32_t RPLIDAR_DEFAULT_TIMEOUT  = 500;

UART_HandleTypeDef *uart_handler = NULL;

#define RPLidar_UART_RX_SIZE 1024
volatile uint8_t RPLidar_rxBuffer[RPLidar_UART_RX_SIZE];

typedef struct
{
	uint8_t *buffer;
	uint16_t head;
	uint16_t tail;
	uint16_t spaceAvailable;
	uint16_t bytesAvailable;
}s_circularBuffer;

s_circularBuffer RPLidar_rxCircularBuffer = {RPLidar_rxBuffer, 0, 0, RPLidar_UART_RX_SIZE, 0};

uint8_t newMeasureAvailable = 0;

// wait for one sample point to arrive
uint8_t recvPos_g = 0;
rplidar_response_measurement_node_t node_g = {0,0,0};
#define PROBE1_Pin GPIO_PIN_8
#define PROBE1_GPIO_Port GPIOA
u_result parser(uint8_t currentbyte)
{
	if(recvPos_g == 0) // expect the sync bit and its reverse in this byte          {
	{

		uint8_t tmp = (currentbyte>>1);
		if ( (tmp ^ currentbyte) & 0x1 ) {
			// pass
		} else {
			return RESULT_OPERATION_TIMEOUT;
		}
		node_g.sync_quality = currentbyte;

	}
	else if(recvPos_g <= 2)  // expect the highest bit to be 1
	{
		if(recvPos_g == 1)
		{
			if (currentbyte & RPLIDAR_RESP_MEASUREMENT_CHECKBIT) {
					// pass
				} else {
					  recvPos_g = 0;
					  node_g.distance_q2 = 0;
					  node_g.angle_q6_checkbit = 0;
					  node_g.sync_quality = 0;
					  return RESULT_OPERATION_TIMEOUT;
				}
		}
		node_g.angle_q6_checkbit |= (uint16_t)(currentbyte << (8*(recvPos_g-1)));

	}
	else if(recvPos_g <= 4)
	{
		node_g.distance_q2 |= (uint16_t)(currentbyte << (8*(recvPos_g-3)));
	}
	recvPos_g++;


  if (recvPos_g == RPLIDAR_RESPONSE_MEASUREMENT_NODE_SIZE) {

	  // store the data ...
	  currentMeasurement.distance = node_g.distance_q2/4.0f;
	  currentMeasurement.angle = (node_g.angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT)/64.0f;
	  currentMeasurement.quality = (node_g.sync_quality>>RPLIDAR_RESP_MEASUREMENT_QUALITY_SHIFT);
	  currentMeasurement.startBit = (node_g.sync_quality & RPLIDAR_RESP_MEASUREMENT_SYNCBIT);
	  newMeasureAvailable = 1;
	  recvPos_g = 0;
	  node_g.distance_q2 = 0;
	  node_g.angle_q6_checkbit = 0;
	  node_g.sync_quality = 0;
	  //HAL_GPIO_WritePin(PROBE1_GPIO_Port, PROBE1_Pin, 0);
	  return RESULT_OK;
  }

  return RESULT_OPERATION_TIMEOUT;
}

void RPLidar_UART_Receive()
{


	  if((__HAL_UART_GET_FLAG(uart_handler, UART_FLAG_RXNE) ? SET : RESET) == SET)
	  {
		  while((__HAL_UART_GET_FLAG(uart_handler, UART_FLAG_RXNE) ? SET : RESET) == SET)
		  {
			  parser((uint8_t)(uart_handler->Instance->DR));
			  /*if(RPLidar_rxCircularBuffer.spaceAvailable)
			  {
				  RPLidar_rxCircularBuffer.buffer[RPLidar_rxCircularBuffer.head] = (uint8_t)(uart_handler->Instance->DR);
				  RPLidar_rxCircularBuffer.head++;
				  RPLidar_rxCircularBuffer.spaceAvailable--;
				  RPLidar_rxCircularBuffer.bytesAvailable++;
				  if(RPLidar_rxCircularBuffer.head > RPLidar_UART_RX_SIZE)
					  RPLidar_rxCircularBuffer.head = 0;
			  }
			  else{
				  volatile uint8_t temp = uart_handler->Instance->DR;
			  }*/
		  }

	  }
}

int32_t RPLidar_bufferReadByte(uint8_t *byte)
{
	if( RPLidar_rxCircularBuffer.bytesAvailable == 0)
		return 0;

	*byte = RPLidar_rxCircularBuffer.buffer[RPLidar_rxCircularBuffer.tail];
	RPLidar_rxCircularBuffer.tail++;
	if(RPLidar_rxCircularBuffer.tail > RPLidar_UART_RX_SIZE)
		RPLidar_rxCircularBuffer.tail = 0;
	RPLidar_rxCircularBuffer.bytesAvailable--;
	RPLidar_rxCircularBuffer.spaceAvailable++;

	return 1;

}

int32_t RPLidar_serialRead()
{
	uint8_t byte;
	if(RPLidar_bufferReadByte(&byte) == 0)
		return -1;

	return (int32_t)byte;
}

void RPLidar_begin(UART_HandleTypeDef *_uart_handler )
{
	currentMeasurement.angle = 0;
	currentMeasurement.distance = 0;
	currentMeasurement.quality = 0;
	currentMeasurement.startBit = 0;
	uart_handler = _uart_handler;
}


bool isOpen()
{
	if(uart_handler == NULL)
		return 0;
	return 1;
}

u_result getHealth(rplidar_response_device_health_t *healthinfo, uint32_t timeout)
{
    uint32_t currentTs = HAL_GetTick();
    uint32_t remainingtime;

    uint8_t *infobuf = (uint8_t *)&healthinfo;
    uint8_t recvPos = 0;

    rplidar_ans_header_t response_header;
    u_result  ans;


    if (!isOpen()) return RESULT_OPERATION_FAIL;

    {
        if (IS_FAIL(ans = _sendCommand(RPLIDAR_CMD_GET_DEVICE_HEALTH, NULL, 0))) {
            return ans;
        }

        if (IS_FAIL(ans = _waitResponseHeader(&response_header, timeout))) {
            return ans;
        }

        // verify whether we got a correct header
        if (response_header.type != RPLIDAR_ANS_TYPE_DEVHEALTH) {
            return RESULT_INVALID_DATA;
        }

        if ((response_header.size) < RPLIDAR_RESPONSE_DEVICE_HEALTH_SIZE) {
            return RESULT_INVALID_DATA;
        }

        while ((remainingtime=HAL_GetTick() - currentTs) <= timeout) {
            int currentbyte = RPLidar_serialRead();
            if (currentbyte < 0) continue;

            if(recvPos == 0)
            	healthinfo->status = currentbyte;
            else if(recvPos <= 2)
            	healthinfo->error_code |= (currentbyte << (8*(recvPos-1)));
            recvPos++;

            if (recvPos == RPLIDAR_RESPONSE_DEVICE_HEALTH_SIZE) {
                return RESULT_OK;
            }
        }
    }
    return RESULT_OPERATION_TIMEOUT;
}


u_result getDeviceInfo(rplidar_response_device_info_t *info, uint32_t timeout)
{
    uint8_t  recvPos = 0;
    uint32_t currentTs = HAL_GetTick();
    uint32_t remainingtime;
    uint8_t *infobuf = (uint8_t*)&info;
    rplidar_ans_header_t response_header;
    u_result  ans;

    if (!isOpen()) return RESULT_OPERATION_FAIL;

    {
        if (IS_FAIL(ans = _sendCommand(RPLIDAR_CMD_GET_DEVICE_INFO,NULL,0))) {
            return ans;
        }

        if (IS_FAIL(ans = _waitResponseHeader(&response_header, timeout))) {
            return ans;
        }

        // verify whether we got a correct header
        if (response_header.type != RPLIDAR_ANS_TYPE_DEVINFO) {
            return RESULT_INVALID_DATA;
        }


        if (response_header.size < RPLIDAR_RESPONSE_DEVICE_INFO_SIZE) {
            return RESULT_INVALID_DATA;
        }

        while ((remainingtime=HAL_GetTick() - currentTs) <= timeout) {
            int currentbyte = RPLidar_serialRead();
            if (currentbyte<0) continue;
            //infobuf[recvPos++] = currentbyte;
            if(recvPos == 0)
            	info->model = currentbyte;
            else if(recvPos <= 2)
            	info->firmware_version |= (currentbyte << (8*(recvPos-1)));
            else if(recvPos <= 3)
            	info->hardware_version = currentbyte;
            else if(recvPos <= 19)
            	info->serialnum[recvPos-4] = currentbyte;
            recvPos++;
            if (recvPos == RPLIDAR_RESPONSE_DEVICE_INFO_SIZE) {
                return RESULT_OK;
            }
        }
    }

    return RESULT_OPERATION_TIMEOUT;
}



// stop the measurement operation
u_result stop()
{
    if (!isOpen()) return RESULT_OPERATION_FAIL;
    u_result ans = _sendCommand(RPLIDAR_CMD_STOP,NULL,0);
    return ans;
}

u_result startScan(bool force, uint32_t timeout)
{
    u_result ans;

    if (!isOpen()) return RESULT_OPERATION_FAIL;

    stop(); //force the previous operation to stop

    {
        ans = _sendCommand(force?RPLIDAR_CMD_FORCE_SCAN:RPLIDAR_CMD_SCAN, NULL, 0);
        if (IS_FAIL(ans)) return ans;

        // waiting for confirmation
        rplidar_ans_header_t response_header;
        if (IS_FAIL(ans = _waitResponseHeader(&response_header, timeout))) {
            return ans;
        }

        // verify whether we got a correct header
        if (response_header.type != RPLIDAR_ANS_TYPE_MEASUREMENT) {
            return RESULT_INVALID_DATA;
        }

        if (response_header.size < RPLIDAR_RESPONSE_MEASUREMENT_NODE_SIZE) {
            return RESULT_INVALID_DATA;
        }
    }
    return RESULT_OK;
}

// wait for one sample point to arrive
u_result waitPoint(uint32_t timeout)
{
	   uint32_t currentTs = HAL_GetTick();
	   uint32_t remainingtime;
	   rplidar_response_measurement_node_t node = {0,0,0};
	   uint8_t *nodebuf = (uint8_t*)&node;

	   uint8_t recvPos = 0;

	   while ((remainingtime=HAL_GetTick() - currentTs) <= timeout) {
	        int currentbyte = RPLidar_serialRead();
	        if (currentbyte<0) continue;


			if(recvPos == 0) // expect the sync bit and its reverse in this byte          {
			{

				uint8_t tmp = (currentbyte>>1);
				if ( (tmp ^ currentbyte) & 0x1 ) {
					// pass
				} else {
					continue;
				}
				node.sync_quality = currentbyte;

			}
			else if(recvPos <= 2)  // expect the highest bit to be 1
			{
				if(recvPos == 1)
				{
					if (currentbyte & RPLIDAR_RESP_MEASUREMENT_CHECKBIT) {
							// pass
						} else {
							recvPos = 0;
							continue;
						}
				}
				node.angle_q6_checkbit |= (uint16_t)(currentbyte << (8*(recvPos-1)));

			}
			else if(recvPos <= 4)
			{
				node.distance_q2 |= (uint16_t)(currentbyte << (8*(recvPos-3)));
			}
			recvPos++;


		  if (recvPos == RPLIDAR_RESPONSE_MEASUREMENT_NODE_SIZE) {
			  // store the data ...
			  currentMeasurement.distance = node.distance_q2/4.0f;
			  currentMeasurement.angle = (node.angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT)/64.0f;
			  currentMeasurement.quality = (node.sync_quality>>RPLIDAR_RESP_MEASUREMENT_QUALITY_SHIFT);
			  currentMeasurement.startBit = (node.sync_quality & RPLIDAR_RESP_MEASUREMENT_SYNCBIT);
			  return RESULT_OK;
		  }

	   }

	   return RESULT_OPERATION_TIMEOUT;
}





u_result processPoint()
{


	   //uint8_t recvPos = 0;
	   int currentbyte;
	   do{
	        currentbyte = RPLidar_serialRead();
	        if (currentbyte<0) return RESULT_OPERATION_TIMEOUT;

	        if( parser(currentbyte) == RESULT_OK)
	        	return RESULT_OK;



	   }while(currentbyte >= 0);

	   return RESULT_OPERATION_TIMEOUT;
}


int32_t getCurrentPoint(RPLidarMeasurement *_data)
{

	CLEAR_BIT(uart_handler->Instance->CR1, USART_CR1_RXNEIE);
	if(newMeasureAvailable)
	{
		_data->angle = currentMeasurement.angle;
		_data->distance = currentMeasurement.distance;
		_data->quality = currentMeasurement.quality;
		newMeasureAvailable = 0;
		SET_BIT(uart_handler->Instance->CR1, USART_CR1_RXNEIE);
		return 0;
	}
	SET_BIT(uart_handler->Instance->CR1, USART_CR1_RXNEIE);
	return -1;
}

void _RPLidar_seriaWrite(uint8_t *data, uint16_t size)
{
	while (HAL_UART_GetState(uart_handler) != HAL_UART_STATE_READY)
	{
	}
	if(HAL_UART_Transmit(uart_handler, (uint8_t*)data, size, 0xFFFF)!= HAL_OK)
	{
	  /* Transfer error in transmission process */
	  //Error_Handler();
	}

    //}
	return;
}

u_result _sendCommand(uint8_t cmd, uint8_t * payload, size_t payloadsize)
{
    rplidar_cmd_packet_t pkt_header;
    rplidar_cmd_packet_t * header = &pkt_header;
    uint8_t checksum = 0;

    if (payloadsize && payload) {
        cmd |= RPLIDAR_CMDFLAG_HAS_PAYLOAD;
    }

    header->syncByte = RPLIDAR_CMD_SYNC_BYTE;
    header->cmd_flag = cmd;

    // send header first
    _RPLidar_seriaWrite( (uint8_t *)header, 2);

    if (cmd & RPLIDAR_CMDFLAG_HAS_PAYLOAD) {
        checksum ^= RPLIDAR_CMD_SYNC_BYTE;
        checksum ^= cmd;
        checksum ^= (payloadsize & 0xFF);

        // calc checksum
        for (size_t pos = 0; pos < payloadsize; ++pos) {
            checksum ^= ((uint8_t *)payload)[pos];
        }

        // send size
        uint8_t sizebyte = payloadsize;
        _RPLidar_seriaWrite((uint8_t *)&sizebyte, 1);

        // send payload
        _RPLidar_seriaWrite((uint8_t *)&payload, sizebyte);

        // send checksum
        _RPLidar_seriaWrite((uint8_t *)&checksum, 1);

    }

    return RESULT_OK;

}

u_result _waitResponseHeader(rplidar_ans_header_t * header, uint32_t timeout)
{

    uint8_t  recvPos = 0;
    uint32_t currentTs = HAL_GetTick();
    uint32_t remainingtime;
    uint8_t *headerbuf = (uint8_t*)header;
    uint32_t temp = 0;
    while ((remainingtime=HAL_GetTick() - currentTs) <= timeout) {

        int currentbyte = RPLidar_serialRead();
        if (currentbyte<0) continue;
        if(recvPos == 0)
        {
            if (currentbyte != RPLIDAR_ANS_SYNC_BYTE1) {
                continue;
            }
            header->syncByte1 = currentbyte;
            recvPos++;
        }
        else if(recvPos == 1)
        {
            if (currentbyte != RPLIDAR_ANS_SYNC_BYTE2) {
                recvPos = 0;
                continue;
            }
            header->syncByte2 = currentbyte;
            recvPos++;
        }
        else if(recvPos >= 2 && recvPos <= 5 )
        {
        	temp |= (currentbyte << (8*(recvPos-2)) );
        	if(recvPos == 5)
        	{
        		header->size = temp & 0x3FFFFFFF;
        		header->sendMode = temp & 0xC0000000;
        	}
        	recvPos++;
        }
        else if(recvPos == 6)
        {
        	header->type = currentbyte;
        	return RESULT_OK;
        }



        //headerbuf[recvPos++] = currentbyte;

        /*if (recvPos == sizeof(rplidar_ans_header_t)) {
            return RESULT_OK;
        }*/


    }

    return RESULT_OPERATION_TIMEOUT;
}
