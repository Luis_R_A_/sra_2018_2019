/*
 * logging.c
 *
 *  Created on: Nov 1, 2018
 *      Author: Luis Afonso
 */


#include "logging.h"


int32_t _sendMessageDriver(UART_HandleTypeDef *_huart, uint8_t _buffer[], uint32_t _size)
{
	//uint32_t i;
    //for(i = 0 ; i < _size ; i++){
        //HAL_UART_Transmit(_huart,(uint8_t*)&_buffer[i],1,10);
	while (HAL_UART_GetState(_huart) != HAL_UART_STATE_READY)
	{
	}
	if(HAL_UART_Transmit(_huart, (uint8_t*)_buffer, _size, 0xFFFF)!= HAL_OK)
	{
	  /* Transfer error in transmission process */
	  //Error_Handler();
	}

    //}
	return 0;
}

uint8_t buffer2Send[255];
int32_t logging_sendMessage(UART_HandleTypeDef *_huart,logging_messageTypes _messageID, uint8_t _content[])
{



	uint8_t checksum = 0;
	buffer2Send[0] = SYNC1;
	buffer2Send[1] = SYNC2;
	buffer2Send[2] = _messageID;
	//buffer2Send[3] = buffer.sync1;

	checksum = SYNC1;
	checksum += SYNC2;
	checksum += _messageID;


	uint32_t lenght = logging_messagesSizes[_messageID];
	uint32_t i = 0;
	while(lenght)
	{
		buffer2Send[3+i] = _content[i];
		checksum += _content[i];
		i++;
		lenght--;
	}

	buffer2Send[3+i] = checksum;


	return _sendMessageDriver(_huart, buffer2Send, 4+i);


}

uint8_t state = 0;
uint8_t type = 0;
uint8_t buffer[255];
uint8_t byteCounter = 0;
uint8_t checksum = 0;
int32_t loggin_parseMessage(uint8_t byte)
{
	if ( state == 0 && byte == SYNC1)
	{
		state++;
		byteCounter = 0;
		checksum = SYNC1;
	}
	else if(state == 1)
	{
		if(byte == SYNC2)
		{
			state++;
			checksum += SYNC2;
		}
		else
		{
			state = 0;
		}
	}
	else if(state == 2)
	{
		type = byte;
		checksum += type;
		state++;
	}
	else if(state == 3)
	{
		buffer[byteCounter] = byte;
		checksum += byte;
		byteCounter++;
		if(byteCounter >= logging_messagesSizes[type])
		{
			state++;
		}
	}
	else if(state == 4)
	{
		state = 0;
		if(byte == checksum)
		{
			if(list_logging_callback[type])
			{
				(*list_logging_callback[type])(buffer);
			}
			return 1;
		}

		return -1;
	}
	return 0;

}

void logging_registerCallback(logging_callback callback, int32_t type)
{
	if(type < 0 || type >= 30)
		while(1); //block to analyze
	list_logging_callback[type] = callback;
}

s_logging_MotorsSpeedMessage logging_getMotorsSpeedMessage(float _motor1, float _motor2)
{
	s_logging_MotorsSpeedMessage message;
	message.speedMotor1 = _motor1;
	message.speedMotor2 =_motor2;
	message.time = HAL_GetTick();

	return message;
}
s_logging_GyroMessage logging_getGyroMessage(int32_t _X, int32_t _Y, int32_t _Z)
{
	s_logging_GyroMessage message;
	message.X = _X;
	message.Y =_Y;
	message.Z =_Z;
	message.time = HAL_GetTick();

	return message;
}
s_logging_AccMessage logging_getAccMessage(int32_t _X, int32_t _Y, int32_t _Z)
{
	s_logging_AccMessage message;
	message.X = _X;
	message.Y =_Y;
	message.Z =_Z;
	message.time = HAL_GetTick();

	return message;
}

s_logging_OrientationMessage logging_geOrientationMessage(float _pitch)
{
	s_logging_OrientationMessage message;
	message.Pitch = _pitch;
	message.time = HAL_GetTick();

	return message;
}

s_logging_pythonMessage logging_gePythonMessage(float _pitch, float _motor1, float _motor2)
{
	if(_pitch == 0.0)
		_pitch = 0.0000000000000000001;
	if(_motor1 == 0.0)
		_motor1 = 0.0000000000000000001;
	if(_motor2 == 0.0)
		_motor2 = 0.0000000000000000001;
	s_logging_pythonMessage message;
	message.Pitch = _pitch;
	message.speedMotor1 = _motor1;
	message.speedMotor2 = _motor2;
	message.time = HAL_GetTick();



	return message;
}

s_logging_poseMessage logging_getPoseMessage(float x, float y, float phi)
{
	s_logging_poseMessage message;
	message.x = x;
	message.y = y;
	message.phi = phi;
	message.time = HAL_GetTick();

	return message;
}


s_logging_RPLidarMessage logging_getRPLidarMessage(float distance, float angle, float quality,
		float x, float y, float phi)
{
	s_logging_RPLidarMessage message;
	message.distance = distance;
	message.angle = angle;
	message.quality = quality;
	message.x = x;
	message.y = y;
	message.phi = phi;
	message.time = HAL_GetTick();

	return message;
}

s_logging_mapCellOccupancyMessage logging_getMapCellOccupancyMessage(uint8_t x, uint8_t y, float logsOccupancy)
{
	s_logging_mapCellOccupancyMessage message;
	message.time = HAL_GetTick();
	message.x = x;
	message.y = y;
	message.logsOccupancy = logsOccupancy;

	return message;
}

s_logging_histogramMessage logging_getHistogramMessage(float histogram[32], float _phi)
{
	s_logging_histogramMessage message;
	message.time = HAL_GetTick();
	int i;
	for(i = 0; i < 36; i++)
	{
		message.histogram[i] = histogram[i];
	}
	message.phi = _phi;
	return message;
}

s_logging_EncoderMessage logging_getEncoderMessage(int16_t left_count, int16_t right_count)
{
	s_logging_EncoderMessage message;
	message.left_count = left_count;
	message.right_count = right_count;
	message.time = HAL_GetTick();

	return message;
}
