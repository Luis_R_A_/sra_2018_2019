/*
 * VFF.H
 *
 *  Created on: Mar 29, 2019
 *      Author: Luis Afonso
 */

#ifndef VFF_VFH_H_
#define VFF_VFH_H_


#include "stdint.h"

typedef struct
{
	uint8_t posX;
	uint8_t posY;
}gridPosition;

typedef struct
{
	float F_x;
	float F_y;
}vectorForce;

void doVFF();
void doVFF_VFH();

gridPosition VFF_getRobotGridPosition(float x, float y);
vectorForce VFF_calculateFr(gridPosition robotPosition);
vectorForce VFF_calculateFa(gridPosition robotPosition, gridPosition targetPosition);
void VFH_histogram(gridPosition robotPosition);
float VFH_getPhi(gridPosition robotPosition, int* error);


#endif /* VFF_VFH_H_ */
