/*
 * mapping.c
 *
 *  Created on: Apr 14, 2019
 *      Author: Luis Afonso
 */


#include "Mapping.h"
#include "math.h"
#include "logging.h"
#include "loggingTypes.h"
#include "arm_math.h"

extern float mapArray[80][80];
float cellSize_mm_half = cellSize_mm/2.0f;
extern float initial_mapData[80][80];

float l_2_prob(float value)
{
	float probability = 1 - 1/(1+exp(value));
	return probability;
}


/*void begin_initialMap()
{
	int i, j; //counting variables
	for(i = 0; i < 80; i++)
	{
		for(j = 0; j < 80; j++)
		{
			initial_mapData[i][j] = mapArray[i][j];
		}
	}
}*/



/*
 * Here "z" is the measurement of the lidar in mm.
 */
float z_max = 2000.0f; //in mm
float l_0 = 0.0f;
float l_ooc = 0.65f;
float l_free = -0.65f;
const float alpha_cellSize = 50;//in mm
const float beta_lidar = 0.0175f; //1.0f; // in mm
float inverseModel(int32_t mx, int32_t my, s_LidarMeasurement lidarMeasurement)
{
	float x = lidarMeasurement.x_robot;//floorf(lidarMeasurement.x_robot/cellSize_mm) * cellSize_mm +cellSize_mm_half;
	float y = lidarMeasurement.y_robot;//floorf(lidarMeasurement.y_robot/cellSize_mm) * cellSize_mm +cellSize_mm_half;

	float xi = mx * cellSize_mm +cellSize_mm_half;
	float yi = my * cellSize_mm +cellSize_mm_half;
	float phi_robot = lidarMeasurement.phi_robot;
	float theta = lidarMeasurement.theta_lidar;
	float z = lidarMeasurement.distance_mm_lidar;

	float r = 0;// sqrtf(powf(xi-x,2.0f) + powf(yi-y,2.0f));
	arm_sqrt_f32( (powf(xi-x,2.0f) + powf(yi-y,2.0f)), &r);

	if( r >= fminf(z_max, z+alpha_cellSize/2) )
	{
		return l_0;
	}

	float phi = atan2f(yi-y, xi-x) - phi_robot;
	float temp1 = phi - theta;
	float temp = atan2f(arm_sin_f32(temp1), arm_cos_f32(temp1));
	if( fabsf(temp) > beta_lidar/2.0f)
	{
		//return l_0;
	}

	if( z < z_max && fabsf(r - z) <= alpha_cellSize/2.0f)
	{
		return l_ooc;
	}

	//if r <= z
	return l_free;
}




void updateMapGivenZ(s_LidarMeasurement _lidarMeasurement)
{
	if(_lidarMeasurement.quality_lidar == 0)
	{
		//_lidarMeasurement.distance_mm_lidar = 125;
		return;
	}


    float Sensor_world_x = (-33.6835f)*arm_cos_f32(_lidarMeasurement.phi_robot) - (-6.8362f)*arm_sin_f32(_lidarMeasurement.phi_robot) + _lidarMeasurement.x_robot;
    float Sensor_world_y = (-33.6835f)*arm_sin_f32(_lidarMeasurement.phi_robot) + (-6.8362f)*arm_cos_f32(_lidarMeasurement.phi_robot) + _lidarMeasurement.y_robot;

	// Robot cell position
	int X_Robot_Cell = floorf(Sensor_world_x/cellSize_mm);// * cellSize_mm +cellSize_mm_half;
	int Y_Robot_Cell = floorf(Sensor_world_y/cellSize_mm);// * cellSize_mm +cellSize_mm_half;

	// Measurement angle
	float phi_measurement = _lidarMeasurement.phi_robot + _lidarMeasurement.theta_lidar;

	// Detected cell position
	int X_Measurement_Cell = X_Robot_Cell;
	float temp = _lidarMeasurement.distance_mm_lidar*arm_cos_f32(phi_measurement)/cellSize_mm;
	X_Measurement_Cell+= round(temp);
//	if( temp >= 0.0f)
//	{
//		X_Measurement_Cell+= ceil(temp);
//	}
//	else
//	{
//		X_Measurement_Cell+= floor(temp);
//	}
	//X_Measurement_Cell += roundf(_lidarMeasurement.distance_mm_lidar*arm_cos_f32(phi_measurement)/cellSize_mm);
	int Y_Measurement_Cell = Y_Robot_Cell;
    temp = _lidarMeasurement.distance_mm_lidar*arm_sin_f32(phi_measurement)/cellSize_mm;
    Y_Measurement_Cell+= round(temp);
//	if( temp >= 0.0f)
//	{
//		Y_Measurement_Cell+= ceil(temp);
//	}
//	else
//	{
//		Y_Measurement_Cell+= floor(temp);
//	}
	//Y_Measurement_Cell += roundf(_lidarMeasurement.distance_mm_lidar*arm_sin_f32(phi_measurement)/cellSize_mm);

	// Displacement
	int dX_cells = X_Measurement_Cell - X_Robot_Cell;
	int dY_cells = Y_Measurement_Cell - Y_Robot_Cell;
	float next_x = X_Robot_Cell;
	float next_y = Y_Robot_Cell;

	if(dX_cells == 0){
		while(next_y != Y_Measurement_Cell)
		{
			if (next_y < 80.0f && next_y >= 0.0f)
			{
				mapArray[X_Robot_Cell][(int32_t)next_y] = mapArray[X_Robot_Cell][(int32_t)next_y] + l_free + initial_mapData[X_Robot_Cell][(int32_t)next_y];
			}

			if(dY_cells > 0)
				next_y += 1;
			else
				next_y -= 1;
		}

		if (next_y < 80 && next_y >= 0)
		{
			mapArray[X_Robot_Cell][(int32_t)next_y] = mapArray[X_Robot_Cell][(int32_t)next_y] + l_ooc + initial_mapData[X_Robot_Cell][(int32_t)next_y];
		}
	}else if(dY_cells == 0){
		while(next_x != X_Measurement_Cell)
				{
					if (next_x < 80 && next_x >= 0)
					{
						mapArray[(int32_t)next_x][Y_Robot_Cell] = mapArray[(int32_t)next_x][Y_Robot_Cell] + l_free + initial_mapData[(int32_t)next_x][Y_Robot_Cell];
					}

					if(dX_cells > 0)
						next_x += 1;
					else
						next_x -= 1;
				}

				if (next_x < 80.0f && next_x >= 0.0f)
				{
					mapArray[(int32_t)next_x][Y_Robot_Cell] = mapArray[(int32_t)next_x][Y_Robot_Cell] + l_ooc + initial_mapData[(int32_t)next_x][Y_Robot_Cell];
				}
	}else{
		float diferential = (float)dY_cells/(float)dX_cells;
		float X_Inc, Y_Inc;

		if(fabsf(diferential) < 1){
			if(dX_cells > 0){
				X_Inc = 1;
				Y_Inc = diferential;
			}else{
				X_Inc = -1;
				Y_Inc = -diferential;
			}
		}else if(dY_cells > 0){
			X_Inc = 1/diferential;
			Y_Inc = 1;
		}else{
			X_Inc = -1/diferential;
			Y_Inc = -1;
		}

		int32_t x = roundf(next_x);
		int32_t y = roundf(next_y);
		while((x != X_Measurement_Cell) || (y != Y_Measurement_Cell))
		{
			if (x < 80 && y < 80 && x >= 0 && y >= 0)
			{
				mapArray[x][y] = mapArray[x][y] + l_free + initial_mapData[x][y];
			}

			next_x += X_Inc;
			next_y += Y_Inc;
			x = roundf(next_x);
			y = roundf(next_y);
		}

		if (x < 80 && y < 80 && x >= 0 && y >= 0)
		{
			mapArray[x][y] = mapArray[x][y] + l_ooc + initial_mapData[x][y];
		}
	}
}


void UnloadMap(UART_HandleTypeDef *huart)
{
	uint8_t i, j;
	for(i = 0; i < 80; i++)
	{
		for(j = 0; j < 80; j++)
		{
			s_logging_mapCellOccupancyMessage message = logging_getMapCellOccupancyMessage(i,j, mapArray[i][j]);
			logging_sendMessage(huart, logging_mapCellOccupancyMessage, (uint8_t *)&message);
			HAL_Delay(3);

		}
	}

}
