/*
 * Robot.c
 *
 *  Created on: Feb 21, 2019
 *      Author: Luis Afonso
 */

#include "Robot.h"
#include "Robot_Parameters.h"
#include <string.h>
#include "stdlib.h"
#include "arm_math.h"
#include "math.h"

#define TIMEOUTVALUE 1000

//I2C i2c(I2C_SDA, I2C_SCL );
const int addr8bit = 20 << 1; // 7bit I2C address is 20; 8bit I2C address is 40 (decimal).


volatile uint32_t timeHasPassed = 0;
extern float Pose_error_integral;

I2C_HandleTypeDef *i2c_handler;
TIM_HandleTypeDef *timer_handler;

void initRobot(I2C_HandleTypeDef *_i2c_handler, TIM_HandleTypeDef *_timer_handler, Robot_Platform_Typedef *_platform)
{
	i2c_handler = _i2c_handler;
	timer_handler = _timer_handler;
	_platform->last_left_count = 0;
	_platform->last_right_count = 0;
	_platform->left_angular_speed = 0;
	_platform->right_angular_speed = 0;
	_platform->delta_x = 0;
	_platform->delta_y = 0;
	_platform->delta_phi = 0;
	_platform->x = 0;
	_platform->y = 0;
	_platform->phi = 0;
	_platform->T = 0;
	_platform->radius_mm = 100.0f;
}


int i2cWrite(uint8_t address, uint8_t buffer[], uint8_t size)
{

	  HAL_StatusTypeDef returnValue = HAL_I2C_Master_Transmit(i2c_handler, (uint16_t)address, (uint8_t*)buffer, size, TIMEOUTVALUE);

	  if(returnValue != HAL_OK)
	  {
		  return -1;
	  }
	  timeHasPassed = 0;
	  HAL_TIM_Base_Start_IT(timer_handler); //no channel necessary
	  while(timeHasPassed == 0);

	  return 0;
}

int i2cReceive(uint8_t address, uint8_t buffer[], uint8_t size)
{

	  HAL_StatusTypeDef returnValue = HAL_I2C_Master_Receive(i2c_handler, (uint16_t)address, (uint8_t *)buffer, size, TIMEOUTVALUE);

	  if(returnValue != HAL_OK)
	  {
		  return -1;
	  }

	  return 0;
}
void setSpeeds(int16_t leftSpeed, int16_t rightSpeed)
{

	/*if( leftSpeed > 0 && rightSpeed > 0)
		if(leftSpeed > rightSpeed)
		{
			if(leftSpeed > MAX_PWM_LIMIT)
			{
				float ratio = leftSpeed/rightSpeed;
				leftSpeed = MAX_PWM_LIMIT;
				rightSpeed = ration*MAX_PWM_LIMIT;
			}
		}
		else if(rightSpeed > leftSpeed)
		{
			if(rightSpeed > MAX_PWM_LIMIT)
			{
				float ratio = rightSpeed/leftSpeed;
				rightSpeed = MAX_PWM_LIMIT;
				leftSpeed = ration*MAX_PWM_LIMIT;
			}
		}*/

    uint8_t buffer[5];

    buffer[0] = 0xA1;
    memcpy(&buffer[1], &leftSpeed, sizeof(leftSpeed));
    memcpy(&buffer[3], &rightSpeed, sizeof(rightSpeed));

    i2cWrite(addr8bit, buffer, 5); // 5 bytes

}

void setLeftSpeed(int16_t speed)
{
    uint8_t buffer[3];

    buffer[0] = 0xA2;
    memcpy(&buffer[1], &speed, sizeof(speed));

    i2cWrite(addr8bit, buffer, 3); // 3 bytes
}

void setRightSpeed(int16_t speed)
{
    uint8_t buffer[3];

    buffer[0] = 0xA3;
    memcpy(&buffer[1], &speed, sizeof(speed));

    i2cWrite(addr8bit, buffer, 3); // 3 bytes
}

void getCounts(int16_t *countsLeft, int16_t *countsRight)
{

    uint8_t write_buffer[2];
    uint8_t read_buffer[4];

    write_buffer[0] = 0xA0;
    i2cWrite(addr8bit, write_buffer, 1);
    i2cReceive( addr8bit, read_buffer, 4);

    int16_t high = (int16_t)(read_buffer[0]<<8);
    int16_t low = (int16_t)(read_buffer[1]);
    *countsLeft = (int16_t)(high|low);
    high = (int16_t)(read_buffer[2]<<8);
    low = (int16_t)(read_buffer[3]);
    *countsRight = (int16_t)(high|low);

}

void getCountsAndReset(int16_t *countsLeft, int16_t *countsRight)
{

    uint8_t write_buffer[2];
    uint8_t read_buffer[4];

    write_buffer[0] = 0xA4;
    i2cWrite(addr8bit, write_buffer, 1);
    i2cReceive( addr8bit, read_buffer, 4);

    int16_t high = (int16_t)(read_buffer[0]<<8);
    int16_t low = (int16_t)(read_buffer[1]);
    *countsLeft = (int16_t)(high|low);
    high = (int16_t)(read_buffer[2]<<8);
    low = (int16_t)(read_buffer[3]);
    *countsRight = (int16_t)(high|low);

}

//dt in s
float calculateSpeed_cm_s(int16_t counts, float dt)
{
	float traveled = WHEEL_PERIMETER_MM*(counts)/ENCODER_PULSES_360;

	float speed_mm_s = traveled/dt;

	float speed_cm_s = speed_mm_s/10.0;

	return speed_cm_s;


}

float calculateWheelAngularSpeed_rad_s(int16_t counts, float dt)
{
	return ((2.0*PI * (float)counts)/(float)ENCODER_PULSES_360)/dt;

}

int calculateWheelRadtoPWM(float rad_s, float max_rad_s)
{
	return 300*(rad_s/max_rad_s);
}


float we_g, wd_g, we_g2, wd_g2;
void updateMotorSpeed_rad_s(float we, float wd)
{

	// Limit by max speed
	if(we > Platform.maxLimit_we || we < -Platform.maxLimit_we)
	{
		float ratio = wd / we;
		we = we > 0.0f ? Platform.maxLimit_we : -Platform.maxLimit_we;
		wd = ratio * we;
	}
	// Limit by min speed
	if(we < Platform.min_we && we > -Platform.min_we && we != 0.0f)
	{
		float ratio = wd / we;
		we = we > 0.0f ? Platform.min_we : -Platform.min_we;
		wd = ratio * we;
	}

	// Limit by max speed
	if(wd > Platform.maxLimit_wd || wd < -Platform.maxLimit_wd)
	{
		float ratio = we / wd;
		wd = wd > 0.0f ? Platform.maxLimit_wd : -Platform.maxLimit_wd;
		we = ratio * wd;
	}
	// Limit by min speed
	if(wd < Platform.min_wd && wd > -Platform.min_wd && wd != 0.0f)
	{
		float ratio = we / wd;
		wd = wd > 0.0f ? Platform.min_wd : -Platform.min_wd;
		we = ratio * wd;
	}

	we_g = we;
	wd_g = wd;
	int speed_e = calculateWheelRadtoPWM(we>Platform.maxLimit_we?Platform.maxLimit_we:we, Platform.max_we);
	int speed_d = calculateWheelRadtoPWM(wd>Platform.maxLimit_wd?Platform.maxLimit_wd:wd, Platform.max_wd);
	/*if(speed_e >= 60 || speed_d >= 60)
	{
		while(1);
	}*/
	setSpeeds(speed_e, speed_d);
	return;
}

void setMaxLimitAngularSpeeds_rad_s(float we, float wd)
{
	Platform.maxLimit_we = we;
	Platform.maxLimit_wd = wd;
}
void setMaxAngularSpeeds_rad_s(float we, float wd)
{
	Platform.max_we = we;
	Platform.max_wd = wd;
}

void setMinAngularSpeeds_rad_s(float we, float wd)
{
	Platform.min_we = we;
	Platform.min_wd = wd;
}

float calculateDistance(float x_initial, float y_initial, float x_final, float y_final)
{
	float v1[] = {x_initial,y_initial};
	float v2[] = {x_final, y_final};
	float result[] = {0,0};
 	arm_sub_f32(v2,v1, result, 2);

 	float sqrtResult = 0;
 	//arm_abs_f32(result, &absResult, 2);
 	arm_sqrt_f32( powf(result[0],2) + powf(result[1],2), &sqrtResult);

 	return sqrtResult;

}

float calculatePoseDiference(float phi_desired)
{
	float temp = phi_desired - Platform.phi;

	temp = atan2f(arm_sin_f32(temp),arm_cos_f32 (temp));

	return temp;

}


void odometry_noT(void)
{
	getCountsAndReset(&Platform.last_left_count, &Platform.last_right_count);


	//Platform.left_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_left_count, 1.0f);
	//Platform.right_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_right_count, 1.0f);
	float delta_theta_e = ((2.0f*PI * (float)Platform.last_left_count)/(float)ENCODER_PULSES_360);
	float delta_theta_d = ((2.0f*PI * (float)Platform.last_right_count)/(float)ENCODER_PULSES_360);

	Platform.delta_s = (delta_theta_d+delta_theta_e)/2.0f;

	Platform.delta_phi = ANGULAR_SPEED_TO_PHI*(delta_theta_d - delta_theta_e);
	Platform.phi += (Platform.delta_phi);
	Platform.phi = atan2f(arm_sin_f32(Platform.phi),arm_cos_f32 (Platform.phi));

	//Platform.delta_x = ((WHEEL_RADIUS_MM * arm_cos_f32(Platform.phi)) / 2) * (Platform.left_angular_speed + Platform.right_angular_speed);
	//Platform.delta_y = ((WHEEL_RADIUS_MM * arm_sin_f32(Platform.phi)) / 2) * (Platform.left_angular_speed + Platform.right_angular_speed);

	float w_t_2 = Platform.delta_phi/2.0f;
	float tempx, tempy;

	if(w_t_2 != 0.0f)
	{
		tempx = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				(arm_sin_f32(w_t_2)/w_t_2) * arm_cos_f32(Platform.phi+w_t_2);
		tempy = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				(arm_sin_f32(w_t_2)/w_t_2) * arm_sin_f32(Platform.phi+w_t_2);
	}
	else
	{
		tempx = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				arm_cos_f32(Platform.phi+w_t_2);
		tempy = (WHEEL_RADIUS_MM/2.0f *(delta_theta_e + delta_theta_d)) *
				arm_sin_f32(Platform.phi+w_t_2);
	}

	Platform.x += (tempx);
	Platform.y += (tempy);
}

void odometry(void)
{

	getCountsAndReset(&Platform.last_left_count, &Platform.last_right_count);


	Platform.left_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_left_count, Platform.T);
	Platform.right_angular_speed = calculateWheelAngularSpeed_rad_s(Platform.last_right_count, Platform.T);

	Platform.delta_phi = ANGULAR_SPEED_TO_PHI*(Platform.right_angular_speed - Platform.left_angular_speed);
	Platform.phi += (Platform.delta_phi*Platform.T);
	/*while(Platform.phi > PI){
		Platform.phi = Platform.phi - 2*PI;
	}
	while(Platform.phi < -PI)
	{
		Platform.phi = Platform.phi + 2*PI;
	}*/
	Platform.phi = atan2f(arm_sin_f32(Platform.phi),arm_cos_f32 (Platform.phi));

	Platform.delta_x = ((WHEEL_RADIUS_MM * cos(Platform.phi)) / 2) * (Platform.left_angular_speed + Platform.right_angular_speed);
	Platform.delta_y = ((WHEEL_RADIUS_MM * sin(Platform.phi)) / 2) * (Platform.left_angular_speed + Platform.right_angular_speed);
	Platform.x += (Platform.delta_x*Platform.T);
	Platform.y += (Platform.delta_y*Platform.T);


}

void goTo_XY(float x, float y)
{

	// Controller Gains
//	float kv = 0.5;
//	float ks = 2;
	float kv = Platform.gains.noPhi_linear_gain;
	float ks = Platform.gains.noPhi_angular_gain;

	float distance = calculateDistance(Platform.x, Platform.y, x, y);

	if(distance > 5)
	{
		float y_error = y-Platform.y;
		float x_error = x-Platform.x;

		//e)
		float phi_desired = atan2f(y_error, x_error);

		//c)
		float sqrtResult = 0;
		arm_sqrt_f32( powf(x_error,2) + powf(y_error,2), &sqrtResult);

		//d)
		float v = kv * sqrtResult;
		float w = ks * calculatePoseDiference(phi_desired);

		//f)
		float we = (v - (WHEELS_L_MM/2)*w)/WHEEL_RADIUS_MM;
		float wd = (v + (WHEELS_L_MM/2 )*w)/WHEEL_RADIUS_MM;


		updateMotorSpeed_rad_s(we,wd);
	}
	else
	{
		updateMotorSpeed_rad_s(0,0);
	}


}

void followLine(float line_a, float line_b, float line_c, float speed)
{
	// Controller Gains

	//float kd = 0.01;
	//float kh = 2;
	float kd = Platform.gains.line_distance_gain;
	float kh = Platform.gains.line_orientation_gain;

	float v1[] = {line_a,line_b,line_c};
	float v2[] = {Platform.x, Platform.y, 1};

	float resultDot = 0;
	arm_dot_prod_f32(v1,v2,3, &resultDot);
	float sqrtResult = 0;
	arm_sqrt_f32( powf(line_a,2) + powf(line_b,2), &sqrtResult);
	float d = resultDot / sqrtResult;


	//2)
	float phi_desired = atan2f(-line_a,line_b);

	//3)
	float w = -kd * d + kh * calculatePoseDiference(phi_desired);

	//5)
	float we = (speed - (WHEELS_L_MM/2)*w)/WHEEL_RADIUS_MM;
	float wd = (speed + (WHEELS_L_MM/2)*w)/WHEEL_RADIUS_MM;

	updateMotorSpeed_rad_s(we,wd);
}

int signal = 1;
void goTo_XYPhi(float x, float y, float phi, int firstRun)
{

	/*
	 *
	 */

   if(firstRun)
   {
	   //firstRun = 0;

		float dx, dy;


		dx = x-Platform.x;
		dy = y-Platform.y;
		float alpha = -Platform.phi + atan2f(dy,dx);
		alpha= atan2f(arm_sin_f32(alpha),arm_cos_f32 (alpha));
		if(alpha > PI/2 || alpha < -PI/2)
		{
			signal = -1;
		}
		else
		{
			signal = 1;
		}
   }

	float distance = calculateDistance(Platform.x, Platform.y, x, y);
	float absValue = fabsf(Platform.phi-phi);
	if(distance > 5.0f || absValue > PI/10.0f)
	{
		// Controller Gains
//		float kp = 0.5;
//		float ka = 4.0;
//		float kb = -1.5;
		float kp = Platform.gains.phi_linear_gain;
		float ka = Platform.gains.phi_alpha_gain;
		float kb = Platform.gains.phi_beta_gain;

		float dx, dy;


		dx = x-Platform.x;
		dy = y-Platform.y;



		//calc de (rho, alpha,beta)
		float sqrtResult = 0;
		arm_sqrt_f32( powf(dx,2) + powf(dy,2), &sqrtResult);
		float rho = sqrtResult;

		float alpha = -Platform.phi + atan2f(dy,dx);
		if(signal == -1)
		{
			alpha = PI + alpha;
			//alpha= atan2f(arm_sin_f32(alpha),arm_cos_f32 (alpha));
		}
		alpha= atan2f(arm_sin_f32(alpha),arm_cos_f32 (alpha));


		float beta = -Platform.phi - alpha + phi;
		beta= atan2f(arm_sin_f32(beta),arm_cos_f32 (beta));

		//calc v, w da plataforma.
		float v = kp * rho * signal;
		float w = (ka * alpha + kb * beta);


		//calc we, wd
		float we = (v - (WHEELS_L_MM/2)*w)/WHEEL_RADIUS_MM;
		float wd = (v + (WHEELS_L_MM/2)*w)/WHEEL_RADIUS_MM;

		updateMotorSpeed_rad_s(we,wd);
	}
	else
	{
		updateMotorSpeed_rad_s(0,0);
	}
}

void goTo_XY_integral(float x, float y)
{
	// Controller Gains
	//	float kv = 5.0;
	//	float ki = 0.05;
	//	float ks = 20;
	float kv = Platform.gains.noPhi_linear_gain;
	float ki = Platform.gains.noPhi_linear_gain_integral;
	float ks = Platform.gains.noPhi_angular_gain;

	//float distance = calculateDistance(Platform.x, Platform.y, x, y);

	float y_error = y-Platform.y;
	float x_error = x-Platform.x;

	//e)
	float phi_desired = atan2f(y_error, x_error);

	//c)
	float sqrtResult = 0;
	arm_sqrt_f32( powf(x_error,2) + powf(y_error,2), &sqrtResult);
	Pose_error_integral += sqrtResult;
	if(Pose_error_integral > 4000)
	{
		Pose_error_integral = 4000;
	}
	else if(Pose_error_integral < -4000)
	{
		Pose_error_integral = -4000;
	}

	//d)
	float v = (kv * sqrtResult) + (ki * Pose_error_integral);
	float w = ks * calculatePoseDiference(phi_desired);

	//f)
	float we = (v - (WHEELS_L_MM/2)*w)/WHEEL_RADIUS_MM;
	float wd = (v + (WHEELS_L_MM/2 )*w)/WHEEL_RADIUS_MM;


	updateMotorSpeed_rad_s(we,wd);

}
//

