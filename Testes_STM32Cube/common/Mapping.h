/*
 * mapping.h
 *
 *  Created on: Apr 14, 2019
 *      Author: Luis Afonso
 */

#ifndef MAPPING_H_
#define MAPPING_H_

#include "stdint.h"
#include "stm32f4xx_hal.h"

#define cellSize_mm 20.0f //size of each cell in millimeters

typedef struct
{
	float x_robot;
	float y_robot;
	float phi_robot;
	float theta_lidar;
	float distance_mm_lidar;
	float quality_lidar;
}s_LidarMeasurement;

float l_2_prob(float value);
extern float inverseModel(int32_t mx, int32_t my, s_LidarMeasurement lidarMeasurement);
extern void updateMapGivenZ(s_LidarMeasurement _lidarMeasurement);
extern void UnloadMap(UART_HandleTypeDef *huart);
#endif /* MAPPING_H_ */
