/*
 * logging.h
 *
 *  Created on: Nov 1, 2018
 *      Author: Luis Afonso
 */

#ifndef LOGGING_H_
#define LOGGING_H_

#include "loggingTypes.h"
#include "stdint.h"
#include "stm32f4xx_hal.h"
#define SYNC1 0x01
#define SYNC2 0x20

typedef struct
{
	uint8_t sync1;
	uint8_t sync2;
	uint8_t type;
	uint8_t content[255];
	uint8_t checkSum;
}s_loggingMessageBuff;

typedef struct
{
	//s_loggingMessage message;
	uint8_t state;
}s_loggingHandler;







extern int32_t logging_parser(s_loggingHandler _loggingHandler, uint8_t _byte);
extern int32_t logging_sendMessage(UART_HandleTypeDef *_huart, logging_messageTypes _messageID, uint8_t _content[]);
extern int32_t loggin_parseMessage(uint8_t byte);
extern void logging_registerCallback(logging_callback callback, int32_t type);
extern s_logging_MotorsSpeedMessage logging_getMotorsSpeedMessage(float _motor1, float _motor2);
extern s_logging_GyroMessage logging_getGyroMessage(int32_t _X, int32_t _Y, int32_t _Z);
extern s_logging_AccMessage logging_getAccMessage(int32_t _X, int32_t _Y, int32_t _Z);
extern s_logging_OrientationMessage logging_geOrientationMessage(float _pitch);
extern s_logging_pythonMessage logging_gePythonMessage(float _pitch, float _motor1, float _motor2);
extern s_logging_poseMessage logging_getPoseMessage(float x, float y, float phi);
extern s_logging_RPLidarMessage logging_getRPLidarMessage(float distance, float angle, float quality,
															float x, float y, float phi);
extern s_logging_mapCellOccupancyMessage logging_getMapCellOccupancyMessage(uint8_t x, uint8_t y, float logsOccupancy);
extern s_logging_histogramMessage logging_getHistogramMessage(float histogram[32], float);
extern s_logging_EncoderMessage logging_getEncoderMessage(int16_t left_count, int16_t right_count);


#endif /* LOGGING_H_ */
