/*
 * VFF.c
 *
 * Note: this first converts odometry x,y position to grid 80x80 position and then uses it for
 * the methods. This loses precision?
 *
 *  Created on: Mar 29, 2019
 *      Author: Luis Afonso
 */




#include <VFF_VFH.h>
#include "stdint.h"
#include "math.h"
#include "Robot.h"
#include "mapping.h"
#include "loggingTypes.h"
#include "logging.h"
#include "arm_math.h"

const uint8_t gridSize = 80;			// Map size in cells
const float gridSize_mm = 4000;			// Map cell size
float Fcr = -10000; 						// constant repulsion force
float Fca = 10; 						// constant atraction force
//const float Platform.radius_mm = 100.0f;	// platform
uint8_t activeWindowSize = 10;
float C_occ = 0.6f;
extern float mapArray[80][80];

extern Robot_Platform_Typedef Platform;


//const float cellSize_mm = gridSize_mm/(float)gridSize;



gridPosition desiredTarget = {30,20};
vectorForce Fr, Fa, Ftotal;
gridPosition robotPosition_g;

/*
 * Function that executes VFF
 */
void doVFF()
{

	// get robot position in grid
	robotPosition_g = VFF_getRobotGridPosition(Platform.x, Platform.y);


	if(robotPosition_g.posX == desiredTarget.posX && robotPosition_g.posY == desiredTarget.posY)
	{
		updateMotorSpeed_rad_s(0,0);
	}
	else{

		//calculate forces and get VFF direction of movement
		Fr = VFF_calculateFr(robotPosition_g);
		Fa = VFF_calculateFa(robotPosition_g, desiredTarget);
		Ftotal.F_x = Fr.F_x+Fa.F_x;
		Ftotal.F_y = Fr.F_y+Fa.F_y;

		//next point
		float nextX = Platform.x + Ftotal.F_x;
		float nextY = Platform.y + Ftotal.F_y;

		//do control
		goTo_XY_integral(nextX, nextY);
	}
}

/*
 * Function that executes VFF+VFH
 */
void doVFF_VFH()
{

	// get robot position in grid
	robotPosition_g = VFF_getRobotGridPosition(Platform.x, Platform.y);





	//calculate forces and get VFF direction of movement
//	Fr = VFF_calculateFr(robotPosition_g);
	Fa = VFF_calculateFa(robotPosition_g, desiredTarget);
//	Ftotal.F_x = Fr.F_x+Fa.F_x;
//	Ftotal.F_y = Fr.F_y+Fa.F_y;
	Ftotal.F_x = Fa.F_x;
	Ftotal.F_y = Fa.F_y;
	//Get magnitude of previous vector. We simply needed some magnitude
	float magnitude = 100;//;sqrtf(powf(Ftotal.F_x,2)+powf(Ftotal.F_y,2));




	//get new direction of movement with VFH
	int error;
	float phi = VFH_getPhi(robotPosition_g, &error);
	if(error)
		magnitude = 0.00;

	//next point
	float nextX = Platform.x + magnitude*arm_cos_f32(phi);
	float nextY = Platform.y + magnitude*arm_sin_f32(phi);

	if(robotPosition_g.posX == desiredTarget.posX && robotPosition_g.posY == desiredTarget.posY)
	{
		updateMotorSpeed_rad_s(0,0);
	}
	else
	{
		//do control
		goTo_XY_integral(nextX, nextY);
	}


}

/*
 * VFF_getRobotGridPosition
 *
 * converts x, y parameters to gridPosition (from millimeters to grid index coordinates)
 *
 */
gridPosition VFF_getRobotGridPosition(float x, float y)
{
	gridPosition robotPosition;
	robotPosition.posX = gridSize*(x/gridSize_mm);
	robotPosition.posY = gridSize*(y/gridSize_mm);

	return robotPosition;
}

/*
 * VFF_calculateFr
 *
 * Calculates repulsive force due to obstacles
 *
 */
vectorForce VFF_calculateFr(gridPosition robotPosition)
{

	uint8_t xo = robotPosition.posX;
	uint8_t yo = robotPosition.posY;
	float xo_mm = (xo*50)+25;
	float yo_mm = (yo*50)+25;

	float Fr_x = 0;
	float Fr_y = 0;
	int i,j;
	for(i = -activeWindowSize; i <= activeWindowSize; i++)
	{
		for(j = -activeWindowSize; j <= activeWindowSize; j++)
		{
			uint8_t xt = xo+i;
			uint8_t yt = yo+j;
			//se estiver fora da �rea do mapa ou se estiver em cima do rob� (d==0);
			if(!(xt >= 0 && yt >= 0 && xt <= 80 && yt <= 80) || (xt==xo && yt==yo))
			{

			}
			else
			{
				float C = l_2_prob(mapArray[xt][yt]);
				/*if(C > C_occ)
					C = 1.0f;
				else C = 0.0f;*/
				//uint8_t C = mapArray[xt][yt];

				if(C == 0.0f)
				{

				}
				else{
					float xt_mm = (xt*50)+25;
					float yt_mm = (yt*50)+25;
					float d = sqrtf(powf((xt_mm - xo_mm),2) + powf((yt_mm - yo_mm),2)) - Platform.radius_mm;
					if (d > 0)
					{
						float temp1 = ((Fcr * (float)C)/powf(d,2));
						float tempX = (xt_mm - xo_mm)/d;
						float tempY = (yt_mm - yo_mm)/d;
						Fr_x += ((temp1*tempX));
						Fr_y += ((temp1*tempY));
					}
					else
					{
						//Fr_x = Fcr;
						//Fr_y = Fcr;
					}



				}


			}
		}

	}


	vectorForce Fr= {Fr_x, Fr_y};
	return Fr;

}

/*
 * VFF_calculateFa
 *
 * calculates atractive force of objective point
 *
 */
vectorForce VFF_calculateFa(gridPosition robotPosition, gridPosition targetPosition)
{
	uint8_t xo = robotPosition.posX;
	uint8_t yo = robotPosition.posY;
	float xo_mm = (xo*50)+25;
	float yo_mm = (yo*50)+25;

	uint8_t xt = targetPosition.posX;
	uint8_t yt = targetPosition.posY;
	float xt_mm = (xt*50)+25;
	float yt_mm = (yt*50)+25;

	float d = sqrtf(powf((xt_mm - xo_mm),2) + powf((yt_mm - yo_mm),2));
	float tempX;
	float tempY;
	if(d > 0)
	{
		tempX = (xt_mm - xo_mm)/d;
		tempY = (yt_mm - yo_mm)/d;
	}
	else{
		tempX = 0;
		tempY = 0;
	}


	vectorForce Fa;

	Fa.F_x = Fca * tempX;
	Fa.F_y = Fca * tempY;

	return Fa;

}

/* =============================================
 * VFH
 *
 =============================================*/

#define alpha 10
#define nsectores (360/alpha)
float hk[nsectores];
float hk2[nsectores];
float a = 62.5, b = 0.125;
int filterWidth = 1;

uint8_t goodSectors[nsectores];
uint16_t Smax = 6;
float threshold = 15;
float phi_g;
uint16_t valleyMinSize = 3;
typedef struct
{
	uint8_t valleySize;
	uint8_t valleyStart;
	uint8_t valleyEnd;
}s_valleys;



/*
 * Simply converts radians to degrees
 */
float rad2deg(float a)
{
	return (a*360.0f/(2.0f*M_PI));
}

/*
 * Creates hk and hk2 histogram.
 * hk is the unfilteres histogram.
 * hk2 is the filtered histogram.
 */
extern UART_HandleTypeDef *huart_interface;
void VFH_histogram(gridPosition robotPosition)
{
	uint8_t xo = robotPosition.posX;
	uint8_t yo = robotPosition.posY;
	float xo_mm = (xo*50)+25;
	float yo_mm = (yo*50)+25;
	int i,j;

	for(i = 0; i < nsectores; i++)
	{
		hk[i] = 0;
	}

	for(i = 0; i < alpha; i++)
	{
		hk[i] = 0;
	}
	for(i = -activeWindowSize; i <= activeWindowSize; i++)
	{
		for(j = -activeWindowSize; j <= activeWindowSize; j++)
		{
			uint8_t xt = xo+i;
			uint8_t yt = yo+j;
			//se estiver fora da �rea do mapa ou se estiver em cima do rob� (d==0);
			if(!(xt >= 0 && yt >= 0 && xt <= 80 && yt <= 80) || (xt==xo && yt==yo))
			{

			}
			else
			{
				float C = l_2_prob(mapArray[xt][yt]);
				/*if(C > C_occ)
					C = 1.0f;
				else C = 0.0f;*/
				//uint8_t C = mapArray[xt][yt];
				if(C == 0.0f)
				{

				}
				else{
					float xt_mm = (xt*50)+25;
					float yt_mm = (yt*50)+25;
					float d = sqrtf(powf((xt_mm - xo_mm),2) + powf((yt_mm - yo_mm),2)) - Platform.radius_mm;
					if (d > 0)
					{
						float beta = rad2deg( atan2f(yt_mm-yo_mm, xt_mm-xo_mm));
						if(beta < 0)
						{
							beta = 360.0f+beta;
						}
						int k = floorf(beta/(float)alpha);
						float m = powf(C,2)*(a-b*d);
						hk[k] += m;
					}

				}
			}
		}
	}


	int k;
	for(i = 0; i < nsectores; i++)
	{
	    float sum = 0;
	    for (k = 0; k < filterWidth; k++)
	    {
	    	int leftIndex = i-k;
	    	int rightIndex = i+k;

	    	if(leftIndex < 0)
	    	{
	    		leftIndex = nsectores+leftIndex;
	    	}
	    	if(rightIndex >= nsectores)
			{
	    		rightIndex = rightIndex-nsectores;
			}


	    	sum = sum + k*hk[leftIndex] + k*hk[rightIndex];
	    }

	    hk2[i] = ((filterWidth+1)*hk[i] + sum)/(2*(filterWidth+1)+1);
	}



	return;
}




/*
 * VFH_getPhi
 *
 * Calculates best direction to move considering desired direction and best valley.
 * Also considers if the valley is narrow or wide and minimum valey size.
 *
 */
float VFH_getPhi(gridPosition robotPosition, int* error)
{

	*error = 0;
	/*
	 * update histogram
	 */
	VFH_histogram(robotPosition);

	/*
	 * Check which sectors are good (bellow threshold)
	 */
	int i;
	for(i = 0; i < nsectores; i++)
	{
		if(hk2[i] < threshold)
		{
			goodSectors[i] = 1;
		}
		else
		{
			goodSectors[i] = 0;
		}
	}



	/*
	 * get desired direction sector
	 */
	float beta = rad2deg( atan2f(Ftotal.F_y, Ftotal.F_x));
	if(beta < 0)
	{
		beta = 360.0f+beta;
	}
	int k_desired = floorf(beta/(float)alpha);

	int leftGood = 0;
	int rightGood = 0;
	int indexLeft;
	int indexRight;
	s_valleys valley = {0,0,0};

	s_valleys valleyLeft = {0,0,0};
	s_valleys valleyRight = {0,0,0};

	uint8_t k_desired_valeyGood = 0;

	/*
	 * Choose best valley if desired direction is in a valley.
	 */
	if(goodSectors[k_desired])
	{
		int leftDone = 0;
		int rightDone = 0;
		valley.valleySize = 1;
		//just find this valley size
		for(i = 1; i < nsectores-1; i++)
		{
			indexLeft = k_desired+i;
			indexRight = k_desired-i;

			//Contains indexes in [0,nsectores-1]
			if(indexRight < 0)
			{
				indexRight = nsectores+indexRight;
			}
			if(indexLeft >= nsectores)
			{
				indexLeft = indexLeft-nsectores;
			}


			/*
			 * Check if left side sector is good
			 */
			if(goodSectors[indexLeft] == 0 && !leftDone)
			{
				indexLeft--;
				if(indexLeft < 0)
				{
					indexLeft = nsectores-1;
				}
				valley.valleyStart = indexLeft;
				leftDone = 1;
			}
			else if(goodSectors[indexLeft] && !leftDone)
			{
				valley.valleySize++;
			}

			/*
			 * Check if right side sector is good
			 */
			if(goodSectors[indexRight] == 0 && !rightDone)
			{
				indexRight++;
				if(indexRight >= nsectores)
				{
					indexRight = indexRight-nsectores;
				}
				valley.valleyEnd = indexRight;
				rightDone = 1;
				//break;
			}
			else if(goodSectors[indexRight] && !rightDone)
			{
				valley.valleySize++;
			}

			/*
			 * if we found a valey over Smax it doesn't matter to check anymore
			 */
			if(valley.valleySize > Smax)
			{
				if(!leftDone)
				{
					valley.valleyStart = indexLeft;
				}
				if(!rightDone)
				{
					valley.valleyEnd = indexRight;
				}
				break;
			}
		}
	}

	/*
	 * Check if valley size is >= minimum size. Only needed in case desired direction is in a valley
	 *  but it's < than minimum size.
	 */
	if(valley.valleySize >= valleyMinSize)
	{
		k_desired_valeyGood = 1;
	}

	/*
	 * Choose best valley if desired direction is NOT in a valley or is in a valley and it's too small.
	 */
	if(!k_desired_valeyGood){
		// find closest valley (left or right)
		for(i = 0; i < nsectores-1; i++)
		{
			indexLeft = k_desired+i;
			indexRight = k_desired-i;
			if(indexRight < 0)
			{
				indexRight = nsectores+indexRight;
			}
			if(indexLeft >= nsectores)
			{
				indexLeft = indexLeft-nsectores;
			}


			/*
			 * leftGood is incremented when there's a goodSector and set to 0 if not.
			 * When leftGood == minimum valley size this means the valley is on the left and has minimum size.
			 * This ensures that the valley found is at least that size.
			 */
			if(goodSectors[indexLeft])
			{
				if(leftGood == 0)
				{
					valleyLeft.valleyStart = indexLeft;
				}
				leftGood++;
				if(leftGood == valleyMinSize)
				{
					rightGood = 0;
					break;
				}
			}
			else{
				leftGood = 0;
			}

			/*
			 * rightGood is incremented when there's a goodSector and set to 0 if not.
			 * When rightGood == minimum valley size this means the valley is on the right and has minimum size.
			 * This ensures that the valley found is at least that size.
			 */
			if(goodSectors[indexRight])
			{
				if(rightGood == 0)
				{
					valleyRight.valleyStart = indexRight;
				}
				rightGood++;
				if(rightGood == valleyMinSize)
				{
					leftGood = 0;
					break;
				}
			}
			else{
				rightGood = 0;
			}
		}



		/*
		 * Find the remaining valley size. Just to check if it's < Smax - to check if it's narrow or not.
		 * Algorithm has diferent counting direction depending if the valley found is on the left or right.
		 */
		if(leftGood)
		{
			valley.valleyStart = valleyLeft.valleyStart;
			for(i = 0; i < nsectores-1; i++)
			{
				indexLeft = valley.valleyStart+i;
				if(indexLeft >= nsectores)
				{
					indexLeft = indexLeft-nsectores;
				}
				if(goodSectors[indexLeft] == 0)
				{
					indexLeft--;
					if(indexLeft < 0)
					{
						indexLeft = nsectores-1;
					}
					valley.valleyEnd = indexLeft;
					break;
				}
				else
				{
					valley.valleySize++;
				}

				//if we found a valey over Smax it doesn't matter to check anymore
				if(valley.valleySize > Smax)
				{
					valley.valleyEnd = indexLeft;
					break;
				}
			}
		}
		else if(rightGood)
		{
			valley.valleyStart = valleyRight.valleyStart;
			for(i = 0; i < nsectores-1; i++)
			{
				indexRight = valley.valleyStart-i;
				if(indexRight >= nsectores)
				{
					indexRight = indexRight-nsectores;
				}
				if(goodSectors[indexRight] == 0)
				{
					indexRight++;
					if(indexRight >= nsectores)
					{
						indexRight = indexRight-nsectores;
					}
					valley.valleyEnd = indexRight;
					break;
				}
				else
				{
					valley.valleySize++;
				}

				//if we found a valey over Smax it doesn't matter to check anymore
				if(valley.valleySize > Smax)
				{
					valley.valleyEnd = indexRight;
					break;
				}

			}
		}
		else
		{
			*error = 1;
			return 0.0f;
		}

	}


	/*
	 * Calculate phi depending if valley is narrow or wide.
	 * due to how the algorithm is made, the math needs to change signal depending on the side of the valley.
	 */
	float phi;
	if(valley.valleySize > Smax)
	{
		//
		if(leftGood)
		{
			phi = (valley.valleyStart + 0.5*Smax)*alpha;
		}
		else if(rightGood)
		{
			phi = (valley.valleyStart - 0.5*Smax)*alpha;
		}
		else{

			phi = (valley.valleyStart - 0.5*Smax)*alpha;
		}

	}
	else{

		int16_t end = valley.valleyEnd*alpha;
		/*if(end > 180)
		{
			end = end-360;
		}*/
		int16_t start = valley.valleyStart*alpha;
		/*if(start > 180)
		{
			start = start-360;
		}*/
		phi  = 0.5 * (start + end);
	}

	//Convert to radian and in [-pi,pi] domain.
	phi = M_PI*phi/180.0f;
	phi = atan2f(sinf(phi), cosf(phi));

	phi_g = phi;

	s_logging_histogramMessage message = logging_getHistogramMessage(hk2, phi);
	logging_sendMessage(huart_interface, logging_histogramMessage, (uint8_t *)&message);

	return phi;

}
