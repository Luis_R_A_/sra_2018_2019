/*
 * Robot_Parameters.h
 *
 *  Created on: Feb 21, 2019
 *      Author: Luis Afonso
 */

#ifndef ROBOT_PARAMETERS_H_
#define ROBOT_PARAMETERS_H_

//#define PI 3.14f
#define WHEEL_RADIUS_MM 35.0f
#define WHEEL_THICKNESS_MM 8.0f
#define ENCODER_PULSES_360 1440.0f
#define WHEELS_L_MM 149.0f //149mm is between the outer face of the wheels
#define ANGULAR_SPEED_TO_PHI (float)(WHEEL_RADIUS_MM/WHEELS_L_MM) // r/b
#define WHEEL_PERIMETER_MM (float)(2.0f*PI*WHEEL_RADIUS_MM)







#endif /* ROBOT_PARAMETERS_H_ */
