/*
 * rplidar_a1.h
 *
 *  Created on: Apr 5, 2019
 *      Author: Luis Afonso
 */

#ifndef COMMON_RPLIDAR_A1_H_
#define COMMON_RPLIDAR_A1_H_

#include "stdint.h"
#include "stdbool.h"
#include "rptypes.h"
#include "rplidar_cmd.h"

#include "stm32f4xx.h"

typedef struct RPLidarMeasurement
{
    float distance;
    float angle;
    uint8_t quality;
    bool  startBit;
}RPLidarMeasurement;




void RPLidar_begin(UART_HandleTypeDef *_uart_handler );
// ask the RPLIDAR for its health info
u_result getHealth(rplidar_response_device_health_t *healthinfo, uint32_t timeout);

// ask the RPLIDAR for its device info like the serial number
u_result getDeviceInfo(rplidar_response_device_info_t *info, uint32_t timeout);

// stop the measurement operation
u_result stop();

// start the measurement operation
u_result startScan(bool force, uint32_t timeout);

// wait for one sample point to arrive
u_result waitPoint(uint32_t timeout);
u_result processPoint();
int32_t getCurrentPoint(RPLidarMeasurement *_data);

u_result _sendCommand(uint8_t cmd, uint8_t * payload, size_t payloadsize);
u_result _waitResponseHeader(rplidar_ans_header_t * header, uint32_t timeout);

#endif /* COMMON_RPLIDAR_A1_H_ */
