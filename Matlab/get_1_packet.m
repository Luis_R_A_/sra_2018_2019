
function [command, size, values] = get_1_packet(s)

        sizes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,...
                0, 0, 0, 0, 0, 16];
                
                

        good = 0;
        state = 0;
        check_sum = 0;
        packet_size = 0;
        counter = 1;
        command = 0;
        key = get(gcf,'CurrentKey');
        while good == 0 && strcmp(key, 's') == 0
            key = get(gcf,'CurrentKey');

            if s.BytesAvailable ~= 0 
                read = fread(s,1);
                if read == 1 && state == 0
                    state = state + 1;
                    check_sum = check_sum_values(check_sum, read);
                    %check_sum = check_sum + number_of_ones(read);
                elseif state == 1
                    if read == 32
                        state = state + 1;
                        check_sum = check_sum_values(check_sum, read);
                        %check_sum = check_sum + number_of_ones(read);
                    else
                        state = 0;
                        check_sum = 0;
                    end
                elseif state == 2
                    command = read;
                    check_sum = check_sum_values(check_sum, read);
                    state = state + 1;
                elseif state == 3
                    size = sizes(command);
                    if counter < size
                        values(counter) = read;
                        check_sum = check_sum_values(check_sum, read);
                        counter = counter +1;
                    else
                        values(counter) = read;
                        check_sum = check_sum_values(check_sum, read);
                        state = state + 1;
                    end
                elseif state == 4
                        %check_sum
                        %read
                    if check_sum == read
                        state = state + 1;
                        good = 1;
                    else
                        state = 0;
                        check_sum = 0;
                    end

                end
            end
        end
end


function check_sum = check_sum_values(check_sum, values)

    check_sum = check_sum + values;
    
    %if(check_sum > 255)
    n = rem(check_sum, 256);
    check_sum = n;
    %end

end
