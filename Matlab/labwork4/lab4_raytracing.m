function [x_mj, y_mj] = lab4_raytracing(map, Measurement)
%returns occupied cell coordinates in mm

%map will have CellSize(mm), Data
%Z will have the measurement (distance, angle and quality) as well the
%robot world coordinates

% gotta use sensor world coordinates instead of robot word coordinates.

%convert distance to max that we want to consider
Measurement.Distance = 2000; %remember, in mm
debug = 0;
occupied_value = 0.7;


if (Measurement.Quality > 0)
    if debug == 1
        Robot_xGrid = floor(Measurement.Robot.X/map.CellSize) + 60;
        Robot_yGrid = floor(Measurement.Robot.Y/map.CellSize) + 60;
    else
        Robot_xGrid = floor(Measurement.Robot.X/map.CellSize);
        Robot_yGrid = floor(Measurement.Robot.Y/map.CellSize);
    end

    Point_xGrid = Robot_xGrid + round((cos(Measurement.Angle+Measurement.Robot.Phi)*Measurement.Distance)/map.CellSize);
    Point_yGrid = Robot_yGrid + round((sin(Measurement.Angle+Measurement.Robot.Phi)*Measurement.Distance)/map.CellSize);
    %line( [Robot_xGrid+0.5, Point_xGrid+0.5 ], [Robot_yGrid+0.5,Point_yGrid+0.5]);

    %plot(Point_xGrid-0.5,Point_yGrid-0.5,'g*');
    dX = Point_xGrid - Robot_xGrid;
    dY = Point_yGrid - Robot_yGrid;

    nextX = Robot_xGrid;
    nextY = Robot_yGrid;

    if dX == 0
        % along Y
        while nextY ~= Point_yGrid
            if dY > 0
                nextY = nextY + 1;
            else
                nextY = nextY - 1;
            end
            if (nextX >= 1) && (nextX <= map.Size) && (nextY >= 1) && (nextY <= map.Size)
                if( map.Data(nextX,nextY) > occupied_value )
                    x_mj = (nextX-1)*map.CellSize + map.CellSize/2;
                    y_mj = (nextY-1)*map.CellSize + map.CellSize/2;
                    return
                end
            else
                x_mj = 8000;
                y_mj = 8000;
                return
            end
        end
    elseif dY == 0
        % along X
        while nextX ~= Point_xGrid
            if dX > 0
                nextX = nextX + 1;
            else
                nextX = nextX - 1;
            end
            if (nextX >= 1) && (nextX <= map.Size) && (nextY >= 1) && (nextY <= map.Size)
                if( map.Data(nextX,nextY) > occupied_value )
                    x_mj = (nextX-1)*map.CellSize + map.CellSize/2;
                    y_mj = (nextY-1)*map.CellSize + map.CellSize/2;
                    return
                end
            else
                x_mj = 8000;
                y_mj = 8000;
                return
            end
        end
    else
        diff = dY/dX;

        if abs(diff) < 1
            if dX > 0
                X_Inc = 1;
                Y_Inc = diff;
            else
                X_Inc = -1;
                Y_Inc = -diff;
            end
        elseif dY > 0 
            X_Inc = 1/diff;
            Y_Inc = 1;
        else
            X_Inc = -1/diff;
            Y_Inc = -1;
        end

        x = round(nextX);
        y = round(nextY);
        while (x ~= Point_xGrid) || (y ~= Point_yGrid)

            nextX = nextX + X_Inc;
            nextY = nextY + Y_Inc;
            x = round(nextX);
            y = round(nextY);
            if (x >= 1) && (x <= map.Size) && (y >= 1) && (y <= map.Size)
                if( map.Data(x,y) > occupied_value )
                    x_mj = (x-1)*map.CellSize + map.CellSize/2;
                    y_mj = (y-1)*map.CellSize + map.CellSize/2;
                    return
                end
            else
                x_mj = 8000;
                y_mj = 8000;
                return
            end
        end

        clear X_Inc Y_Inc x y diff;
    end
    clear dX dY nextX nextY;
end

x_mj = 8000;
y_mj = 8000;
clear Robot_xGrid Robot_yGrid Point_xGrid Point_yGrid
