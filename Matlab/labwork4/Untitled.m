
syms x y theta dSr dSl b
dS = (dSr+dSl)/2;
dTheta = (dSr-dSl)/b;
Fx = x + dS*cos(theta+dTheta/2);
Fy = y + dS*sin(theta+dTheta/2);
Ftheta = theta + dTheta;

dFx_x = diff(Fx,x);
dFx_y = diff(Fx,y);
dFx_theta = diff(Fx,theta);

dFy_x = diff(Fy,x);
dFy_y = diff(Fy,y);
dFy_theta = diff(Fy,theta);

dFtheta_x = diff(Ftheta,x);
dFtheta_y = diff(Ftheta,y);
dFtheta_theta = diff(Ftheta,theta);


dFx_Sr = diff(Fx,dSr);
dFx_Sl = diff(Fx,dSl);
dFy_Sr = diff(Fy,dSr);
dFy_Sl = diff(Fy,dSl);
dFtheta_Sr = diff(Ftheta,dSr);
dFtheta_Sl = diff(Ftheta,dSl);
