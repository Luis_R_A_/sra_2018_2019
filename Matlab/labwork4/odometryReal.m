function Pose_real = odometryReal(dl,dr, Pose_Initial)

ENCODER_PULSES_360 = 1440;
WHEEL_RADIUS_MM = 35.0;
WHEELS_L_MM  = 149.0;
ANGULAR_SPEED_TO_PHI = (WHEEL_RADIUS_MM/WHEELS_L_MM);
    
Pose.x = Pose_Initial.x;
Pose.y = Pose_Initial.y;
Pose.phi = Pose_Initial.phi;

for i=1:size(dl,1)
    
    delta_theta_e = (2.0*pi * dl(i))/ENCODER_PULSES_360;
    delta_theta_d = (2.0*pi * dr(i))/ENCODER_PULSES_360;

    delta_phi = ANGULAR_SPEED_TO_PHI*(delta_theta_d - delta_theta_e);
    Pose.phi = Pose.phi+(delta_phi);
    Pose.phi = atan2(sin(Pose.phi),cos (Pose.phi));
    
    Pose_real(i).phi = Pose.phi;
    
	w_t_2 = delta_phi/2.0;

	if(w_t_2 ~= 0.0)
	
		tempx = (WHEEL_RADIUS_MM/2.0 *(delta_theta_e + delta_theta_d)) * ...
				(sin(w_t_2)/w_t_2) * cos(Pose.phi+w_t_2);
		tempy = (WHEEL_RADIUS_MM/2.0 *(delta_theta_e + delta_theta_d)) * ...
				(sin(w_t_2)/w_t_2) * sin(Pose.phi+w_t_2);
	
    else
		tempx = (WHEEL_RADIUS_MM/2.0 *(delta_theta_e + delta_theta_d)) * ...
				cos(Pose.phi+w_t_2);
		tempy = (WHEEL_RADIUS_MM/2.0 *(delta_theta_e + delta_theta_d)) * ...
				sin(Pose.phi+w_t_2);
    end

	Pose.x = Pose.x+(tempx);
	Pose.y = Pose.y+(tempy);
    
    Pose_real(i).x = Pose.x;
    Pose_real(i).y = Pose.y;
    
end
