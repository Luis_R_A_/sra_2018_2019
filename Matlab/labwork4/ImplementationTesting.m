clear all; clc; close all;

folder = 'Test015';
MapFileName  = sprintf('%s/mapCellOccupancyMessage001.csv',folder);
%encoderFileName  = sprintf('%s/EncoderMessage001.csv',folder);
poseFileName  = sprintf('%s/poseMessage001.csv',folder);

%% receber mapa conhecido do robo
M = csvread(MapFileName, 1,0);

T = M(:,1);
x_map = M(:,2)+1;
y_map = M(:,3)+1;
logsOccupancy = M(:,4);

%% receber encoders do rob�
%M = csvread(encoderFileName, 1,0);

%T_encoder = M(:,1);
%dl = M(:,2);
%dr = M(:,3);

%% receber pose do rob�
M = csvread(poseFileName, 1,0);

T_pose = M(:,1);
X = M(:,2);
Y = M(:,3);
Phi = M(:,4);

%% Gerar mapa
if size(T,1) ~= 6400
    disp("error size of map occupancy");
    %return
end

for i = 1:6400%size(T,1)
    p = 1 - 1/(1+exp(logsOccupancy(i)));
    probability_map(81-y_map(i), x_map(i)) = p; 
    mapStruct.Data(x_map(i), y_map(i)) = p;
    probability(x_map(i), y_map(i)) = p; %here is x,y correctly because it's not used with ROS occupancy function.
end
mapStruct.ocuppiedValue = 0.65;
mapStruct.CellSize = 20;
mapStruct.Size = 80;
map = robotics.OccupancyGrid(probability_map);

%% Gerar odometria
Pose_Initial.x = X(1);
Pose_Initial.y = Y(1);
Pose_Initial.phi = Phi(1);

%Pose_real = odometryReal(dl,dr, Pose_Initial);
goodOdometry = 1;
kalmanOdometry = 1;
for i = 1:size(T_pose,1)
    if(T_pose(i) == 0)
        x_vector(goodOdometry) = X(i);
        y_vector(goodOdometry) = Y(i);
        goodOdometry = goodOdometry + 1;
    else
        x_vector_kalman(kalmanOdometry) = X(i);
        y_vector_kalman(kalmanOdometry) = Y(i);
        kalmanOdometry = kalmanOdometry + 1;  
    end
end




%% Plot
% mapa
Hfig = figure; 
Hmap = show(map);
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
hold on
title(folder);

% Odometria
plot(x_vector/mapStruct.CellSize +0.5,y_vector/mapStruct.CellSize +0.5, 'g.');
title("Odometria real");

% Pose
plot(x_vector_kalman/mapStruct.CellSize +0.5,y_vector_kalman/mapStruct.CellSize +0.5, 'r.');