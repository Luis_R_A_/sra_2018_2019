%{



Modela��o do ruido deve estar toda bem. � ao quadrado para ser variancia
https://arxiv.org/ftp/arxiv/papers/1611/1611.07112.pdf

Os 2 jacobianos de F est�o bem.

kr e kl

Q - deve ser valores bem pequenos pois temos odometrias a cada 3ms

%}

clear all; clc; close all;

folder = 'Test009';
MapFileName  = sprintf('%s/mapCellOccupancyMessage001.csv',folder);
encoderFileName  = sprintf('%s/EncoderMessage001.csv',folder);
RPLidarFileName  = sprintf('%s/RPLidarMessage001.csv',folder);

Sensor.xrobot_offset = -33.6835;
Sensor.yrobot_offset = -6.8362;
%% calculate g jacobian
%{
syms xmj ymj x y theta 
xs = Sensor.xrobot_offset*cos(theta) - Sensor.yrobot_offset*sin(theta) + x;
ys = Sensor.xrobot_offset*sin(theta) + Sensor.yrobot_offset*cos(theta) + y;
g = sqrt( (xs - xmj)^2 + (ys - ymj)^2); 
dgx = simplify(diff(g,x), 'IgnoreAnalyticConstraints', true);
dgy = simplify(diff(g,y), 'IgnoreAnalyticConstraints', true);
dgtheta = simplify(diff(g,theta), 'IgnoreAnalyticConstraints', true);
%}
dgx_function = @(x,y,theta,xmj,ymj)(2*x - 2*xmj - (67367*cos(theta))/1000 + (34181*sin(theta))/2500)/(2*((x - xmj - (67367*cos(theta))/2000 + (34181*sin(theta))/5000)^2 + (ymj - y + (34181*cos(theta))/5000 + (67367*sin(theta))/2000)^2)^(1/2));
dgy_function = @(x,y,theta,xmj,ymj)-(2*ymj - 2*y + (34181*cos(theta))/2500 + (67367*sin(theta))/1000)/(2*((x - xmj - (67367*cos(theta))/2000 + (34181*sin(theta))/5000)^2 + (ymj - y + (34181*cos(theta))/5000 + (67367*sin(theta))/2000)^2)^(1/2));
dgtheta_function = @(x,y,theta,xmj,ymj)(2*((34181*cos(theta))/5000 + (67367*sin(theta))/2000)*(x - xmj - (67367*cos(theta))/2000 + (34181*sin(theta))/5000) + 2*((67367*cos(theta))/2000 - (34181*sin(theta))/5000)*(ymj - y + (34181*cos(theta))/5000 + (67367*sin(theta))/2000))/(2*((x - xmj - (67367*cos(theta))/2000 + (34181*sin(theta))/5000)^2 + (ymj - y + (34181*cos(theta))/5000 + (67367*sin(theta))/2000)^2)^(1/2));
 
%jacobian_gi_generic.dgx = dgx;
%jacobian_gi_generic.dgy = dgy;
%jacobian_gi_generic.dgtheta = dgtheta;

%% receber mapa conhecido do robo
M = csvread(MapFileName, 1,0);
start = 1
T = M(start:end,1);
x_map = M(start:end,2)+1; %M(start:start+6400-1,2)+1;
y_map = M(start:end,3)+1;
logsOccupancy = M(start:end,4);

if size(T,1) ~= 6400
    disp("error size of map occupancy");
    %return
end

for i = 1:6400%size(T,1)
    p = 1 - 1/(1+exp(logsOccupancy(i)));
    probability_map(81-y_map(i), x_map(i)) = p; 
    mapStruct.Data(x_map(i), y_map(i)) = p;
    probability(x_map(i), y_map(i)) = p; %here is x,y correctly because it's not used with ROS occupancy function.
end
mapStruct.ocuppiedValue = 0.65;
mapStruct.CellSize = 20;
mapStruct.Size = 80;
map = robotics.OccupancyGrid(probability_map);
Hfig = figure; 
Hmap = show(map);
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
hold on
title(folder);

%% Position estimation covariance
Xerror = 0.01;%0.01^2;
Yerror = 0.01;%0.01^2; 
PhiError = deg2rad(0.5)^2;%deg2rad(0.5)^2; %0.5 graus deve ser 8 ticks de encoder
Q = [Xerror, 0, 0; 0, Yerror, 0; 0, 0, PhiError];


%% receber encoders do rob�
filename = encoderFileName;
M = csvread(filename, 1,0);
T_pose = M(:,1);
dl = M(:,2);
dr = M(:,3);
Pose_Initial.x = 600;
Pose_Initial.y = 800;
Pose_Initial.phi = 0;
Pose_real = odometryReal(dl,dr, Pose_Initial);
Pose_now.x = Pose_Initial.x;
Pose_now.y = Pose_Initial.y;
Pose_now.phi = Pose_Initial.phi;
Pose_now_bad = Pose_now;
Pose_now_good = Pose_now;
hold on
for i = 1:size(Pose_real,2)
    x_vector(i) = Pose_real(i).x;
    y_vector(i) = Pose_real(i).y;
end
plot(x_vector/mapStruct.CellSize +0.5,y_vector/mapStruct.CellSize +0.5);
title("Odometria real");

lh_kalman_odometry = plot([0],[0], 'g.');
lh_odometry = plot([0],[0], 'r.');
lh_kalman_obs = plot([0],[0], 'black.');
lh_odometry_good = plot([0],[0], '.', 'Color', [0 1 1]);
lh_odometry_good_obs = plot([0],[0], 'black.');
M = csvread(RPLidarFileName, 1,0);
Measurements.numPoints = size(M,1);
Measurements.Time = M(:,1);
Measurements.Distance = M(:,2);
Measurements.Angle = atan2(sin(((M(:,3)))),cos(((M(:,3)))));
Measurements.Quality = M(:,4);
%Measurement.Robot.X = M(:,5);
%Measurement.Robot.Y = M(:,6);
%Measurement.Robot.Phi = M(:,7);

odometria = 1;
medidas = 1;
plot_counter = 1;
plot_counter_obs = 1;
estado = 0;
C = [1, 0, 0; 0, 1, 0; 0, 0, deg2rad(0.5)];

pause
gi_debug = [];
quality_i_debug = [];
vi_debug = [];
C_debug = []; counter_C_debug = 1;
si_debug = [];
%d = Measurement.Distance(1+measure_block_size*(k-1):1+measure_block_size*(k-1)+measure_block_size);
for k = 1:(size(T_pose,1) + Measurements.numPoints) %numero de vezes que parou
  

    if estado == 0
        estado = 1;
    elseif estado == 1 && odometria ~= size(T_pose,1)
        if(T_pose(odometria+1) - T_pose(odometria) > 100 && T_pose(odometria+1) - T_pose(odometria) < 600)
            odometria = odometria + 1;
            estado = 2;
    
            v_array = [];
            jacobian_gi_matrix = [];
            R_vector = [];
            valid_counter = 1;
            %jacobian_gi_position.dgx = subs(jacobian_gi_generic.dgx,[x,y,theta],[X.x,X.y,X.phi]);
            %jacobian_gi_position.dgy = subs(jacobian_gi_generic.dgy, [x,y,theta],[X.x,X.y,X.phi]);
            %jacobian_gi_position.dgtheta = subs(jacobian_gi_generic.dgtheta,[x,y,theta],[X.x,X.y,X.phi]);
            
        else
            odometria = odometria+1;
        end  
    elseif estado == 2 
        pause(0.01);
        go = 0;
        if  medidas == Measurements.numPoints
            go = 1;
        elseif(Measurements.Time(medidas+1) - Measurements.Time(medidas) > 25)
            go = 1;
        end
        if go == 1
            valid_counter
            medidas = medidas+1;
            estado = 3;
            
            if valid_counter >= 3
              
         
                

                R_matrix = diag(R_vector);


                %v_array; %feita no loop antes, todos os v v�lidos.
                %jacobian_gi_matrix; %%feita no loop antes, todos os jacobianos de v v�lidos
                S = jacobian_gi_matrix * C * transpose(jacobian_gi_matrix) + R_matrix;

                G = C * transpose(jacobian_gi_matrix) * inv(S);
                %X = X + G*v_array;
                temp = G*v_array;
                X.x = X.x + temp(1);
                X.y = X.y + temp(2);
                X.phi = X.phi + temp(3);

                Pose_now.x = X.x;
                Pose_now.y = X.y;
                Pose_now.phi = X.phi;
                % este � o C(K+1|K+1)
                C = C - G*S*transpose(G);
            end
            
            lh_kalman_obs.XData(plot_counter_obs) = X.x/mapStruct.CellSize + 0.5;
            lh_kalman_obs.YData(plot_counter_obs) = X.y/mapStruct.CellSize + 0.5;
            
            lh_odometry_good_obs.XData(plot_counter_obs) = Pose_now_good.x/mapStruct.CellSize + 0.5;
            lh_odometry_good_obs.YData(plot_counter_obs) = Pose_now_good.y/mapStruct.CellSize + 0.5;
            
            plot_counter_obs = plot_counter_obs +1;
        else
            medidas = medidas+1;
        end  
    end
    
    if estado == 3
        estado = 1;
    elseif estado == 1
        %pause(0.01);
        Pose_now = odometryBad(dl(odometria), dr(odometria), Pose_now);
        
        Pose_now_bad = odometryBad(dl(odometria), dr(odometria), Pose_now_bad);
        
        Pose_now_good = odometryGood(dl(odometria), dr(odometria), Pose_now_good);
        
        X.x = Pose_now.x;
        X.y = Pose_now.y;
        X.phi = Pose_now.phi;
        
        %TODO : dr e dl
        deltaS = Pose_now.deltaS;%(dr(odometria)+dl(odometria))/2;
        deltaTheta = Pose_now.deltaTheta;
        
        phi = X.phi;
        temp = phi+deltaTheta/2;
        b = Pose_now.b;
        jacobian_p_F = [1, 0, -deltaS*sin(temp); 0, 1, deltaS*cos(temp); 0, 0, 1];
        jacobian_u_F = [0.5*cos(temp)- ( (deltaS/(2*b)) * sin(temp)), 0.5*cos(temp) + ( (deltaS/(2*b))*sin(temp) );
                        0.5*sin(temp)+ ( (deltaS/(2*b)) * cos(temp)), 0.5*sin(temp) - ( (deltaS/(2*b))*cos(temp) );
                        1/b, -1/b];
        %TODO : definir kr, kl. O que � dr e dl.
        kr = 0.5;%0.2857;
        kl = 0.5;%0.14286;
        Cu = [ (kr*abs(Pose_now.deltaDR)), 0; 0, (kl*abs(Pose_now.deltaDL))];

        % este � o C(K+1|K)
        C = jacobian_p_F*C*transpose(jacobian_p_F) + jacobian_u_F*Cu*transpose(jacobian_u_F) + Q;
        C_debug(:,:,counter_C_debug) =  C;
        counter_C_debug = counter_C_debug + 1;
    elseif estado == 2

        
        Sensor.world_x = Sensor.xrobot_offset*cos(X.phi) - Sensor.yrobot_offset*sin(X.phi) + X.x;
        Sensor.world_y = Sensor.xrobot_offset*sin(X.phi) + Sensor.yrobot_offset*cos(X.phi) + X.y;
        %% receber observa��o do rob�
       
        oi = Measurements.Distance(medidas); 

        %% raytracing dado theta da observa��o. Ver primeira c�lula ocupada
        Measurement.Angle = Measurements.Angle(medidas);
        Measurement.Quality = Measurements.Quality(medidas); 
        Measurement.Robot.X = Sensor.world_x;
        Measurement.Robot.Y = Sensor.world_y;
        Measurement.Robot.Phi = X.phi;
        [x_mj, y_mj] = lab4_raytracing(mapStruct, Measurement);
        gi = sqrt( (Sensor.world_x - x_mj)^2 + (Sensor.world_y - y_mj)^2 );
        gi_debug = [gi_debug gi];
        oi_estimate = gi;
        %jacobian_gi;
        jacobian_gi.dgx = dgx_function(X.x,X.y,X.phi,x_mj,y_mj);%double(subs(jacobian_gi_position.dgx,[xmj,ymj],[x_mj, y_mj]));
        jacobian_gi.dgy = dgy_function(X.x,X.y,X.phi,x_mj,y_mj);%double(subs(jacobian_gi_position.dgy, [xmj,ymj],[x_mj, y_mj]));
        jacobian_gi.dgtheta = dgtheta_function(X.x,X.y,X.phi,x_mj,y_mj);%double(subs(jacobian_gi_position.dgtheta,[xmj,ymj],[x_mj, y_mj]));
        jacobian_g = [jacobian_gi.dgx, jacobian_gi.dgy, jacobian_gi.dgtheta];
        
        
        %% inova��o
        vi = oi - oi_estimate;
        vi_debug = [vi_debug, vi];
        %covariancia da inova��o
        distanceError = 0.01; %in percentage
   
        Ri = (oi*distanceError + mapStruct.CellSize/2 )^2;  %
   
        si = jacobian_g * C * transpose(jacobian_g) + Ri;

        quality_i = vi * inv(si) * transpose(vi);
        quality_i_debug = [quality_i_debug quality_i];
        % quality_i < e^2
        % colocar em array validas
        %e = 2;
        if quality_i < 3.48%e^2
            si_debug = [si_debug si];
            v_array = [v_array; vi];
            jacobian_gi_matrix = [jacobian_gi_matrix; jacobian_gi.dgx, jacobian_gi.dgy, jacobian_gi.dgtheta];
            R_vector = [R_vector, Ri];
            valid_counter = valid_counter + 1;
        end

    end

    lh_kalman_odometry.XData(plot_counter) = X.x/mapStruct.CellSize + 0.5;
    lh_kalman_odometry.YData(plot_counter) = X.y/mapStruct.CellSize + 0.5;
    lh_odometry.XData(plot_counter) = Pose_now_bad.x/mapStruct.CellSize + 0.5;
    lh_odometry.YData(plot_counter) = Pose_now_bad.y/mapStruct.CellSize + 0.5;
    
    lh_odometry_good.XData(plot_counter) = Pose_now_good.x/mapStruct.CellSize + 0.5;
    lh_odometry_good.YData(plot_counter) = Pose_now_good.y/mapStruct.CellSize + 0.5;
    
    
    plot_counter = plot_counter + 1;

end






return


Pose_now_bad.x-x_vector(end);
Pose_now_bad.y-y_vector(end);
bad = sqrt((Pose_now_bad.x-x_vector(end))^2 + (Pose_now_bad.y-y_vector(end))^2)

Pose_now.x-x_vector(end);
Pose_now.y-y_vector(end);
good = sqrt((Pose_now.x-x_vector(end))^2 + (Pose_now.y-y_vector(end))^2)

rad2deg(Pose_now_bad.phi - Pose_real(end).phi)
rad2deg(Pose_now.phi - Pose_real(end).phi)



