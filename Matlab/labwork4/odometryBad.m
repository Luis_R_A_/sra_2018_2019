function Pose_bad = odometryBad(dl,dr, Pose_Initial)



ENCODER_PULSES_360 = 1440;
WHEEL_RADIUS_MM_d = 36.0;
WHEEL_RADIUS_MM_e = 34.0;
WHEELS_L_MM  = 149.0;%%149.0;= (WHEEL_RADIUS_MM/WHEELS_L_MM);
    
%ANGULAR_SPEED_TO_PHI 
Pose_bad.x = Pose_Initial.x;
Pose_bad.y = Pose_Initial.y;
Pose_bad.phi = Pose_Initial.phi;


delta_theta_e = WHEEL_RADIUS_MM_e*(2.0*pi * dl)/ENCODER_PULSES_360;
delta_theta_d = WHEEL_RADIUS_MM_d*(2.0*pi * dr)/ENCODER_PULSES_360;
Pose_bad.deltaDL = delta_theta_e;
Pose_bad.deltaDR = delta_theta_d;


Pose_bad.deltaTheta = (delta_theta_d - delta_theta_e)/WHEELS_L_MM;

Pose_bad.deltaS = (delta_theta_d + delta_theta_e)/2;

Pose_bad.b = WHEELS_L_MM;

Pose_bad.x = Pose_bad.x +  Pose_bad.deltaS* cos(Pose_bad.phi + Pose_bad.deltaTheta/2);
Pose_bad.y = Pose_bad.y + Pose_bad.deltaS * sin(Pose_bad.phi + Pose_bad.deltaTheta/2);

Pose_bad.phi = Pose_bad.phi+Pose_bad.deltaTheta;





