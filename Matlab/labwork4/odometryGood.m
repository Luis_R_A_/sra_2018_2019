function Pose_good = odometryGood(dl,dr, Pose_Initial)



ENCODER_PULSES_360 = 1440;
WHEEL_RADIUS_MM_d = 35.0;
WHEEL_RADIUS_MM_e = 35.0;
WHEELS_L_MM  = 149.0;%%149.0;= (WHEEL_RADIUS_MM/WHEELS_L_MM);
    
%ANGULAR_SPEED_TO_PHI 
Pose_good.x = Pose_Initial.x;
Pose_good.y = Pose_Initial.y;
Pose_good.phi = Pose_Initial.phi;


delta_theta_e = WHEEL_RADIUS_MM_e*(2.0*pi * dl)/ENCODER_PULSES_360;
delta_theta_d = WHEEL_RADIUS_MM_d*(2.0*pi * dr)/ENCODER_PULSES_360;
Pose_good.deltaDL = delta_theta_e;
Pose_good.deltaDR = delta_theta_d;

Pose_good.deltaTheta = (delta_theta_d - delta_theta_e)/WHEELS_L_MM;

Pose_good.deltaS = (delta_theta_d + delta_theta_e)/2;

Pose_good.b = WHEELS_L_MM;

Pose_good.x = Pose_good.x +  Pose_good.deltaS* cos(Pose_good.phi + Pose_good.deltaTheta/2);
Pose_good.y = Pose_good.y + Pose_good.deltaS * sin(Pose_good.phi + Pose_good.deltaTheta/2);

Pose_good.phi = Pose_good.phi+Pose_good.deltaTheta;





