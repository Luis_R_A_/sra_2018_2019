

Sensor.xrobot_offset = -33.6835;
Sensor.yrobot_offset = -6.8362;

X.x = 600;
X.y = 600;
X.phi = deg2rad(90);

Sensor.world_x = Sensor.xrobot_offset*cos(X.phi) - Sensor.yrobot_offset*sin(X.phi) + X.x;
Sensor.world_y = Sensor.xrobot_offset*sin(X.phi) + Sensor.yrobot_offset*cos(X.phi) + X.y;


plot(Sensor.world_x, Sensor.world_y, 'r.');
axis([-1000 1000 -1000 1000]);
hold on
circle(X.x,X.y, 75)
u = cos(X.phi)* 100;
v = sin(X.phi)* 100;
quiver(X.x,X.y, u,v);

return


function circle(x,y,r)
    %x and y are the coordinates of the center of the circle
    %r is the radius of the circle
    %0.01 is the angle step, bigger values will draw the circle faster but
    %you might notice imperfections (not very smooth)
    ang=0:0.01:2*pi; 
    xp=r*cos(ang);
    yp=r*sin(ang);
    plot(x+xp,y+yp);
end