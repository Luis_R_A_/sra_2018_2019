function mouseDown(object, eventdata)
global enableMouse startPosition startPointer

enableMouse = 1;
startPosition = get (gca, 'CurrentPoint');
startPointer = get(0, 'PointerLocation');