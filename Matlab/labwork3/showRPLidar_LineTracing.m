%clear; close; clc;
clear; clc;

debug = 0;
%% File read
folder = 'RPLidar_Test11';
RPLidarFileName  = sprintf('%s/RPLidarMessage001.csv',folder);

M = csvread(RPLidarFileName, 1,0);
Measurement.numPoints = size(M,1);
Measurement.Time = M(:,1);
Measurement.Distance = M(:,2);
Measurement.Angle = atan2(sin(((M(:,3)))),cos(((M(:,3)))));
Measurement.Quality = M(:,4);
Measurement.Robot.X = M(:,5);
Measurement.Robot.Y = M(:,6);
Measurement.Robot.Phi = M(:,7);

clear M folder RPLidarFileName;

%% Map init
if debug == 1
    map.Size = 200;
else
    map.Size = 80;
end

map.ocuppiedValue = 0.65;
map.CellSize = 50;
map.Data = zeros(map.Size,map.Size);
for i = 1:map.Size
    map.Data(1,i) = map.ocuppiedValue;
    map.Data(i,1) = map.ocuppiedValue;
    map.Data(map.Size,i) = map.ocuppiedValue;
    map.Data(i,map.Size) = map.ocuppiedValue;
end
initial_mapData = map.Data;
clear i;

%% Fill map
lfree = -0.65;
locc = 0.65;
for i = 1:Measurement.numPoints
    if (Measurement.Quality(i) > 0)
        if debug == 1
            Robot_xGrid = floor(Measurement.Robot.X(i)/map.CellSize) + 60;
            Robot_yGrid = floor(Measurement.Robot.Y(i)/map.CellSize) + 60;
        else
            Robot_xGrid = floor(Measurement.Robot.X(i)/map.CellSize);
            Robot_yGrid = floor(Measurement.Robot.Y(i)/map.CellSize);
        end
        
        Point_xGrid = Robot_xGrid + round((cos(Measurement.Angle(i))*Measurement.Distance(i))/map.CellSize);
        Point_yGrid = Robot_yGrid + round((sin(Measurement.Angle(i))*Measurement.Distance(i))/map.CellSize);
        dX = Point_xGrid - Robot_xGrid;
        dY = Point_yGrid - Robot_yGrid;

        nextX = Robot_xGrid;
        nextY = Robot_yGrid;

        if dX == 0
            % along Y
            while nextY ~= Point_yGrid
                if (nextX >= 1) && (nextX <= map.Size) && (nextY >= 1) && (nextY <= map.Size)
                    map.Data(nextX,nextY) = map.Data(nextX,nextY) + lfree + initial_mapData(nextX,nextY);
                end
                if dY > 0
                    nextY = nextY + 1;
                else
                    nextY = nextY - 1;
                end
            end
            if (nextX >= 1) && (nextX <= map.Size) && (nextY >= 1) && (nextY <= map.Size)
                map.Data(nextX,nextY) = map.Data(nextX,nextY) + locc + initial_mapData(nextX,nextY);
            end
        elseif dY == 0
            % along X
            while nextX ~= Point_xGrid
                if (nextX >= 1) && (nextX <= map.Size) && (nextY >= 1) && (nextY <= map.Size)
                    map.Data(nextX,nextY) = map.Data(nextX,nextY) + lfree + initial_mapData(nextX,nextY);
                end
                if dX > 0
                    nextX = nextX + 1;
                else
                    nextX = nextX - 1;
                end
            end
            if (nextX >= 1) && (nextX <= map.Size) && (nextY >= 1) && (nextY <= map.Size)
                map.Data(nextX,nextY) = map.Data(nextX,nextY) + locc + initial_mapData(nextX,nextY);
            end
        else
            diff = dY/dX;

            if abs(diff) < 1
                if dX > 0
                    X_Inc = 1;
                    Y_Inc = diff;
                else
                    X_Inc = -1;
                    Y_Inc = -diff;
                end
            elseif dY > 0 
                X_Inc = 1/diff;
                Y_Inc = 1;
            else
                X_Inc = -1/diff;
                Y_Inc = -1;
            end

            x = round(nextX);
            y = round(nextY);
            while (x ~= Point_xGrid) || (y ~= Point_yGrid)
                if (x >= 1) && (x <= map.Size) && (y >= 1) && (y <= map.Size)
                    map.Data(x,y) = map.Data(x,y) + lfree + initial_mapData(x,y);
                end

                nextX = nextX + X_Inc;
                nextY = nextY + Y_Inc;
                x = round(nextX);
                y = round(nextY);
            end
            if (x >= 1) && (x <= map.Size) && (y >= 1) && (y <= map.Size)
                map.Data(x,y) = map.Data(x,y) + locc + initial_mapData(x,y);
            end

            clear X_Inc Y_Inc x y diff;
        end

        clear dX dY nextX nextY;
    end
    clear Robot_xGrid Robot_yGrid Point_xGrid Point_yGrid
end
clear i initial_mapData lfree locc;

%% Convert and show map
figure;
OccGrid = robotics.OccupancyGrid(map.Size,map.Size,1);
p = zeros(map.Size*map.Size,1);
counter = 1;
for x = 1:map.Size
    for y = 1:map.Size
        p(counter) = 1 - 1/(1+exp(map.Data(x,y)));
        array(counter,:) = [x,y];
        counter = counter+1;
        %setOccupancy(OccGrid,[x,y],p);      
    end
end
setOccupancy(OccGrid,array,p); 
clear x y;
show(OccGrid);
grid on
set(gca,'XTick',0:1:map.Size,'YTick',0:1:map.Size)
xlim([0 map.Size])
ylim([0 map.Size])

hold on
if debug == 1
    plot((Measurement.Robot.X/50)+60,(Measurement.Robot.Y/50)+60);
    plot((Measurement.Robot.X(1)/50)+60,(Measurement.Robot.Y(1)/50)+60,'bo');
else
    plot((Measurement.Robot.X/50),(Measurement.Robot.Y/50));
    plot((Measurement.Robot.X(1)/50),(Measurement.Robot.Y(1)/50),'bo');
end
