%clear all; close all; clc;
clear all; clc;

folder = 'RPLidar_Test10';
RPLidarFileName  = sprintf('%s/mapCellOccupancyMessage001.csv',folder);
poseFileName  = sprintf('%s/poseMessage001.csv',folder);
histogramFileName  = sprintf('%s/histogramMessage001.csv',folder);

start = 1;
M = csvread(RPLidarFileName, 1,0);
T = M(start:end,1);
x = M(start:end,2)+1; %M(start:start+6400-1,2)+1;
y = M(start:end,3)+1;
logsOccupancy = M(start:end,4);



nMapas = size(T,1)/6400;
for k = 1:nMapas
    temposMapa(k,1) = T((k-1)*6400 +1,1);
end



for i = 1:6400%size(T,1)
    p = 1 - 1/(1+exp(logsOccupancy(i)));
    %need to change orientation because OccupancyGrid is stupid
    probability(81-y(i),x(i)) = p;
end

map = robotics.OccupancyGrid(probability);

filename = poseFileName;
M = csvread(filename, 1,0);


T_pose = M(:,1);
X = M(:,2);
Y = M(:,3);
Phi = M(:,4);

Hfig = figure; 
Hmap = show(map);
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
hold on
lh = plot((X/50),(Y/50))
title(folder);


filename = histogramFileName;
M = csvread(filename, 1,0);
T = M(:,1);

for i = 1:36
 sectors{i} = num2cell( M(:,1+i));  
end
phi_line = rad2deg(M(:,end));
for i = 1:size(phi_line,1)
    if(phi_line(i) < 0.0)
        phi_line(i) = phi_line(i) + 360;
    end
end
%plot(X,Y);
lh = plot(0,0); % plot(X(1:i),Y(1:i))
hold on
qh = quiver(0,0,0,0,0,'linewidth',10, 'MaxHeadSize', 5);%quiver(p1(1),p1(2),dp(1),dp(2),0,'linewidth',10, 'MaxHeadSize', 5)

thetas = [];
for i = 0:35
    thetas = [thetas, i*deg2rad(10)+(0)];
end
threshold = 5;
for k = 1:36
    val = cell2mat(sectors{k}(1));
    [x, y] =  pol2cart(thetas(k), val);
    if(val < threshold)
        handle_lines(k) = line([X(1)/50, (X(1)+x)/50 ],[Y(1)/50, (Y(1)+y)/50], 'Color', 'green');
    else
        handle_lines(k) = line([X(1)/50, (X(1)+x)/50 ],[Y(1)/50, (Y(1)+y)/50], 'Color', 'red');
    end
end

 r = robotics.Rate(1/0.001);
 mapCounter = 2;
 pause
 for i=1:10:size(X,1)
     

     if T_pose(i) > temposMapa(mapCounter)
         
         for n = 1:80
             for m = 1:80
                 p = 1 - 1/(1+exp(logsOccupancy((n-1)*80 + m + (mapCounter-1)*6400)));
                 x = n;
                 y = 81-m;
                 Hmap.CData((x-1)*80 + y) = p;
             end
         end
         
         mapCounter = mapCounter+1;
     end
     
     
     
	set(lh, 'YData',Y(1:i)/50);
    set(lh, 'XData',X(1:i)/50);
    
    
    
    p1 = [X(i) Y(i)];                         % First Point
    dx = cos(Phi(i))*5;
    dy = sin(Phi(i))*5;
    p2 = [X(i)+dx  Y(i)+dy];                         % Second Point
    dp = p2-p1;                         % Difference
    
    set(qh, 'XData', p1(1)/50);
    set(qh, 'YData', p2(2)/50);
    set(qh, 'UData', dp(1)/50);
    set(qh, 'VData', dp(2)/50);

   
    if(i <= size(sectors{k},1))
        for k = 1:36
            val = cell2mat(sectors{k}(i));
            [x, y] =  pol2cart(thetas(k), 250);
            handle_lines(k).XData = [X(i)/50, (X(i)+x)/50];
            handle_lines(k).YData = [Y(i)/50, (Y(i)+y)/50];
            selected = round((phi_line(i))/10)+1;
            if(k == selected)
                handle_lines(k).Marker = 'o';
                blue = 0.5;
            else
                handle_lines(k).Marker = 'none';
                blue = 0;
            end

            if(val < threshold)
                handle_lines(k).Color = [blue 0.5 blue];
            else
                handle_lines(k).Color = [0.5 0 blue];
            end
        end

    end
    %toc
    waitfor(r);

     %pause(0.02)
 end





 for n = 1:80
     for m = 1:80
         p = 1 - 1/(1+exp(logsOccupancy((n-1)*80 + m + (mapCounter-1)*6400)));
         x = n;
         y = 81-m;
         Hmap.CData((x-1)*80 + y) = p;
     end
 end
 
 
 

 
 
 
figure;

for i = 1:size(temposMapa)
    subplot(1,3,i)
     for n = 1:80
         for m = 1:80
             p = 1 - 1/(1+exp(logsOccupancy((n-1)*80 + m + (i-1)*6400)));
             x = n;
             y = 81-m;
             probability(y,x) = p;
         end
     end
     map = robotics.OccupancyGrid(probability);
     Hmap = show(map);
end





return
        for k = 1:36
            val = cell2mat(sectors{k}(end));
            [x, y] =  pol2cart(thetas(k), 250);
            handle_lines(k).XData = [X(end)/50, (X(end)+x)/50];
            handle_lines(k).YData = [Y(end)/50, (Y(end)+y)/50];
            if(val < threshold)

                handle_lines(k).Color = [0 1 0];
            else
                handle_lines(k).Color = [1 0 0];
            end
        end
figure;
x = zeros(1,36);
lh = plot([0:1:35], x) 
axis([0 36, 0 60]);
line([0, 36 ],[threshold, threshold], 'Color', 'red');
line([27, 27 ],[0, 200], 'Color', 'red');
pause;
for k = 1:size(sectors{1},1)    
    x = [];
    for i = 1:36
       x = [x, cell2mat(sectors{i}(k))];
    end
    lh.YData = x;
    drawnow
    pause(0.02)
end
%scatter(xGrid+0.5, yGrid+0.5)
