 function lreturn = inverseModel(mi, Robot_RPLidar)

    x = Robot_RPLidar.x;
    y = Robot_RPLidar.y;
    theta = Robot_RPLidar.theta;
    Z = Robot_RPLidar.distance;
    phi_robot = 0;
    
    l_0 = 0.0;
    locc = 0.65;
    lfree = -0.65;
    z_max = 2000; %in mm
    z_min = 150; %in mm
    alpha = 50; %in mm
    beta = deg2rad(1); %in degrees
    xi = mi(1);
    yi = mi(2);

    r = sqrt( (xi-x)^2 + (yi-y)^2);
    phi = atan2(yi-y, xi-x) - phi_robot; 

    temp = phi - theta;
    temp = atan2(sin(temp), cos(temp));
    %k = phi - theta; %why we need this? why ^k???
    if r >= min(z_max, Z + alpha/2) || norm(temp) > beta/2
        lreturn = l_0;
        return;
    end

    if Z < z_max && norm(r-Z) < alpha/2
        lreturn = locc;
        return;
    end

    if r <= Z
        lreturn = lfree;
        return
    end
    
    %lreturn = l_0;
    %return 

end