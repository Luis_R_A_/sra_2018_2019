%clear all; close all; clc;
clear all; clc;

folder = 'RPLidar_Test10';
RPLidarFileName  = sprintf('%s/mapCellOccupancyMessage001.csv',folder);

start = 1;
M = csvread(RPLidarFileName, 1,0);
T = M(start:end,1);
x = M(start:end,2)+1; %M(start:start+6400-1,2)+1;
y = M(start:end,3)+1;
logsOccupancy = M(start:end,4);



nMapas = size(T,1)/6400;
for k = 1:nMapas
    temposMapa(k,1) = T((k-1)*6400 +1,1);
end



for i = 1:6400%size(T,1)
    p = 1 - 1/(1+exp(logsOccupancy(i)));
    %need to change orientation because OccupancyGrid is stupid
    probability(81-y(i),x(i)) = p;
end

map = robotics.OccupancyGrid(probability);






Hfig = figure; 
Hmap = show(map);
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
hold on
title(folder);

 

 
 
 
figure;

for i = 1:size(temposMapa)
    subplot(1,2,i)
     for n = 1:80
         for m = 1:80
             p = 1 - 1/(1+exp(logsOccupancy((n-1)*80 + m + (i-1)*6400)));
             x = n;
             y = 81-m;
             probability(y,x) = p;
         end
     end
     map = robotics.OccupancyGrid(probability);
     Hmap = show(map);
end




 

%scatter(xGrid+0.5, yGrid+0.5)
