
clear all; clc;

folder = 'recordings';
FileName  = sprintf('%s/Recording14.mat',folder);

M = matfile(FileName);

cpu_freq = 180; %in mhz
summer = 0;
channelData = M.Channel1;
sampleTime = M.SamplePeriodInSeconds;
emptyCounter = 0;
for i = 1:size(M.Channel0,2)
    width = pulsewidth(double(channelData(:,i)))* sampleTime*10^6;
    summer = summer + width;
    if( isempty(width))
        widths(i) = 0;
        emptyCounter = emptyCounter+1;
    else
        
        widths(i) = width(1);
    end
end

 maximum = max(widths)
 minimum = min(widths)
 average = mean(widths)
 
 cycles_maximum = (maximum/(1/cpu_freq))
 cycles_minimum = (minimum/(1/cpu_freq))
 cycles_average = (average/(1/cpu_freq))
 
 plot(widths)
 legend('processing time in us')
return


