clear all; clc; close all;

folder = 'testHistogram';
histogramFileName  = sprintf('%s/histogramMessage001.csv',folder);
RPLidarFileName  = sprintf('%s/mapCellOccupancyMessage001.csv',folder);

filename = histogramFileName;
M = csvread(filename, 1,0);


T = M(:,1);
for i = 1:36
 sectors{i} = num2cell( M(:,1+i));  
end


alpha = 10;
angle_steps = 360/10;

edgesLimits = [];
for i = 0:35
    edgesLimits = [edgesLimits, i*(alpha)];
end
threshold = 50;
figure

for k = 1:50:size(M,1)
    histo = [];
    array2 = [];

        for i = 1:36
            angle = alpha * (i-1);
            %array = (ones(1,round(cell2mat(sectors{i}(k))))*angle)+0.1;
            array = round(cell2mat(sectors{i}(k)));
            histo = [histo, array];
            array2 = [array2 round(cell2mat(sectors{i}(k)))];
        end
    if(k == 1)

        %lh = polarhistogram('BinEdges',edgesLimits,'BinCounts',histo)
        lh = bar(edgesLimits,histo);
        hold on;
        line([0,360],[threshold,threshold], 'Color', 'red');
        axis([0, 360 0 500]);
        pause;
    else
        lh.YData = histo;
        drawnow
    end
    pause(0.03);
end




