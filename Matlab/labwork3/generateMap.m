clear all; close all; clc;
global enableMouse currentMouseLocation exitNow drawSize

%% Create map
exitNow = 0;
loadmap = 0;
map = 0;
Hfig = figure;
if loadmap == 1
   folder = 'testVFF1';
    matFileName = sprintf('%s/MapArray.mat',folder);
    poseFileName  = sprintf('%s/poseMessage001.csv',folder);

    example = matfile(matFileName);
    map = example.map; 
else
    map = robotics.BinaryOccupancyGrid(80,80,1);
end
for i = 1:80
     setOccupancy(map,[80, i],1);  
     setOccupancy(map,[i, 80],1);  
     setOccupancy(map,[0, i],1);  
     setOccupancy(map,[i, 0],1);  
end

Hmap = show(map);

hold on
%plot(xy(:,1),xy(:,2),'xr','MarkerSize', 0.1)
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
%}

drawSize = 0;
disp('created')
set (Hfig, 'WindowButtonDownFcn', @mouseDown);
set (Hfig, 'WindowButtonUpFcn', @mouseUp);
set(Hfig,'WindowButtonMotionFcn', @mouseMovement)
set(Hfig,'KeyPressFcn',@buttonPressed);



screenSize = get(0,'ScreenSize')
figureSize = getpixelposition(gca)
ratio = figureSize(3:4)./screenSize(3:4);
ratio2 = [80 80]./figureSize(3:4);




while(exitNow == 0)
    if enableMouse == 1
        
        C = get(0, 'PointerLocation');
        
        position = currentMouseLocation(1,1:2);%(startPosition(1,1:2) + ratio2.*(C-startPointer).*ratio);

        x = position(1);
        y = position(2);

        
        counter = 1;
        for k = x-drawSize:x+drawSize
            for n = y-drawSize:y+drawSize
                x2(counter,:) = k;
                y2(counter,:) = n;
                if(x2(counter) < 0)
                    x2(counter)  = 0;
                end
                if y2(counter) < 0
                    y2(counter) = 0;
                end
                if x2(counter)  > 80
                    x2(counter)  = 80;
                end
                if y2(counter) > 80
                    y2(counter) = 80;
                end
                counter = counter+1;
            end
        end
        setOccupancy(map,[x2, y2],1);
        temp = occupancyMatrix(map);
        Hmap.CData = temp;
        drawnow limitrate
    end
    pause(0.001)
end


mapData = occupancyMatrix(map);
mapDataCorrect = [];
for i = 1:80
    for j=1:80
        mapDataCorrect(i,j) = mapData(81-j, i);
    end
end
originalmapData  = mapData;
mapData = mapDataCorrect;


%% write file
ocuppiedValue = 9999999999;
save('MapArray.mat', 'map');
fid  = fopen('MapCArray.h', 'w');
fprintf(fid, 'float mapArray[80][80] = {\n');
for i = 1:size(mapData,1)
    for k = 1:size(mapData,1)
        if k == 1
            fwrite(fid, '{');
        end
        occ = (mapData(i,k));
        if occ > 0
            fwrite(fid, num2str(ocuppiedValue));
        else
            fwrite(fid, num2str(-ocuppiedValue));
        end
        if k == size(mapData,1)
            fwrite(fid, '}');
        end
        if i ~= size(mapData,1) || k ~= size(mapData,1)
            fwrite(fid, ',');
        end
    end
    fprintf(fid, '\n');
end
fwrite(fid, '};');

fclose(fid);
