%clear all; close all; clc;
clear; clc;

folder = 'RPLidar_Test7';
RPLidarFileName  = sprintf('%s/RPLidarMessage001.csv',folder);

M = csvread(RPLidarFileName, 1,0);
T = M(:,1);
distance = M(:,2);
angle = (360-M(:,3))-90;%270-M(:,3);
%angle = rad2deg(atan2(sin(deg2rad(angle)),cos(deg2rad(angle))));
quality = M(:,4);
X = M(:,5);
Y = M(:,6);
Phi = M(:,7);



for i = 2:size(T,1)
    t(i-1) = T(i)-T(i-1);
end
averageSampleTime_ms = mean(t)


sizeGrid = 80; %80x80
sizeCell = 50; %in mm

%matFileName = 'MapArray.mat';
%example = matfile(matFileName);
%map = example.map;
ocuppiedValue = 0.65;
mapData = zeros(80,80);
for i = 1:80
    mapData(1,i) = ocuppiedValue;
    mapData(i,1) = ocuppiedValue;
    mapData(80,i) = ocuppiedValue;
    mapData(i,80) = ocuppiedValue;
end
initial_mapData = mapData;


%profile on
timeSum = 0;
t2 = tic;
for k = 1:size(distance,1)
    if(quality(k) ~= 0)
        Robot_RPLidar.x = X(k);
        Robot_RPLidar.y = Y(k);
        temp =deg2rad(angle(k));
        Robot_RPLidar.theta = atan2(sin(temp),cos(temp));
        Robot_RPLidar.distance = distance(k);
  
        xGrid = floor(X(k)/sizeCell);
        yGrid = floor(Y(k)/sizeCell);
        gridRadius = ceil(distance(k)/sizeCell);
        t1 = tic;
        for i = -gridRadius:gridRadius%size(mapData)
            for j = -gridRadius:gridRadius%size(mapData)
                %if(i == 38 && j == 9 && Robot_RPLidar.distance > 1600)
                %    disp('stuff')
                %end
                 x = xGrid + i;
                 y = yGrid + j;
                 if x <= 80 && y <= 80 && x > 0 && y > 0
                     mi = [(x-1)*sizeCell+sizeCell/2, (y-1)*sizeCell+sizeCell/2];
                     l_inverse = inverseModel(mi, Robot_RPLidar);
                     mapData(x,y) = mapData(x,y) + l_inverse + initial_mapData(x,y);
                 end
                 %{
                 x = xGrid - i;
                 y = yGrid - j;
                 if x >= 1 && y >= 1
                     mi = [(x-1)*sizeCell+sizeCell/2, (y-1)*sizeCell+sizeCell/2];
                     l_inverse = inverseModel(mi, Robot_RPLidar);
                     mapData(x,y) = mapData(x,y) + l_inverse + initial_mapData(x,y);
                 end
                 %}
            end
        end
        timeSum = (timeSum+toc(t1));
       
    
    end
end
timeTotal = toc(t2);
timeAverage = timeSum /size(distance,1); 

%%
map = robotics.OccupancyGrid(80,80,1);
p = zeros(80*80,1);
counter = 1;
for x = 1:80
    for y = 1:80
        p(counter) = 1 - 1/(1+exp(mapData(x,y)));
        array(counter,:) = [x,y];
        counter = counter+1;     
    end
end
setOccupancy(map,array,p); 



Hfig = figure; 

subplot(1,2,1)
scan = lidarScan(distance/1000.0,deg2rad(angle-90));
plot(scan)
hold on


subplot(1,2,2)
Hmap = show(map);
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
hold on
scatter(xGrid+0.5, yGrid+0.5)




