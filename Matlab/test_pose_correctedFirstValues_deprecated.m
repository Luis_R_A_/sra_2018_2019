close all
clear all


%%
%Get readings
%rawAccGyro_gravityTest1 simply stand still
%filename = 'rawAccGyro_integrationTest1.csv';
filename = 'poseMessage001.csv';
M = csvread(filename, 1,0);

T = M(:,1);
X = M(:,2);
Y = M(:,3);
Phi = M(:,4);

r=35;
for i=1:size(X,1)-1
    vx(i) = X(i+1)-X(i);
    vy(i) = Y(i+1)-Y(i);
    w(i) = vx(i)/ (r * cos(Phi(i))/2);
end

Phi_correct = Phi*0.02;
for i=1:size(X,1)-1
    
    dt_x = (r * cos(Phi_correct(i))/2)*w(i);
    dt_y = (r * sin(Phi_correct(i))/2)*w(i);
    if i ~= 1
        x_correct(i,1) = x_correct(i-1,1) + dt_x*0.02;
        y_correct(i,1) = y_correct(i-1,1) + dt_y*0.02;
    else
        x_correct(i,1) = 0 + dt_x;
        y_correct(i,1) = 0 + dt_y;
    end
    
end

X = x_correct;
Y = y_correct;
Phi = Phi_correct(1:end-1);
T = T(1:end-1);


figure;
subplot(1,3,1);
plot(T,X);
subplot(1,3,2);
plot(T,Y);
subplot(1,3,3);
plot(T,Phi);

figure;
%plot(X,Y);
lh = plot(0,0); % plot(X(1:i),Y(1:i))
hold on
qh = quiver(0,0,0,0,0,'linewidth',10, 'MaxHeadSize', 5);%quiver(p1(1),p1(2),dp(1),dp(2),0,'linewidth',10, 'MaxHeadSize', 5)
 %axis([-20000, -500, -2000, 4000]);
 axis([-200, 500, -200, 400]);
 line([0,0],[-1000,1000], 'Color','g');
 line([-1000,1000],[0 0], 'Color','g');
 
 r = robotics.Rate(1/0.02);
 for i=1:size(X,1)
    tic
	set(lh, 'YData',Y(1:i));
    set(lh, 'XData',X(1:i));
    
    
    
    p1 = [X(i) Y(i)];                         % First Point
    dx = cos(Phi(i))*5;
    dy = sin(Phi(i))*5;
    p2 = [X(i)+dx  Y(i)+dy];                         % Second Point
    dp = p2-p1;                         % Difference
    
    set(qh, 'XData', p1(1));
    set(qh, 'YData', p2(2));
    set(qh, 'UData', dp(1));
    set(qh, 'VData', dp(2));
    %toc
    waitfor(r);
    toc
     %pause(0.02)
 end


%grid
%text(p1(1),p1(2), sprintf('(%.0f,%.0f)',p1))
%text(p2(1),p2(2), sprintf('(%.0f,%.0f)',p2))

return;


