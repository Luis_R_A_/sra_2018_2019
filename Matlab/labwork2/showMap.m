clear all; close all; clc;

folder = 'testVFH3';
matFileName = sprintf('%s/MapArray.mat',folder);
poseFileName  = sprintf('%s/poseMessage001.csv',folder);
poseFileName  = sprintf('%s/poseMessage001.csv',folder);

example = matfile(matFileName);
map = example.map;



Hfig = figure; 
Hmap = show(map);
hold on
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])


%% robot pose 
filename = poseFileName;
M = csvread(filename, 1,0);


T = M(:,1);
X = M(:,2);
Y = M(:,3);
Phi = M(:,4);
%figure
lh = plot((X/50),(Y/50))
title(folder);

return;
