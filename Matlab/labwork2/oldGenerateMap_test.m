clear all; close all; clc;
global enableMouse startPosition startPointer currentMouseLocation exitNow drawSize

%% Create map
exitNow = 0;
%{
map = robotics.BinaryOccupancyGrid(10,10,10);
x = [1.2; 2.3; 3.4; 4.5; 5.6];
y = [5.0; 4.0; 3.0; 2.0; 1.0];

setOccupancy(map, [x y], ones(5,1))
figure
show(map)
%}

Hfig = figure;
map = robotics.BinaryOccupancyGrid(80,80,1);
%{
xy = [50 50; 43 44; 56 53];
setOccupancy(map,xy,1);
%}
for i = 1:80
     setOccupancy(map,[80, i],1);  
     setOccupancy(map,[i, 80],1);  
     setOccupancy(map,[0, i],1);  
     setOccupancy(map,[i, 0],1);  
end

Hmap = show(map);

hold on
%plot(xy(:,1),xy(:,2),'xr','MarkerSize', 0.1)
grid on
set(gca,'XTick',0:1:80,'YTick',0:1:80)
xlim([0 80])
ylim([0 80])
%}

drawSize = 2;
disp('created')
set (Hfig, 'WindowButtonDownFcn', @mouseDown);
set (Hfig, 'WindowButtonUpFcn', @mouseUp);
set(Hfig,'WindowButtonMotionFcn', @mouseMovement)
set(Hfig,'KeyPressFcn',@buttonPressed);



screenSize = get(0,'ScreenSize')
figureSize = getpixelposition(gca)
ratio = figureSize(3:4)./screenSize(3:4);
ratio2 = [80 80]./figureSize(3:4);
%set(gcf, 'units','pixels','outerposition', screenSize);



while(exitNow == 0)
    if enableMouse == 1
        
        C = get(0, 'PointerLocation');
        
        position = currentMouseLocation(1,1:2);%(startPosition(1,1:2) + ratio2.*(C-startPointer).*ratio);
        %disp('-------------')
        %disp(startPosition(1,1:2))
        %disp(C)
        %disp(startPointer)
        %disp((C-startPointer).*ratio)
        %disp((C-startPointer).*ratio)
        %disp(position);

        x = position(1);
        y = position(2);

        
        counter = 1;
        for k = x-drawSize:x+drawSize
            for n = y-drawSize:y+drawSize
                x2(counter,:) = k;
                y2(counter,:) = n;
                if(x2(counter) < 0)
                    x2(counter)  = 0;
                end
                if y2(counter) < 0
                    y2(counter) = 0;
                end
                if x2(counter)  > 80
                    x2(counter)  = 80;
                end
                if y2(counter) > 80
                    y2(counter) = 80;
                end
                counter = counter+1;
            end
        end
        setOccupancy(map,[x2, y2],1);
        temp = occupancyMatrix(map);
        Hmap.CData = temp;
        drawnow limitrate
        %show(map);
        %setOccupancy(map,[x2, y2],1);
    end
    pause(0.001)
end


mapData = occupancyMatrix(map);
mapDataCorrect = [];
for i = 1:80
    for j=1:80
        mapDataCorrect(i,j) = mapData(81-j, i);
    end
end
originalmapData  = mapData;
mapData = mapDataCorrect;

%mapData =  transpose(mapData);

%% write file

save('MapArray.mat', 'map');
fid  = fopen('MapCArray.h', 'w');
fprintf(fid, 'uint8_t mapArray[80][80] = {\n');
for i = 1:size(mapData,1)
    for k = 1:size(mapData,1)
        if k == 1
            fwrite(fid, '{');
        end
        fwrite(fid, num2str(mapData(i,k)));
        if k == size(mapData,1)
            fwrite(fid, '}');
        end
        if i ~= size(mapData,1) || k ~= size(mapData,1)
            fwrite(fid, ',');
        end
    end
    %fwrite(fid, '\r\n ');
    fprintf(fid, '\n');
end
fwrite(fid, '};');
%fwrite(fid, mapData, 'uint8');
fclose(fid);
%hPoly = drawpolygon;
%Frame = getframe(Hfig)
%FrameData = Frame.cdata;
%figure;
%imshow(FrameData);


%% robot position and size
xo = 40;
yo = 40;
xo_cm = xo*5;
yo_cm = yo*5;
robotSize = 1;

%% VFF
activeWindowSize = 30;
C = mapData;
Fcr = 400;
counter = 1;


for i = -activeWindowSize:activeWindowSize
    for j = -activeWindowSize:activeWindowSize
        xt = (xo+i);
        yt = (yo+j);
        if ((norm(xt-xo) < robotSize &&   norm(yt-yo) < robotSize)) || ~(xt >= 1 && yt >= 1 && xt <= 80 && yt <= 80)
            F(counter,:) = [0,0];
            Fgraph(counter,:) = [F(counter,:), xt, yt];
        else

            xt_cm = xt*5;
            yt_cm = yt*5;
            d = sqrt((xt_cm - xo_cm)^2 + (yt_cm - yo_cm)^2);
            if(d == 0)
                disp("error")
            end
            cell =  C(xt,yt);
            %if xt == 0 || yt == 0 || xt == 80 || yt == 80
            %  cell = 1;
            %end
            temp1 = ((Fcr * cell)/d^2);
            tempX = (xt_cm - xo_cm)/d;
            tempY = (yt_cm - yo_cm)/d;
            temp2 = temp1 * [tempX tempY];
            F(counter,:) = -temp2;
            Fgraph(counter,:) = [F(counter,:), xt, yt];

        end
        counter = counter+1;
    end
end
Fr = sum(F)


figure;
quiver(xo, yo,Fr(1), Fr(2), 'black')
hold on
axis([-10,90,-10,90])
for i = 1:size(Fgraph,1)
    if(round(Fgraph(i,1),10) ~= 0 && round(Fgraph(i,2),10) ~= 0)
        quiver(Fgraph(i,3), Fgraph(i,4), Fgraph(i,1), Fgraph(i,2), 'r')
    end

end
Fca = 20;
xtarget = 60;
ytarget = 60;
xt_cm = xtarget*5;
yt_cm = ytarget*5;
d = sqrt((xt_cm - xo_cm)^2 + (yt_cm - yo_cm)^2);
tempX = (xt_cm-xo_cm)/d;
tempY = (yt_cm-yo_cm)/d;
Fa = Fca*[tempX tempY]
quiver(xo, yo,Fa(1), Fa(2), 'b')

Ftotal = Fa+Fr
quiver(xo, yo,Ftotal(1), Ftotal(2), 'g')

%% VFH


a= 30*5;
b = 1;
%565.6854
counter = 1;


%% get histogram
for i = -activeWindowSize:activeWindowSize
    for j = -activeWindowSize:activeWindowSize
        xt = (xo+i);
        yt = (yo+j);
        if ((norm(xt-xo) < robotSize &&   norm(yt-yo) < robotSize)) || ~(xt >= 1 && yt >= 1 && xt <= 80 && yt <= 80)
            
        else
            xt_cm = xt*5;
            yt_cm = yt*5;
            d =  sqrt((xt_cm - xo_cm)^2 + (yt_cm - yo_cm)^2);
            beta = atan2(yt_cm-yo_cm, xt_cm-xo_cm);
            m = C(xt,yt)^2 *(a-b*d);
            result(counter,:) = [beta, m, xt, yt];
            counter = counter+1;
        end
    end
end

figure;
subplot(2,2,1);
histogram(result(:,1))
subplot(2,2,2);
histogram(result(:,2))

alpha = 10;
nSectores = 360/alpha;

for i = 1:nSectores
    hk(i) = 0;
end


for i = 1:size(result,1)
    beta = result(i,1);
    if(beta < 0)
        beta = 2*pi + beta;
    end
    m = result(i,2);
    k = floor(rad2deg(beta) / alpha);
    if k >= nSectores
        k = 0;
    end
    hk(k+1) = hk(k+1) + m;
end

subplot(2,2,3)
bar(1:nSectores, hk);

%% filter histogram

for i = 1:nSectores
    hk2(i) = 0;
end

filterWidth = 2;
for i = 1:filterWidth
    hk2(i) = hk(i);
end
for i = (filterWidth+1):size(hk,2)-(filterWidth+1)
    sum = 0;
    for k = 1:filterWidth
        sum = sum + k*hk(i-k) + k*hk(i+k);
    end
    hk2(i) = ((filterWidth+1)*hk(i) + sum)/(2*(filterWidth+1)+1);
end
for i = 0:filterWidth-1
    hk2(end-i) = hk(end-i);
end
subplot(2,2,4)
bar(1:nSectores, hk2);

%% find all valleys

PODThreshold = 100;
valeyCounter = 0;
inValey  =0;
for i = 1:size(hk2,2)
    if(hk2(i) < PODThreshold)
        goodSectors(i) = 1;
        if inValey == 0
            newValey = 1;
        end
        inValey = 1;
    else
        if inValey
            valeyCenter(valeyCounter) = valeyStart(valeyCounter) + valeySize(valeyCounter)/2;
            valeyLast(valeyCounter) = i-1;
        end
        goodSectors(i) = 0;
        inValey = 0;
    end
    
    if inValey
        if newValey
            sectorCounter = 1;
            newValey = 0;
            valeyCounter = valeyCounter+1;
            valeySize(valeyCounter) = 1;
            valeyStart(valeyCounter) = i;
        else
           valeySize(valeyCounter) = valeySize(valeyCounter) + 1; 
        end

        
    end
    
end
if inValey
    valeyCenter(valeyCounter) = valeyStart(valeyCounter) + valeySize(valeyCounter)/2;
    valeyLast(valeyCounter) = i-1;
end


%% find the best valey 
% never selects the first valey even if it's the best.
%there's a bug here for the first valey.
Smax = 30;
ktarg = atan2(Ftotal(2),Ftotal(1));%atan2(xtarget-xo,ytarget-yo);
if(ktarg < 0)
    ktarg = 2*pi + ktarg;
end
sizeSectors = 2*pi/nSectores;

sectorNumber = floor(ktarg/sizeSectors);

bestValey = 0;
kn = 0;
bestSize = 0;
closest = 1000;
for i = 1:size(valeyStart,2)
    if valeyStart(i) < sectorNumber && valeyLast(i) > sectorNumber
        bestValey = i;
        bestSize = valeySize(i);
        kn = sectorNumber;
    else
        temp1 = norm(sectorNumber - valeyStart(i));
        temp2 = norm(sectorNumber - valeyLast(i));
        if temp1 < closest
            closest = temp1;
            bestValey = i;
            bestSize = valeySize(i);
            kn = (valeyStart(i));
        elseif temp2 < closest 
            closest = temp2;
            bestValey = i;
            bestSize = valeySize(i);
            kn = (valeyLast(i));
        end
    end
end

%% calculate phi
phi = 0;
% deve haver aqui um erro no alpha. acho que devia ser considerado para rad
% e n�o em degrees.
if bestSize > Smax
    phi = (kn + 0.5*Smax)*alpha;
else
    phi = 0.5 * (valeyStart(bestValey) + valeyLast(bestValey))*alpha;
end


%hk = some m(i,j) por sector

%h�k = ...