inicial: 100,100,0
final: 1750,2000

Dados:
- Tamanho do rob�: raio de 100mm
- Active window size = 5 // ou seja 10x10 squares
- ignoramos dados dentro do circulo do rob�.

Ganhos:
- Kp = 2.0
- Ki = 0.1
- Ka = 5.0

VFF:
- Fcr = -100
- Fca = 10


VFH:
- alpha = 10 //resolu��o em �
- nsectores = 36
- a = 500
- b = 1
- filterWidth = 4 // n� se amostras usadas em torno da amostra a filtrar
- threshold = 100
- Smax = 5 //50�
- Smin = 3 //30�
