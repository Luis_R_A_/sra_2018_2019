clear all;
close all;

% delete all serial ports from memory 
% important, if you the code is stopped without closing and deleting the
% used COM, you need to do this to open it again
delete(instrfindall);

% Init and open the serial port
%s = serial('COM26', 'baudrate', 115200);
s  = Bluetooth('HC_05_115200_LR',1);
fopen(s);

%{
command = 0; size = 0;
key = get(gcf,'CurrentKey'); %get the key currently pressed
while command ~= 16 && strcmp(key, 's') == 0
     key = get(gcf,'CurrentKey'); %get the key currently pressed
     [command, size, values] = get_1_packet(s);
end


fclose(s); %close serial port
delete(s); %remove serial port from memory

return
%}

%set the maximum for the values 
y_max = 4095;

points = 900; %number of points on the graph at all times
data_period = 0.01; %data period in milliseconds 
%x will be the time axis. The time between points is defined by
%"data_period"
x1 = linspace(-points*data_period,0, points);

%y will hold the distance, for now all values will be 0 and will have the
%size defined by "points"
y1 = linspace(0,0,points);
t1 = linspace(0,0,points);
phi = linspace(0,0,points);

% draw a area graph with parameter x and y. Save the return value so we can
% edit the graph later. You can use "plot" instead of "area", I just liked
% the aspect better.
lh1 = plot(x1,y1);
str = sprintf('0 to %d', y_max)
title(str);


p1 = [0 0];                         % First Point
dx = cos(0)*100;
dy = sin(0)*100;
p2 = [0+dx  0+dy];                         % Second Point
dp = p2-p1;                         % Difference
hold on
qh = quiver(p1(1),p1(2),dp(1),dp(2),0,'linewidth',10, 'MaxHeadSize', 5)

axis([-1000, 1000, -1000, 1000]);

counter = 1;
key = get(gcf,'CurrentKey'); %get the key currently pressed

updateCounter = 0;
while ( strcmp(key, 's') == 0 ) 
    key = get(gcf,'CurrentKey'); %get the key currently pressed
    command = 0; size = 0;

            
    [command, size, values] = get_1_packet(s);


    t1(counter) = bitsll(values(4), 24) + bitsll(values(3), 16) + bitsll(values(2), 8) +  values(1);
    xBytes = [values(5), values(6), values(7) values(8)];
    x1(counter) = typecast(uint8(xBytes), 'int32_t');
    yBytes = [values(9), values(10), values(11) values(12)];
	y1(counter) = typecast(uint8(yBytes), 'int32_t');
    phiBytes = [values(13), values(14), values(15) values(16)];
    phi(counter) = typecast(uint8(phiBytes), 'int32_t')/1000;
    
    
    
    updateCounter = updateCounter + 1;
    p1 = [x1(counter) y1(counter)];                         % First Point
    
    if updateCounter > 50

        updateCounter = 0;
        dx = cos(phi(counter))*5;
        dy = sin(phi(counter))*5;
        p2 = [x1(counter)+dx  y1(counter)+dy];                         % Second Point
        dp = p2-p1;                         % Difference

        set(qh, 'XData', p1(1));
        set(qh, 'YData', p2(2));
        set(qh, 'UData', dp(1));
        set(qh, 'VData', dp(2));


        set(lh1, 'YData',y1);
        set(lh1, 'XData',x1);
        %request the plot to be updated
        drawnow;

    end
    
 
    counter = counter+1;
end

fclose(s); %close serial port
flushinput(s);
delete(s); %remove serial port from memory