close all
clear all

%{
    - poseMessage001_part1_test001 - origin (0,0,0). To (500,500).
    (kv,ks)=(1.0, 3.0)
    - poseMessage001_part1_test002 - origin (0,0,0). To (500,500).
    (kv,ks)=(1.0, 10.0)
    - poseMessage001_part1_test003 - origin (0,0,0). To (500,500).
    (kv,ks)=(3.0, 3.0)
    - poseMessage001_part2_test001 - origin (0,0,0). (kd, kh) = (0.01,5.0).
    origin (0,0,0). first (a,b,c,v) = (0,1,-500,600), then (0,-1,500,600)
    - poseMessage001_part3_test001 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-2.5).
    desired (500,500,-90�).
    - poseMessage001_part3_test002 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-2.5).
    desired (-500,-500,-90�).
    - poseMessage001_part3_test003 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-2.5).
    desired (-500,750,180�).
    - poseMessage001_part3_test004 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-2.5).
    desired (500,-200,-90).
    - poseMessage001_part3_test005 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-2.5).
    desired (900,900,90).
    - poseMessage001_part3_test006 - origin (0,0,0). (kp, ka, kb) = (3.0,4.0,-2.5).
    desired (900,900,90).
    - poseMessage001_part3_test007 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-5.0).
    desired (900,900,90).
    - poseMessage001_part3_test008 - origin (0,0,0). (kp, ka, kb) = (3.0,8.0,-0.5).
    desired (900,900,90).
%}

%%
%Get readings
%rawAccGyro_gravityTest1 simply stand still
%filename = 'rawAccGyro_integrationTest1.csv';
filename = 'poseMessage001_part2_test001.csv';
M = csvread(filename, 1,0);

T = M(:,1);
X = M(:,2);
Y = M(:,3);
Phi = M(:,4);


dt = 20;
missed = 0;
for i = 1:size(T,1)-1
    testDt(i) = T(i+1) - T(i);
    if testDt(i) ~= dt
        missed = missed +1;
    end
end

missed

%{
figure;
subplot(1,3,1);
plot(T,X);
subplot(1,3,2);
plot(T,Y);
subplot(1,3,3);
plot(T,Phi);
%}

figure;
%plot(X,Y);
lh = plot(0,0); % plot(X(1:i),Y(1:i))
hold on
qh = quiver(0,0,0,0,0,'linewidth',10, 'MaxHeadSize', 5);%quiver(p1(1),p1(2),dp(1),dp(2),0,'linewidth',10, 'MaxHeadSize', 5)

xmax = 3000, xmin = -500, ymax = 1000, ymin=-500;
axis([xmin, xmax, ymin, ymax]);
line([0,0],[ymin,ymax], 'Color','g');
line([xmin,xmax],[0 0], 'Color','g');

%line([500,500],[ymin,ymax], 'Color','black');
line([xmin,xmax],[500 500], 'Color','black','LineStyle','--');
%text(500,500, sprintf('end: (%.0f,%.0f)',[500, 500]));
text(0,0, sprintf('start'));
pause;
 r = robotics.Rate(1/0.002);
 for i=1:size(X,1)
   % tic
	set(lh, 'YData',Y(1:i));
    set(lh, 'XData',X(1:i));
    
    
    
    p1 = [X(i) Y(i)];                         % First Point
    dx = cos(Phi(i))*5;
    dy = sin(Phi(i))*5;
    p2 = [X(i)+dx  Y(i)+dy];                         % Second Point
    dp = p2-p1;                         % Difference
    
    set(qh, 'XData', p1(1));
    set(qh, 'YData', p2(2));
    set(qh, 'UData', dp(1));
    set(qh, 'VData', dp(2));
    %toc
    waitfor(r);
     %pause(0.02)
 end


%grid
text(X(end),Y(end), sprintf('(%.0f,%.0f)',[X(end), Y(end)]));

Xdesired = 500;
Ydesired = 500;
distanceToPoint = norm([Xdesired, Ydesired] - [X(end), Y(end)]);
disp(sprintf('distance to point = %.0f mm',distanceToPoint));

%text(p2(1),p2(2), sprintf('(%.0f,%.0f)',p2))

return;


