clear; close; clc;
%%
MarkerDistanceMax = 200;
MarkerDistanceMin = 180;

%% File read
folder = 'labwork3/RPLidar_calib';
RPLidarFileName  = sprintf('%s/RPLidarMessage001.csv',folder);

M = csvread(RPLidarFileName, 1,0);
Measurement.numPoints = size(M,1);
Measurement.Time = M(:,1);
Measurement.Distance = M(:,2);
Measurement.Angle = atan2(sin(deg2rad((360-M(:,3)))),cos(deg2rad((360-M(:,3)))));
Measurement.Quality = M(:,4);
Measurement.Robot.X = M(:,5);
Measurement.Robot.Y = M(:,6);
Measurement.Robot.Phi = M(:,7);

clear M folder RPLidarFileName;

%% Get marker readings
Marker.Distance = [];
Marker.Angle = [];
for i = 1:Measurement.numPoints
    if (Measurement.Distance(i) <= MarkerDistanceMax) && (Measurement.Distance(i) >= MarkerDistanceMin)
        Marker.Distance = [Marker.Distance; Measurement.Distance(i)];
        Marker.Angle = [Marker.Angle; Measurement.Angle(i)];
    end
end
clear i MarkerDistanceMax MarkerDistanceMin Measurement;

%% Find calibration points
% scan = lidarScan(Marker.Distance,Marker.Angle);
% plot(scan)
clear Marker

points_1 = [ 180.4 46.49; ...
             180.2 17.3; ...
            -192.9 51.06; ...
            -193.7 20.15];

% Medidas reais da pe�a 3D
points_2 = [ 15 -193.7; ...
            -15 -193.7; ...
             15  193.7; ...
            -15  193.7];


% plot(points_1(:,1),points_1(:,2), 'r*'); hold on; grid;
% plot(points_1(1,1),points_1(1,2), 'ro');
% plot(points_2(:,1),points_2(:,2), 'b*');
% plot(points_2(1,1),points_2(1,2), 'bo');

%% Transformation
reg = absor(points_1',points_2');
% M =
% 
%     0.0100    1.0000  -33.6835
%    -1.0000    0.0100   -6.8362
%          0         0    1.0000

%% error check

error = zeros(3,4);
for i = 1:4
    error(:,i) = (reg.M*[points_1(i,:) 1]') - ([points_2(i,:) 1]');
end
clear i points_1 points_2;

max_error_per_coord = max(error(1:2,:),[],2);
max_error_general = max(max(error(1:2,:)));
mean_error_per_coord = mean(error(1:2,:),2);
mean_error_general = mean(mean(error(1:2,:)));
clear error;

