%{

=========================================================================
Note: kv = 1 and ks = 1.
    - poseMessage001_part1_test001 - origin (0,0,PI-PI/6). To (500,500). On air.No minimum speed
    - poseMessage001_part1_test002 - origin (400,400,PI-PI/6). To (500,500). On air. No minimum speed
    - poseMessage001_part1_test003 - origin (400,400,PI-PI/6). To (500,500). On
    air. speed [0.1,2] rad/s = 2rad/s, stops motors if within 20mm of the
    (x_desired, y_desired)
    - poseMessage001_part1_test004 - origin (400,400,PI-PI/6). To (500,500). On
    air. speed [1,5] rad/s = 5rad/s, stops motors if within 5mm of the
    (x_desired, y_desired)
    - poseMessage001_part1_test005 - origin (400,400,PI-PI/6). To (500,500). On
    air. speed [1,3] rad/s = 3rad/s, stops motors if within 5mm of the
    (x_desired, y_desired)
kv = 2 and ks = 2.
    -poseMessage001_part1_test006 - origin (400,400,PI-PI/6). To (500,500). On
    air. speed [1,3] rad/s = 3rad/s, stops motors if within 5mm of the
    (x_desired, y_desired)
    -poseMessage001_part1_test007 - origin (100,400,PI/2). To (500,500). On
    air. speed [1,3] rad/s = 3rad/s, stops motors if within 5mm of the
    (x_desired, y_desired)

=========================================================================
testes part2:
    - poseMessage001_part2_test001.csv - kd = 0.01, kh = 0.01, v = 200, 
    (a,b,c) = (0,-1,200). pose = (100,400,PI/2).
    - poseMessage001_part2_test002.csv - kd = 0.01, kh = 0.05, v = 200, 
    (a,b,c) = (0,-1,200). pose = (100,400,PI/2).
    - poseMessage001_part2_test003.csv - kd = 0.01, kh = 1, v = 200, 
    (a,b,c) = (0,-1,200). pose = (100,400,PI/2).
    - poseMessage001_part2_test004.csv - kd = 0.05, kh = 1, v = 200, 
    (a,b,c) = (0,-1,200). pose = (100,400,PI/2).

=========================================================================
part3, pre alpha and beta limitation:
    - poseMessage001_part3_test001.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(100,100,pi/2); final(x,y,phi)=(-100,-400,-pi/2); On air.
    Se alpha !e [-pi/2,pi/2] inverte v e w.
    - poseMessage001_part3_test002.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(100,100,pi/2); final(x,y,phi)=(-100,-400,-pi/2); On air.
    - poseMessage001_part3_test003.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(0,0,0); final(x,y,phi)=(500,500,pi/2); On air.
    - poseMessage001_part3_test004.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(0,0,0); final(x,y,phi)=(500,500,-pi/2); On air.
    - poseMessage001_part3_test005.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(0,0,0); final(x,y,phi)=(500,500,0); On air.
    - poseMessage001_part3_test006.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500,0); On air.
    - poseMessage001_part3_test007.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500,-pi/2); On air.
    - poseMessage001_part3_test008.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500,-pi); On air.
part3, just beta limitation;
    - poseMessage001_part3_test009.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500,-pi); On air. B
    limited to [-pi,pi]

part3, alpha and beta limited to [-pi, pi]
    - poseMessage001_part3_test010.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500,-pi/2); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test011.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500,-pi); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test012.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,500, 0); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test013.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(-100,-500, PI/2); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test014.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(50,100,pi/2); final(x,y,phi)=(500,500, 0); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test015.csv - kp=1, ka=2, kb=-1;
    inicial(x,y,phi)=(-700,-900,-pi); final(x,y,phi)=(500,500, 0); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test016.csv - kp=0.1, ka=0.2, kb=-1;
    inicial(x,y,phi)=(-700,-900,-pi); final(x,y,phi)=(500,500, 0); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test017.csv - kp=0.2, ka=0.5, kb=-1;
    inicial(x,y,phi)=(-700,-900,-pi); final(x,y,phi)=(500,500, 0); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test018.csv - kp=0.2, ka=1, kb=-1;
    inicial(x,y,phi)=(-700,-900,-pi); final(x,y,phi)=(500,500, 0); On air.
    alpha and beta limited to [-pi,pi]
    - poseMessage001_part3_test019.csv - kp=0.4, ka=2, kb=-1;
    inicial(x,y,phi)=(-700,-900,-pi); final(x,y,phi)=(500,500, 0); On air.
    alpha and beta limited to [-pi,pi]
%}
